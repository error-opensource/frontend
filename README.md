# Humap Frontend

This is a Gatsby project which generates frontend static sites for Humap subscribers.

# Getting started

Install the js dependencies by running `docker-compose run --rm gatsby yarn`

To run the project you'll be able to do `docker-compose up` and view the site on http://localhost:8000 on your host machine.

# Accessing backend services

The frontend gatsby site shares a network with all the other Humap services so you'll be able to access the following in development:

`http://humap-core:3000` Rails API

`http://graphql-engine:8080` Hasura 

`redis://redis:6379` Redis

`http://sidekiq:9292` Sidekiq