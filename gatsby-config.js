require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
    siteMetadata: {
        title: `Humap`,
        description: `Map compelling stories about places, people, and artefacts`,
        author: `Error Agency`,
        tenant_slug: process.env['GATSBY_TENANT_SLUG'],
        tenant_id: process.env['GATSBY_TENANT_ID']
    },
    plugins: [
        {
            resolve: `gatsby-plugin-create-client-paths`,
            options: { prefixes: [`/map/*`] },
        },
        {
            resolve: `gatsby-source-graphql`,
            options: {
                typeName: `Humap`,
                fieldName: `humap`,
                url: `${process.env['GATSBY_GRAPHQL-SERVICE_URL']}/v1/graphql`,
                headers: {
                    "X-Hasura-Tenant-Id": process.env['GATSBY_TENANT_ID'],
                }
            },
        },
        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/assets/images`,
            },
        },
        `gatsby-plugin-sass`
        // this (optional) plugin enables Progressive Web App + Offline functionality
        // To learn more, visit: https://gatsby.dev/offline
        // `gatsby-plugin-offline`,
    ],
}
