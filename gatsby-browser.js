import "./src/assets/styles/normalise.css";
import "./src/assets/styles/base.css";

export { wrapRootElement } from './src/apollo/wrap-root-element';