const createPages = require('./sources/createPages')
const createCollections = require('./sources/createCollections')
const createTenantStylesheet = require('./sources/createTenantStylesheet')

exports.createPages = async ({ graphql, actions }) => {
    await createPages({ actions, graphql })
    // await createCollections({ actions, graphql })
}

// exports.onCreatePage = async ({ page, actions }) => {
//     const { createPage } = actions
//     // Only update the `/app` page.

//     if (page.path.match(/^\/map/)) {
//         // page.matchPath is a special key that's used for matching pages
//         // with corresponding routes only on the client.
//         page.matchPath = "/map/*"
//         // Update the page.
//         createPage(page)
//     }
// }

// Download a tenant-specific stylesheet if any
exports.onPreInit = async ({reporter}) => {
  await createTenantStylesheet({reporter: reporter});
}

// This loads the two standalone scss files
exports.onCreateWebpackConfig = ({ actions }, options) => {
    actions.setWebpackConfig({
        module: {
            rules: [
                {
                    test: /\.s[ac]ss$/,
                    loader: "sass-resources-loader",
                    options: {
                        resources: [
                          './src/assets/styles/theme/tenant-stylesheet.scss',
                          './src/assets/styles/theme/variables.scss',
                            './src/assets/styles/theme/mixins.scss'
                            ]
                    }
                }
            ]
        }
    });
};
