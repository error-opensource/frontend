FROM YOUR REPO
# Needed because Heroku doesn't run as root, so we need to try it without, locally.
RUN adduser -D user

RUN apk add ruby readline-dev libxml2 libxml2-dev libxml2-utils libxslt-dev zlib-dev zlib yaml-dev build-base ruby-dev util-linux

RUN mkdir -p /app


RUN yarn

USER user



