const fs = require('fs')
const axios = require('axios')

module.exports = async ({reporter}) => {
  const stylesheetUrl = `${process.env.GATSBY_CORE_URL}/tenants/${process.env.GATSBY_TENANT_ID}/stylesheet`;
  const stylesheetFile = './src/assets/styles/theme/tenant-stylesheet.scss';

  try {
    const response = await axios.get(stylesheetUrl, {responseType: "text"})
    if (response.status === 200) {
      const scss = response.data
      reporter.info(`Loading tenant stylesheet from ${stylesheetUrl}`)
      fs.writeFile(stylesheetFile, scss, (error) => {
        if (error) {
          reporter.error(`Something wrong when writing ${stylesheetFile}`, error)
        }
      });
    }
  } catch(e) {
    reporter.error(`Core didn't return a 200 OK HTTP response for ${stylesheetUrl}`)
  }

}