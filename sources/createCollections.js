const path = require(`path`)

module.exports = async ({ actions, graphql }) => {
    const collections = [
        {name: "First collection", slug: "collection-1", content: "Testing", "back": "collection-2"},
        {name: "Collection 2", slug: "collection-2", content: "Testing", "back": "collection-1"}
    ]
    
    const { createPage } = actions

    collections.map(collection => {
        console.log(`\n********Creating collection ${collection.slug}********\n`)
        createPage( {
            path: `/map/collections/${collection.slug}`,
            component: path.resolve(`./src/components/Collections/CollectionView.js`),
            context: collection
        })
    })
}
