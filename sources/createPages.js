const path = require(`path`)

module.exports = async ({ actions, graphql }) => {
	const { createPage } = actions;
	const pageTypes = {
		content_page: 0,
		home_page: 1
	}
	const { data } = await graphql(`
        query {
            humap {
                pages {
                    slug
                    id
                    page_type
                }
                tenant_settings {
                	is_embedded
                }
            }
        }      
    `)

	if (data.humap.pages.length > 0) {
		data.humap.pages.forEach(({ slug, id, page_type }) => {
				if (page_type === pageTypes.home_page) {
					pageComponent = path.resolve(`./src/components/Templates/homePage.js`)
					pagePath = "/"
				} else {
					pageComponent = path.resolve(`./src/components/Templates/contentPage.js`)
					pagePath = slug
				}
				createPage({
					path: pagePath,
					component: pageComponent,
					context: {
						pageId: id
					}
				})
			}
		)
	}


}
