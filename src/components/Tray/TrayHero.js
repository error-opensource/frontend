import React from "react"

// Styles
import styles from "./TrayHero.module.scss"

const TrayHero = (props) => (
    <div className={styles.TrayHero}>
        {props.children}
    </div>
)

export default TrayHero