import React from "react"

// Styles
import styles from "./TrayContent.module.scss"

// Components
import HumapFooter from "../Shared/HumapFooter";

const TrayContent = (props) => {
    return <div className={styles.TrayContent}>
        {props.children}
        <HumapFooter/>
    </div>
}

export default TrayContent