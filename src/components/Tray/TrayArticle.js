import React from "react"

// Styles
import styles from "./TrayArticle.module.scss"

const TrayArticle = (props) => (
    <div className={`${styles.TrayArticle} TrayArticle--${props.layoutType}`}> {/* Centred */}
        {props.children}
    </div>
)

export default TrayArticle