import React from "react"

// Styles
import styles from "./TrayTitle.module.scss"

const TrayTitle = (props) => (
    <div className={styles.TrayTitle}>
        {props.children}
    </div>
)

export default TrayTitle