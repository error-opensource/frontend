import React from "react"
import {ResponsiveContext} from "grommet"
import {useStore} from "../../models/RootStore"
import {observer} from "mobx-react-lite";

// Styles
import styles from "./Tray.module.scss"

// Components
import Toolbar from "../Global/Toolbar"

const Tray = ({children}) => {
    const {uiSettings: {trayVisible}} = useStore()

    return <>

        <ResponsiveContext.Consumer>
            {responsive =>
                responsive === "smallMobile" || responsive === "mobile" || responsive === "tablet" ? (

                    <>
                        <Toolbar/>
                        <div className={`${styles.Tray}`}>
                            {children}
                        </div>
                    </>

                ) : (

                    <>
                        <div className={`${styles.Tray} ${trayVisible ? styles.isOpen : styles.isClosed}`}>
                            <Toolbar/>
                            {children}
                        </div>
                    </>

                )
            }
        </ResponsiveContext.Consumer>

    </>

}

export default observer(Tray)