import React from "react"
import {useStore} from "../../models/RootStore";
import {observer} from "mobx-react-lite";

// Styles
import styles from "./TrayOverview.module.scss"

const TrayOverview = (props) => {
    const {uiSettings: {trayVisible, setTrayVisible}} = useStore()

    return <div className={`${styles.TrayOverview} ${trayVisible ? styles.isHidden : styles.isVisible}`}>
        {props.children &&
        <div className={styles.content}>
            {props.children}
        </div>
        }
        <button className={styles.openTrayButton} onClick={() => setTrayVisible(!trayVisible)}><span>Open tray</span>
        </button>
    </div>
}

export default observer(TrayOverview)