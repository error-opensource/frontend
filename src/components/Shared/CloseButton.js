import React from "react"
import { Link } from "gatsby"
import { navigate, useLocation } from "@reach/router"
import PropTypes from "prop-types"

// Styles
import styles from "./CloseButton.module.scss"


const CloseButton = (props) => {
    const location = useLocation()

    const goBack = () => {
        // if( location.hasOwnProperty('action') ) {
        //     console.log("Going back")
        //     event.preventDefault()
        // }

        props.callback()
    }

    return <div className={styles.CloseButton} onClick={goBack}>
        <Link to={props.closePath || CloseButton.defaultProps.closePath}>{props.children}</Link>
    </div>
}

CloseButton.defaultProps = {
    closePath: "/map/browse",
    callback: () => {
    }
}

CloseButton.propTypes = {
    callback: PropTypes.func
}

export default CloseButton