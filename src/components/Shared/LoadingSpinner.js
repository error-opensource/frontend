import React from "react"
import BounceLoader from "react-spinners/BounceLoader";

// Styles
import styles from "./LoadingSpinner.module.scss"

const LoadingSpinner = (props) => {

    return <div className={styles.LoadingSpinner}>
        <BounceLoader size={60} color={styles.spinnerColor} loading={true}/>
    </div>

}

export default LoadingSpinner