import React from "react"
import PropTypes from "prop-types"
import {observer} from "mobx-react-lite"

// Styles
import styles from "./HierarchicalTermSelector.module.scss"
import HierarchicalTermSelectorItem from "./HierarchicalTermSelectorItem";

const HierarchicalTermSelector = ({taxonomy}) => {
    return <div className={styles.HierarchicalTermSelector}>
        <h2>Refine your search by choosing {taxonomy.name.toLowerCase()}</h2>
        <div className={styles.HierarchicalTermList}>
            {taxonomy.terms.map((term) => <HierarchicalTermSelectorItem key={`term-${term.id}`} taxonomy={taxonomy} term={term} /> )}
        </div>
    </div>
}

HierarchicalTermSelector.propTypes = {
    taxonomy: PropTypes.object.isRequired
}

export default observer(HierarchicalTermSelector)