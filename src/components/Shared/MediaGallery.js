import React from "react"
import { Layer } from "grommet"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons"
import { observer } from "mobx-react-lite"
import { useStore } from "../../models/RootStore";

// Styles
import styles from "./MediaGallery.module.scss"

// Components
import MediaGalleryItem from "./MediaGalleryItem";

const MediaGallery = (props) => {
    const {uiSettings: {mediaGalleryVisible, setMediaGalleryVisible}} = useStore()

    const closeMediaGallery = () => (
        setMediaGalleryVisible(false)
    )

    const images = props.attached_images.map((attached_image, idx) => (
        <MediaGalleryItem key={`gallery-item-${idx}`} {...attached_image} />
    ))

    let imagesLabel = "";

    if (props.attached_images.length > 1) {
        imagesLabel = 'images';
    } else {
        imagesLabel = 'image'
    }
    
    return <>
        {mediaGalleryVisible &&

        <Layer full plain animation="fadeIn" onClickOutside={closeMediaGallery} margin="0">
            <div className={styles.MediaGallery}>

                <div className={styles.header}>
                    <div className={styles.title}>
                        <h1><span>Gallery</span> {props.record.name}</h1>
                    </div>
                    <div className={styles.controls}>
                        <div className={styles.count}>{props.attached_images.length} <span>{imagesLabel}</span></div>
                        {props.attached_images.length > 1 &&
                        <>
                            {/*<button><FontAwesomeIcon icon={faAngleUp}/></button>
                            <button><FontAwesomeIcon icon={faAngleDown}/></button>*/}
                        </>
                        }
                    </div>
                    <button className={styles.close} onClick={closeMediaGallery}>Close <span>gallery</span></button>
                </div>

                <div className={styles.mediaItems}>
                    {images}
                </div>

            </div>
        </Layer>

        }
    </>

}

export default observer(MediaGallery)