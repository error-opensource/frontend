import React from "react"

const PlaceholderImage = (props) => {

    const dimensions = props.width + "x" + props.height
    const unsplashImageSource = `https://source.unsplash.com/random/${dimensions}?sig=${Math.floor(Math.random() * 100)}`

    return (
        <img src={unsplashImageSource} alt="Placeholder image" />
    )

}

export default PlaceholderImage