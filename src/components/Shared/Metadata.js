import React from "react"

// Styles
import styles from "./Metadata.module.scss"

const Metadata = (props) => (

    <div className={styles.Metadata}>

        {typeof props.recordCount == "number" &&
        <span>{props.recordCount} records</span>
        }

        {typeof props.collectionCount == "number" &&
        <span>{props.collectionCount} collections</span>
        }

        {typeof props.trailCount == "number" &&
        <span>{props.trailCount} trails</span>
        }

        {typeof props.trailStops == "number" &&
        <span>{props.trailStops} stops</span>
        }
        

        {props.name &&
        <span>Created by {props.name}</span>
        }

        {props.children}
    </div>
)

export default Metadata




