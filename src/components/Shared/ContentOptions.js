import React, { useEffect } from "react"
import { CheckBox } from "grommet"
import { observer } from "mobx-react-lite"
import { useStore } from "../../models/RootStore"

// Styles
import styles from "./ContentOptions.module.scss"

const ContentOptions = () => {
  const { cardItems: { filter } } = useStore()
  
  return <div className={styles.ContentOptions}>
    <CheckBox name={`record`} checked={filter.type.indexOf(`record`)>-1} label="Records" onChange={filter.toggleSearchType} />
    <CheckBox name={`collection`} checked={filter.type.indexOf(`collection`)>-1} label="Collections" onChange={filter.toggleSearchType} />
  </div>
}

export default observer(ContentOptions)