import React from "react"
// Styles
import styles from "./CategoryList.module.scss"

// Components
import CategoryListItem from "./CategoryListItem.js"

const CategoryList = ({taxonomies}) => {
    return <ul className={styles.CategoryList}>
        {taxonomies.map((taxonomy) => <CategoryListItem key={`category-list-taxonomy-${taxonomy.id}`} {...taxonomy} />)}
    </ul>
}

export default CategoryList