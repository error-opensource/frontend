import React from "react"

// Styles
import styles from "./LocationBar.module.scss"

const LocationBar = (props) => (
    <div className={styles.LocationBar}>
        <span className={styles.title}>{props.title}</span>
        {props.children}
    </div>
)

export default LocationBar