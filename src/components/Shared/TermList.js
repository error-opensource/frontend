import React from "react"
import PropTypes from "prop-types"

// Styles
import styles from "./TermList.module.scss"

// Components
import TermListItem from "./TermListItem.js"

const TermList = ({terms}) => {
    return (
      <ul className={styles.TermList}>
          {terms.map(({id, name}) => <TermListItem key={`term-${id}`} id={id} name={name} />)}
      </ul>
    )
}


TermList.propTypes = {
    terms: PropTypes.object.isRequired
}

export default TermList