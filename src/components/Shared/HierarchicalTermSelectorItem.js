import React, { useState, useRef } from 'react';
import PropTypes from "prop-types"
import { CheckBox } from "grommet"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCaretDown } from "@fortawesome/free-solid-svg-icons"
import { observer } from "mobx-react-lite"

// Styles
import styles from "./HierarchicalTermSelectorItem.module.scss";

// Components
//import CategoryListItem from "./CategoryListItem.js"

const HierarchicalTermSelectorItem = (props) => {
    const [menuState, toggleMenu] = useState(false);

    const childTermsDropdown = useRef();

    const showChildTerms = (event) => {
        toggleMenu( true);
        document.addEventListener('click', hideChildTerms);
    }

    const hideChildTerms = (event) => {

        if (childTermsDropdown.current && !childTermsDropdown.current.contains(event.target)) {

            toggleMenu(false);
            document.removeEventListener('click', hideChildTerms);

        }
    }

    return <div className={styles.HierarchicalTermSelectorItem}>
        <CheckBox label={props.term.name} onChange={props.term.toggleAllSubTerms} checked={props.term.allChecked} indeterminate={props.term.partiallyChecked}/>

        <a onClick={() => showChildTerms()} className={styles.refineButton}><FontAwesomeIcon icon={faCaretDown}/></a>

        {menuState && props.taxonomy.hasSubTerms &&
        <div className={styles.childTerms} ref={childTermsDropdown}>
            {props.term.sub_terms.map((sub_term) => {
                return <div key={sub_term.id} className={styles.childTerm}>
                    <CheckBox label={sub_term.name} checked={sub_term.isChecked} onChange={sub_term.toggle}/>
                </div>
            })}
        </div>
        }
    </div>
}

HierarchicalTermSelectorItem.propTypes = {
    term: PropTypes.object.isRequired,
    taxonomy: PropTypes.object.isRequired
}

export default observer(HierarchicalTermSelectorItem)