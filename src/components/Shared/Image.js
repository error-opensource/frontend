import React from "react";
import PropTypes from "prop-types";

import ImgixClient from "../../queries/ImgixClient";

const Image = (props) => {

  if (props.hasOwnProperty('url') && typeof(props.url) == 'string') {
    const key = JSON.stringify(props);

    const {url, name, description, credit, parameters, options, sizes, ...otherArgs} = props;

    const defaultParameters = {
      fit: 'crop',
      crop: 'faces,entropy,center'
    }

    const defaultSizes = {
        "(min-width:1400px)": "420px",
        "(max-width: 399px)": "200px",
        "(max-width: 599px)": "300px",
        "(max-width: 799px)": "400px",
        "(max-width:1024px)": "512px",
        "(max-width:1199px)": "300px",
        "(max-width:1399px)": "350px"
    }

    const defaultOptions = { maxWidth: 2000 }

    const fallbackSize = 1680;

    let path;

    // extract the path of the image
    try {
      path = new URL(url).pathname
    } catch(e) {
      // if we fail to parse it, assume the url is just a path
      path = url
    }


    // Concatenate the text content from name, description and credit (if present)
    const textContent = [name, description, credit]
      .filter((text) => typeof(text) != 'undefined' && text != null)
      .join(". ")

    const srcset = ImgixClient.buildSrcSet(path, Object.assign({}, defaultParameters, parameters), Object.assign({}, defaultOptions, options))
    const src = ImgixClient.buildURL(path, Object.assign({},defaultParameters,{w: fallbackSize}))
    const imageSizes = Object.entries(typeof(sizes) === 'undefined' ? defaultSizes : sizes).map((e) => (`${e[0]} ${e[1]}`)).join(", ")

    return <img key={key}
                src={src}
                srcSet={srcset}
                sizes={imageSizes}
                alt={textContent}
                crossOrigin={'anonymous'}
                {...otherArgs}
    />
  } else {
    return <></>;
  }
}

export default Image;

Image.propTypes = {
  url: PropTypes.string,
  parameters: PropTypes.object,
  options: PropTypes.object,
  sizes: PropTypes.object,
  title: PropTypes.string,
  description: PropTypes.string,
  credit: PropTypes.string
}

