import React from "react"

// Styles
import styles from "./LoadButton.module.scss"

const LoadButton = ({callback, children}) => (
    <div className={styles.LoadButton}>
        <button onClick={callback} className={styles.LoadButton}>
            {children}
        </button>
    </div>
)

export default LoadButton