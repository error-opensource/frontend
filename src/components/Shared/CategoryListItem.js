import React from "react"
import { Link } from "gatsby"

// Styles
import styles from "./CategoryListItem.module.scss"

const CategoryListItem = ({name, id}) => (
    <li className={styles.CategoryListItem}><Link to={`/map/search?terms=${id}`}>{name}</Link></li>
)

export default  CategoryListItem