import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebookF, faTwitter } from "@fortawesome/free-brands-svg-icons"
import { faEnvelope } from "@fortawesome/free-solid-svg-icons"

// Styles
import styles from "./ShareButtons.module.scss"

const ShareButtons = (props) =>  {

    return <div className={styles.ShareButtons}>

        <ul>
            <li className={styles.facebook}><a target="_blank" href={`https://www.facebook.com/sharer/sharer.php?u=${props.URL}`}><span>Facebook</span><FontAwesomeIcon icon={faFacebookF} /></a></li>
            <li className={styles.twitter}><a target="_blank" href={`https://twitter.com/share?url=${props.URL}&text=${props.excerpt}`}><span>Twitter</span><FontAwesomeIcon icon={faTwitter} /></a></li>
            <li className={styles.email}><a target="_blank" href={`mailto:?subject=Check%20out%20this%20content%20from%20${props.site_title}.&body=${props.URL}`}><span>Email</span><FontAwesomeIcon icon={faEnvelope} /></a></li>
        </ul>

    </div>

}

export default ShareButtons