import React from "react"
import { observer } from "mobx-react-lite"
import { useStore } from "../../models/RootStore";
import Image from "./Image";

// Styles
import styles from "./MediaList.module.scss"

const MediaList = ({ id, attached_images }) => {
    const {uiSettings: {mediaGalleryVisible, setMediaGalleryVisible}} = useStore()

    if( attached_images.loading.isLoading ) {
        return <p>Loading images...</p>
    }

    const images = attached_images.results.map((attached_image, idx) => {
        return <li key={`attached-image-${id}-${idx}`}>
            <Image {...attached_image}
                   parameters={{
                       ar: '3:2'
                   }}
                   options={{
                       minWidth: 200,
                       maxWidth: 512
                   }}
            />
        </li>
    })

    return <div className={styles.MediaList}>
        <ul onClick={(e) => { setMediaGalleryVisible(!mediaGalleryVisible); e.preventDefault();}}>
            {images}
        </ul>
    </div>

}

export default observer(MediaList)