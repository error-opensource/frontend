import React from "react"

// Styles
import styles from "./MediaGalleryItem.module.scss"
import Image from "./Image";

const MediaGalleryItem = (props) => {

    return (
        <div className={styles.MediaGalleryItem}>
            <div className={styles.sidebar}>
                <div className={styles.pane}>
                    <div className={styles.caption}>
                        <p>
                            {props.name}
                        </p>
                    </div>
                    { props.credit &&
                        <div className={styles.attribution}>
                            {props.credit}
                        </div>
                    }
                </div>
            </div>
            <div className={styles.item}>
              <Image {...props}
                  parameters={{
                    fit: 'clip'
                  }}
                     options={{
                       minWidth: 370,
                       maxWidth: 1740
                     }}
                     sizes={{
                       "(min-width:1800)": "1740px",
                       "(max-width: 399px)": "370px",
                       "(max-width: 599px)": "570px",
                       "(max-width: 767px)": "738px",
                       "(max-width:1024px)": "964px",
                       "(max-width:1199px)": "940px",
                       "(max-width:1399px)": "1140px",
                       "(max-width:1599px)": "1340px",
                       "(max-width:1799px)": "1540px"
                     }}

              />
            </div>
        </div>
    )

}

export default MediaGalleryItem