import React from "react"
import humapLogoAsset from "../../assets/images/logo-humap-map-white.svg";

// Styles
import styles from "./HumapFooter.module.scss"

const HumapFooter = ({children}) => (

    <div className={styles.HumapFooter}>
        <a href="https://humap.me">
            Made with <b>Humap</b> <img src={humapLogoAsset} alt="Logo" />
        </a>
    </div>

)

export default HumapFooter