import React, { useState } from 'react';

import styles from "./ColorPicker.module.scss"
import RecordCard from "../Records/RecordCard";
import Page from "../Pages/Page";

const ColorPicker = () => {

    const [currentColor, setCurrentColor] = useState('');
    const [currentTextColor, setCurrentTextColor] = useState('');
    const [currentBackgroundColor, setCurrentBackgroundColor] = useState('');

    const rgb = [255, 0, 0];

    function updateColors(e) {


        //console.log(e.target.value);
        let hex = e.target.value.substr(1);
        let rgb = []
        let bigint = parseInt(hex, 16);
        rgb[0] = (bigint >> 16) & 255;
        rgb[1] = (bigint >> 8) & 255;
        rgb[2] = bigint & 255;


        // http://www.w3.org/TR/AERT#color-contrast
        const brightness = Math.round(((parseInt(rgb[0]) * 299) +
            (parseInt(rgb[1]) * 587) +
            (parseInt(rgb[2]) * 114)) / 1000);
        const textColour = (brightness > 125) ? 'black' : 'white';
        const backgroundColour = 'rgb(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ')';

        setCurrentColor(e.target.value)
        setCurrentTextColor(textColour)
        setCurrentBackgroundColor(backgroundColour)

    }

    return <div className={styles.ColorPicker}>

        <style dangerouslySetInnerHTML={{
            __html: `
        *[class*="RecordCard"] section,
        *[class*="ColorPicker-module--buttons"] button,
        *[class*="ColorPicker-module--block"] div { background: ${currentColor}; color: ${currentTextColor} }
        *[class*="ColorPicker-module--mode"] h1,
        *[class*="ColorPicker-module--buttons"] li:nth-child(3) button { color: ${currentColor} }
        *[class*="ColorPicker-module--block"] div h1,
        *[class*="ColorPicker-module--block"] div p { color: ${currentTextColor} !important }
       
        
        *[class*="currentColor"] {
        color: ${currentTextColor};
        background:${currentBackgroundColor};
        }
    `
        }}/>

        <div className={styles.input}>
            <input type="color" id="head" name="head" onChange={updateColors}/>
            <div className={styles.currentColor} style={{background: currentColor}}>Current color is: {currentColor}</div>
        </div>

        <div className={`${styles.mode} ${styles.modeLight}`}>

            <h2>Headings</h2>
            <ul>
                <li>
                    <h1>The Title of a Record</h1>
                    <p>Integer accumsan libero at dui dignissim egestas. Integer et risus quam. Sed eget volutpat felis. Cras quis quam consequat arcu.</p>
                </li>
            </ul>

            <h2>Buttons</h2>
            <ul className={styles.buttons}>
                <li className="on-white">
                    <button>Choose</button>
                </li>
                <li className="on-gray">
                    <button>Choose</button>
                </li>
                <li className="on-dark">
                    <button>Choose</button>
                </li>
            </ul>

            <h2>Records</h2>
            <ul className={styles.records}>
                <li>
                    <RecordCard path="#" record={{name: "Example Record", cardImage: "https://images.unsplash.com/photo-1596415524580-7426d86a836a?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=200&ixlib=rb-1.2.1&q=80&w=200"}} />
                    <RecordCard path="#" record={{name: "Example Record", cardImage: "https://images.unsplash.com/photo-1596415524580-7426d86a836a?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=200&ixlib=rb-1.2.1&q=80&w=200"}} />
                </li>
            </ul>

            <h2>Block</h2>
            <ul className={styles.block}>
                <li>
                    <div>
                        <h1>The Title of a Record</h1>
                        <p>Nunc aliquet erat felis, et luctus erat posuere viverra. Curabitur eu nibh porta, porta lorem id, congue felis. Nulla ultricies.</p>
                    </div>
                </li>
            </ul>

        </div>

        <div className={`${styles.mode} ${styles.modeDark}`}>

            <h2>Headings</h2>
            <ul>
                <li>
                    <h1>The Title of a Record</h1>
                    <p>Integer accumsan libero at dui dignissim egestas. Integer et risus quam. Sed eget volutpat felis. Cras quis quam consequat arcu.</p>
                </li>
            </ul>

            <h2>Buttons</h2>
            <ul className={styles.buttons}>
                <li className="on-white">
                    <button>Choose</button>
                </li>
                <li className="on-gray">
                    <button>Choose</button>
                </li>
                <li className="on-dark">
                    <button>Choose</button>
                </li>
            </ul>

            <h2>Records</h2>
            <ul className={styles.records}>
                <li>
                    <RecordCard path="#" record={{name: "Example Record", cardImage: "https://images.unsplash.com/photo-1596415524580-7426d86a836a?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=200&ixlib=rb-1.2.1&q=80&w=200"}} />
                    <RecordCard path="#" record={{name: "Example Record", cardImage: "https://images.unsplash.com/photo-1596415524580-7426d86a836a?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=200&ixlib=rb-1.2.1&q=80&w=200"}} />
                </li>
            </ul>

            <h2>Block</h2>
            <ul className={styles.block}>
                <li>
                    <div>
                        <h1>The Title of a Record</h1>
                        <p>Nunc aliquet erat felis, et luctus erat posuere viverra. Curabitur eu nibh porta, porta lorem id, congue felis. Nulla ultricies.</p>
                    </div>
                </li>
            </ul>


        </div>
    </div>

}

export default ColorPicker