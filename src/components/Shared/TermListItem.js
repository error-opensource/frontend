import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"

// Styles
import styles from "./TermListItem.module.scss"

const TermListItem = ({id, name}) => (
    <li className={styles.TermListItem}>
        <Link to={`/map/search/results?taxonomy_term_ids=${id}`} term-id={id}>{name}</Link>
    </li>
)

TermListItem.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
}

export default TermListItem