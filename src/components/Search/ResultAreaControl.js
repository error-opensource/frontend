import React from "react"
import { CheckBox } from "grommet"

// Styles
import styles from "./ResultAreaControl.module.scss"

const ResultAreaControl = (props) => (
    <div className={styles.ResultAreaControl}>
        <span>Search visible area</span>
        <CheckBox toggle/>
        <span>Search whole map</span>
    </div>
)

export default ResultAreaControl