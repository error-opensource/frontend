import React, { useState, useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import { Button, CheckBox, Form, FormField, TextInput } from 'grommet'
import { motion, AnimatePresence } from 'framer-motion'
import { navigate } from '@reach/router'
import { useStore } from '../../models/RootStore'

// Styles
import styles from './SearchForm.module.scss'

// Components
import Metadata from '../Shared/Metadata'
import TermList from '../Shared/TermList'
import TrayContent from '../Tray/TrayContent'
import TrayTitle from '../Tray/TrayTitle'
import HierarchicalTermSelector from '../Shared/HierarchicalTermSelector'
import LoadingSpinner from '../Shared/LoadingSpinner'
import queryString from 'query-string'

function SearchForm() {
  const {
    adjustLayoutScroll,
    loading,
    searchResults,
    searchResults: {
      reset, 
      filter: { taxonomies },
    },
  } = useStore()

  const [query, setQuery] = useState('')

  useEffect(() => {
    reset()
    adjustLayoutScroll('top')
  }, [])

  if (loading.didFail) {
    return <p>Error</p>
  }

  const taxonomyContainers = taxonomies.results.map((taxonomy) => {
    switch (taxonomy.type) {
      case 'dropdown':
        return <React.Fragment key={`taxonomy-${taxonomy.id}`} />
      case 'radio':
        return <React.Fragment key={`taxonomy-${taxonomy.id}`} />
      case 'nested':
        return (
          <HierarchicalTermSelector
            taxonomy={taxonomy}
            key={`taxonomy-${taxonomy.id}`}
          />
        )
      default:
        return <></>
    }
  })

  const closePath = (location) => {
    return '/map/search'
  }

  const submitForm = () => {
    searchResults.filter.setSearchParameter('query', query)
    navigate(`/map/search/results?${searchQuerystring()}`)
  }

  const searchQuerystring = () => {
    const params = {
      q: searchResults.filter.query,
      taxonomy_term_ids: searchResults.filter.taxonomies.checkedIds,
    }

    return queryString.stringify(params, {
      arrayFormat: 'comma',
      encode: false,
    })
  }

  return (
    <div className={styles.SearchForm}>
      <AnimatePresence>
        <motion.div
          key="modal"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: 0.5 }}
        >
          <TrayContent>
            <div className={`${styles.SearchFormWrap} ${styles.isForm}`}>

                <TrayTitle>
                  <h1>Search</h1>
                </TrayTitle>

                <Form>
                  <FormField label="Text search" name="search">
                    <TextInput
                      name="search"
                      placeholder="Your search terms"
                      value={query}
                      onChange={(event) => setQuery(event.target.value)}
                    />
                  </FormField>

                  {taxonomies.loading.isLoading && <LoadingSpinner />}

                  {!taxonomies.loading.isLoading && taxonomies.count > 0 && (
                    <>{taxonomyContainers}</>
                  )}

                  <FormField
                    className={styles.checkBoxesWrap}
                    label="Content Type"
                    name="search"
                    data-rerender-type={searchResults.filter.type}
                  >
                    {searchResults.filter.contentTypes.filter((type) => type.value !== 'overlay').map((type) => (
                      <CheckBox
                        key={`content-type-filter-${type.value}`}
                        checked={
                          searchResults.filter.enabledContentTypes.indexOf(
                            type.value,
                          ) > -1
                        }
                        onChange={() =>
                          searchResults.filter.toggleSearchType(type.value)
                        }
                        label={type.label}
                      />
                    ))}
                  </FormField>

                  <FormField
                    className={styles.toggleWrap}
                    label="Search area"
                    name="search"
                  >
                    <span className={styles.extraLabel}>
                      Search visible area
                    </span>
                    <CheckBox
                      toggle
                      label="Search whole map"
                      onChange={searchResults.filter.toggleBoundedSearch}
                      checked={searchResults.filter.boundedSearch}
                    />
                  </FormField>

                  <Button
                    type="submit"
                    primary
                    label="Search"
                    onClick={submitForm}
                  />
                </Form>

            </div>
          </TrayContent>
        </motion.div>
      </AnimatePresence>
    </div>
  )
}

export const SearchResultsTrayOverview = observer((props) => {
  const { searchResults } = useStore()

  return (
    <>
      <h1>Searched: {searchResults.filter.query}</h1>
      {[].length > 0 && <TermList terms={[]} />}
      <section>
        <span className={styles.title}>Results</span>
        <Metadata
          recordCount={searchResults.recordCount}
          collectionCount={searchResults.collectionCount}
        />
      </section>
    </>
  )
})

export default observer(SearchForm)
