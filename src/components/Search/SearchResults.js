import React, { useEffect } from "react"
import { graphql } from "gatsby"
import { observer } from "mobx-react-lite";
import queryString from 'query-string';
import { motion, AnimatePresence } from "framer-motion"

import { useStore } from "../../models/RootStore"

import styles from "./SearchResults.module.scss"

import RecordCard from "../Records/RecordCard"
import TermList from "../Shared/TermList";
import Metadata from "../Shared/Metadata";

import TrayContent from "../Tray/TrayContent";
import TrayTitle from "../Tray/TrayTitle";
import LoadingSpinner from "../Shared/LoadingSpinner";
import LocationBar from "../Shared/LocationBar";
import CloseButton from "../Shared/CloseButton";
import RecordsList from "../Records/RecordsList";
import LoadButton from "../Shared/LoadButton";
import CollectionCard from "../Collections/CollectionCard";

// default Gatsby build-time query
export const BUILD_QUERY = graphql`
query SearchHeadingQuery {
  site {
    siteMetadata {
      title
    }
  }
}`

const SearchResults = (props) => {
    const {mapRefIsSet, searchResults,  mapViewport, searchResults: {loading, filter: { taxonomies }}} = useStore()

    const results = searchResults.results.map((result, idx) => {
        switch (result.type) {
            case 'record':
                return <RecordCard key={`card-${idx}-${result.slug}`} path={`/map/records/${result.slug}`}
                                   record={result}/>
            case 'collection':
                return <CollectionCard key={`card-${idx}-${result.slug}`} path={`/map/collections/${result.slug}`}
                                    collection={result}/>
            case 'trail':
                return <RecordCard key={`card-${idx}-${result.slug}`} path={`/map/trails/${result.slug}`}
                                    record={result} resultType={`Trail`} />
            default:
                return <></>
        }
    })

    useEffect(() => {
        const search = queryString.parse(props.location.search, {arrayFormat: "comma"});
        const {q: query=searchResults.filter.query, taxonomy_term_ids=[]} = search

        const ids = [].concat(taxonomy_term_ids).map(i => parseInt(i))

        const hasResults = searchResults.results.count > 0 
        
        if (!hasResults && ( searchResults.filter.taxonomiesAreLoaded && ((query && query !== searchResults.filter.query) || (ids !== searchResults.filter.taxonomies.checkedIds))) )  {
            searchResults.filter.setSearchParameter('query', query)
            searchResults.filter.taxonomies.setCheckedIds(ids)
            searchResults.fetchResults();
        }
    }, [mapRefIsSet, searchResults.filter.taxonomiesAreLoaded])

    return (
        <div className={styles.SearchResults}>

            <AnimatePresence>

                <motion.div key="modal" initial={{opacity: 0}} animate={{opacity: 1}} exit={{opacity: 0}}
                            transition={{duration: 0.5}}>

                    <TrayContent>

                        <LocationBar title='Search'>
                            <Metadata recordCount={searchResults.recordCount} collectionCount={searchResults.collectionCount} trailCount={searchResults.trailCount} />
                            <CloseButton closePath={'/map/search'}>Search again</CloseButton>
                        </LocationBar>

                        <TrayTitle>
                            {searchResults.filter.query &&
                                <h1>Your search: {searchResults.filter.query}</h1>
                            }
                            {!searchResults.filter.query &&
                                <h1>Your search</h1>
                            }
                        </TrayTitle>

                        {/*<Button primary label="Search again" />*/}

                        {loading.isLoading && searchResults.results.length === 0 &&
                            <LoadingSpinner/>
                        }

                        {searchResults.results.length === 0 && loading.isLoading &&
                        <RecordsList>
                            <p>No results.</p>
                        </RecordsList>
                        }


                        {searchResults.results.length > 0 &&
                        <>
                            <RecordsList>
                                {results}
                            </RecordsList>

                            {searchResults.hasMore &&
                            <LoadButton callback={searchResults.fetchNextPage}>Load more</LoadButton>
                            }

                            { loading.isLoading &&
                              <LoadingSpinner />
                            }

                        </>
                        }

                    </TrayContent>

                </motion.div>

            </AnimatePresence>

        </div>
    )

}


export const SearchResultsTrayOverview = observer((props) => {
    const {searchResults} = useStore()

    return <>
        <h1>Searched: {searchResults.filter.query}</h1>
        {[].length > 0 &&
        <TermList terms={[]}/>
        }
        <section>
            <span className={styles.title}>Results</span>
            <Metadata recordCount={searchResults.recordCount} collectionCount={searchResults.collectionCount}/>
        </section>
    </>
})


export default observer(SearchResults)
