import React from "react"
import RecordCard from "../Records/RecordCard"
import {observer} from "mobx-react-lite"

const CollectionRecordsView = ({path, loading, results}) => {
  if (loading.didFail) {
    return <p>Error</p>
  }

  if (loading.isLoading) {
    return <></>
  }

  const records = results.map((result) => <RecordCard key={result.slug} path={`${path}/records/${result.slug}`}
                                                      record={result}/>)

  return <>
    {records}
  </>
}

export default observer(CollectionRecordsView)
