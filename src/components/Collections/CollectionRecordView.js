import React, { useEffect } from "react"
import {observer} from "mobx-react-lite"
import { RecordViewDetail } from "../Records/RecordView"
import {useStore} from "../../models/RootStore"

// Components
import CloseButton from "../Shared/CloseButton"
import LocationBar from "../Shared/LocationBar"
import LoadingSpinner from "../Shared/LoadingSpinner"

export const CollectionRecordView = ({recordId, stopNumber, ...props}) => {
    const { setRecordSlug, record, loading } = useStore()
    
    useEffect(() => {
        setRecordSlug(recordId)
    })

    if (!record && loading.isLoading) {
        return <LoadingSpinner />
    }
    
    if (!record) {
        return <></>
    }

    return <RecordViewDetail record={record}>
        <LocationBar title='Collection'>
            <CloseButton closePath={`/map/collections/${props.collectionId}`}>Close</CloseButton>
        </LocationBar>

        {props.children}
    </RecordViewDetail>
}

export default observer(CollectionRecordView)