import React, { useEffect } from "react"
import {observer} from "mobx-react-lite"
import {useStore} from "../../models/RootStore"

const CollectionContainer = (props) => {
    const {collection, setCollectionSlug} = useStore()

    useEffect(() => {
        setCollectionSlug(props.collectionId)
    }, [])

    if (!collection) {
        return <></>
    }

    return <>
        {props.children}
    </>
}
export default observer(CollectionContainer)
