import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { motion, AnimatePresence } from "framer-motion"
import Image from "../Shared/Image";

// Styles
import styles from "../Shared/ItemCard.module.scss"

const CollectionCard = (props) => {

    return (
        <AnimatePresence>
            <motion.div className={styles.ItemCard} key="modal" initial={{opacity: 0}} animate={{opacity: 1}} exit={{opacity: 0}} transition={{duration: 0.5}}>
                <Link to={props.path}>

                    {props.collection.image.url &&
                    <Image {...props.collection.image}
                           parameters={{
                               ar: '3:2'
                           }}
                           options={{
                               minWidth: 200,
                               maxWidth: 512
                           }}
                    />
                    }

                    {(!props.collection.image || (props.collection.image && !props.collection.image.url)) &&
                    <div className={styles.imageSubstitute}>
                        <span>{props.collection.excerpt}</span>
                    </div>
                    }

                    <section>
                        <h3>{props.collection.name}</h3>
                        <div className={styles.typeIndicator}>Collection</div>
                        <div className={styles.description}>
                            <p>{props.collection.excerpt}</p>
                        </div>
                    </section>

                </Link>

            </motion.div>
        </AnimatePresence>
    )

}

CollectionCard.defaultProps = {
    parent: `/map`
}

CollectionCard.propTypes = {
    path: PropTypes.string.isRequired,
    collection: PropTypes.object.isRequired
}

export default CollectionCard