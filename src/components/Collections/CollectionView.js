import React, {useEffect} from "react"
import {observer} from "mobx-react-lite"
import {useStore} from "../../models/RootStore"

// Styles
import styles from "../Collections/CollectionView.module.scss"

// Components
import CloseButton from "../Shared/CloseButton"
import Metadata from "../Shared/Metadata"
import TermList from "../Shared/TermList"
import LoadButton from "../Shared/LoadButton"
import CollectionRecordsView from "./CollectionRecordsView"
import LocationBar from "../Shared/LocationBar"
import TrayHero from "../Tray/TrayHero"
import TrayContent from "../Tray/TrayContent"
import Image from "../Shared/Image"
import LoadingSpinner from "../Shared/LoadingSpinner";
import TrayTitle from "../Tray/TrayTitle";
import TrayArticle from "../Tray/TrayArticle";
import RecordsList from "../Records/RecordsList";


const CollectionView = ({location}) => {
    const {previousLocations, setCollectionSlug, unlockPoints, collection, loading} = useStore()

    if (!collection && loading.isLoading) {
        return <LoadingSpinner/>
    }

    if (loading.didFail) {
        return <p>Error</p>
    }

    const closePath = () => {
        return previousLocations['collections']
    }

    const clearStore = () => {
        setCollectionSlug(null)
        unlockPoints()
    }

    return <div className={styles.CollectionView}>

        <TrayContent>

            <LocationBar title='Collection'>
                <Metadata name={collection.owner} recordCount={collection.records.count}/>
                <CloseButton closePath={closePath()} callback={clearStore}>Close
                    Collection</CloseButton>
            </LocationBar>

            <TrayHero>
                {collection.image &&
                <Image {...collection.image}
                       parameters={{
                           ar: '3:2'
                       }}
                       options={{
                           minWidth: 400,
                           maxWidth: 1024
                       }}
                       sizes={{
                           "(min-width:1400px)": "840px",
                           "(max-width: 399px)": "400px",
                           "(max-width: 599px)": "600px",
                           "(max-width: 799px)": "800px",
                           "(max-width:1024px)": "1024px",
                           "(max-width:1199px)": "600px",
                           "(max-width:1399px)": "700px"
                       }}
                />
                }
            </TrayHero>

            <TrayTitle>
                <h1>{collection.name}</h1>
            </TrayTitle>

            <TrayArticle layoutType="centred">
                <div dangerouslySetInnerHTML={{__html: collection.sanitised_content}}></div>
            </TrayArticle>

            {/* TODO - terms will need to be denormalised onto collection for speed.*/}
            {/*{collection.terms.length > 0 &&*/}
            {/*<>*/}
            {/*    <h2>This collection is tagged with:</h2>*/}
            {/*    <TermList terms={collection.terms}/>*/}
            {/*</>*/}
            {/*}*/}

            {collection.records &&
            <>
                <h2>In this collection:</h2>
                <RecordsList>
                    <CollectionRecordsView loading={collection.records.loading} path={location.pathname}
                                           results={collection.records.results}/>
                </RecordsList>
            </>
            }

            {collection.records.hasMore &&
            <LoadButton callback={collection.fetchNextPage}>Load more</LoadButton>
            }

        </TrayContent>

    </div>
}

export const CollectionTrayOverview = observer((props) => {
    const {collection} = useStore()

    if (!collection) {
        return <></>
    }

    return <>
        <h1>{collection.name}</h1>
        {collection.terms.length > 0 &&
        <TermList terms={collection.terms}/>
        }
        <section>
            <span className={styles.title}>Collection</span>
            <Metadata name={collection.owner} recordCount={collection.records.count}/>
        </section>

        {props.children}
    </>
})

export default observer(CollectionView)
