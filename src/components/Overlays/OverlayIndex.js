import React, { useEffect } from "react"
import { observer } from "mobx-react-lite";
import { motion, AnimatePresence } from "framer-motion"
import { useStore } from "../../models/RootStore"

// Styles
import styles from "./OverlayIndex.module.scss";

// Components
import LoadButton from "../Shared/LoadButton"
import OverlayCard from "./OverlayCard"
import RecordsList from "../Records/RecordsList";
import TrayContent from "../Tray/TrayContent";
import TrayTitle from "../Tray/TrayTitle";
import TrayArticle from "../Tray/TrayArticle";

const OverlayIndex = () => {
    const {overlayGroups, overlayGroups: {results}} = useStore()

    useEffect(() => {
        overlayGroups.fetchOverlayGroups()
    }, [results])

    const overlayGroupComponents = overlayGroups.listOverlayGroups.map((result, idx) => {
        const overlayGroup = result
        return <OverlayCard key={`overlay-${idx}`} path={`/map/overlays/${result.slug}`} overlayGroup={overlayGroup}/>
    })

    return <div className={styles.OverlayIndex}>
        <AnimatePresence>
            <motion.div key="modal" initial={{opacity: 0}} animate={{opacity: 1}}
                        exit={{opacity: 0}} transition={{duration: 0.5}}>

                <TrayContent>

                    <TrayTitle>
                        <h1>Overlays</h1>
                    </TrayTitle>

                    <TrayArticle>
                        <p>Choose historical maps and overlay them with information about a range of topics and themes.</p>
                    </TrayArticle>

                    <RecordsList>
                        {overlayGroupComponents}
                    </RecordsList>

                    {overlayGroups.hasMore &&
                    <LoadButton callback={overlayGroups.fetchNextPage}>Load more</LoadButton>
                    }

                </TrayContent>

            </motion.div>
        </AnimatePresence>
    </div>
}

export default observer(OverlayIndex)
