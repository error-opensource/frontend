import React, { useEffect } from "react"
import PropTypes from "prop-types"
import { observer } from "mobx-react-lite"
import { Layer, Source } from "react-map-gl"

const OverlayLayer = ({overlay}) => {
    const ref = React.createRef()

    useEffect(() => {
        if( ref && ref.current._map ) {
            ref.current._map.setPaintProperty(`overlay-${overlay.id}`, `${overlay.overlay_type.source}-opacity`, parseFloat( overlay.layerOpacity ) )
        }
    })
    
    const paintOptions = {}
    paintOptions[`${overlay.overlay_type.source}-opacity`] = overlay.layerOpacity

    return <>
        <Source id={`overlay-${overlay.id}`} type='raster' tiles={[overlay.url]} />

        <Layer
            id={`overlay-${overlay.id}`}
            type={overlay.overlay_type.source}
            source={`overlay-${overlay.id}`}
            paint={paintOptions}
            bounds={overlay.metadata.bounds}
            ref={ref}
            beforeId={overlay.previousOverlayId ? `overlay-${overlay.previousOverlayId}` : ''}
        />
    </>
}

OverlayLayer.propTypes = {
    overlay: PropTypes.object.isRequired
}

export default observer( OverlayLayer )
