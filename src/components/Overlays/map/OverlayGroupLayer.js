import React from "react"
import PropTypes from "prop-types"
import { observer } from "mobx-react-lite"
import OverlayLayer from "./OverlayLayer"

const OverlayGroupLayer = ({overlayGroup}) => {
    const overlays = overlayGroup.overlays.map((overlay) => {
        return <React.Fragment key={`overlay-layer-${overlay.id}`}>
            <OverlayLayer overlay={overlay} />
        </React.Fragment>
    })

    return <>{overlays}</>
}

OverlayGroupLayer.propTypes = {
    overlayGroup: PropTypes.object.isRequired
}

export default observer( OverlayGroupLayer )
