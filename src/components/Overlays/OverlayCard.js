import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { CheckBox } from "grommet"
import { observer } from "mobx-react-lite"
import Image from "../Shared/Image";

// Styles
import styles from "./OverlayCard.module.scss"

const OverlayCard = (props) => {
    return (
        <div className={styles.OverlayCard}>
            <Link to={props.path}>
                {props.overlayGroup.image &&
                <Image {...props.overlayGroup.image}
                       parameters={{
                           ar: '3:2'
                       }}
                       options={{
                           minWidth: 200,
                           maxWidth: 512
                       }}
                />
                }

                <div className={styles.checkboxWrapper}>
                    <CheckBox checked={props.overlayGroup.enabled} onChange={props.overlayGroup.toggle} onClick={(e) => {
                    e.stopPropagation();
                }}/> Select
                </div>
                <section>
                    <h3>{props.overlayGroup.name}</h3>
                    {/* <div dangerouslySetInnerHTML={{__html: props.overlayGroup.sanitised_content}}></div> */}
                    {/*<p>Donec mollis felis sagittis nunc sodales fermentum. Sed velit lacus.</p>*/}
                </section>
            </Link>
        </div>
    )
}

OverlayCard.propTypes = {
    path: PropTypes.string.isRequired,
    overlayGroup: PropTypes.object.isRequired
}

export default observer( OverlayCard )
