import React from "react"
import PropTypes from "prop-types"
import { observer } from "mobx-react-lite"
import OverlayToolRow from "./OverlayToolRow"
import { Draggable } from "react-beautiful-dnd"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faGripLines } from "@fortawesome/free-solid-svg-icons"
import { Link } from "gatsby"

// Styles
import styles from "./OverlayTools.module.scss"

const Handle = (props) => (
    <div {...props}>
        <FontAwesomeIcon icon={faGripLines} />
    </div>
)

const OverlayGroup = ({overlayGroup, index}) => {
    if( overlayGroup.simplified ) {
        let overlays = overlayGroup.overlays.map((overlay) => <OverlayToolRow link={`/map/overlays/${overlayGroup.slug}`} name={overlayGroup.name} overlay={overlay} />)
        return <Draggable draggableId={`id-${overlayGroup.id}`} key={`id-${overlayGroup.id}`} index={index}>
            {(provided) => (
                <div ref={provided.innerRef} {...provided.draggableProps}>
                    <ul className={overlayGroup.simplified ? styles.overlaysListSimple : styles.overlaysListAdvanced}>
                        <Handle {...provided.dragHandleProps} />
                        {overlays}
                    </ul>
                </div>
            )}
        </Draggable>
    }else {
        const overlays = overlayGroup.overlays.map((overlay) => <OverlayToolRow name={overlay.name} overlay={overlay} key={`overlay-tool-row-${overlay.id}`} />)

        return <Draggable draggableId={`id-${overlayGroup.id}`} key={`id-${overlayGroup.id}`} index={index}>
            {(provided) => (
                <div ref={provided.innerRef} {...provided.draggableProps}>
                    <ul className={overlayGroup.simplified ? styles.overlaysListSimple : styles.overlaysListAdvanced}>
                        <li className={`${overlayGroup.closed ? 'closed' : ''}`} onClick={() => overlayGroup.toggleClosed()}>
                            <Handle {...provided.dragHandleProps} />

                            <div className={styles.overlayListItem}>
                                <span className={styles.overlayTitle}>
                                    <Link to={`/map/overlays/${overlayGroup.slug}`}>{overlayGroup.name}</Link>
                                    </span>
                                <span className={styles.dropdownIndicator}></span>
                            </div>
                            
                            <ul className={styles.childOverlaysList}>
                                {overlays}
                            </ul>
                        </li>
                    </ul>
                </div>
            )}
        </Draggable>
    }
}

OverlayGroup.propTypes = {
    overlayGroup: PropTypes.object.isRequired
}

export default observer(OverlayGroup)
