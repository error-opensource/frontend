import React from "react"
import {CheckBox} from "grommet"

import styles from "./OverlayKeeper.module.scss";

const OverlayKeeper = (props) => {

    return <div className={styles.OverlayKeeper}>
        <CheckBox label="Use this overlay" checked={props.checked} onChange={props.overlayGroup.toggle}/>
    </div>

}

export default OverlayKeeper