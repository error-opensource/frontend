import React from "react"
import { observer } from "mobx-react-lite"
import { motion, AnimatePresence } from "framer-motion"
import { useStore } from "../../models/RootStore"
import { DragDropContext, Droppable } from "react-beautiful-dnd"
import { ResponsiveContext } from "grommet"

// Styles
import styles from "./OverlayTools.module.scss"

// Components
import OverlayGroup from "./OverlayGroup"

const OverlayToolList = React.memo( function OverlayToolList( {overlayGroups} ) {
    return overlayGroups.map((overlayGroup, index) => (
        <OverlayGroup overlayGroup={overlayGroup} index={index} key={`overlaygroup-${index}`} />
    ))
})

const OverlayTools = () => {
    const {overlayGroups: {enabledOverlayGroups, setSortOrder}, uiSettings: {overlayToolsVisible, setOverlayToolsVisible, trayVisible}} = useStore()

    const reorderedIds = (list, startIndex, endIndex) => {
        const sortOrder = list.map((og) => og.slug)

        const [removed] = sortOrder.splice(startIndex, 1)
        sortOrder.splice(endIndex, 0, removed)

        return sortOrder
    }

    const onDragEnd = ( result ) => {
        if( !result.destination || result.destination.index === result.source.index ) {
            return
        }

        const sorted = reorderedIds( enabledOverlayGroups, result.source.index, result.destination.index )
        setSortOrder( sorted )
    }

    return <>
        <DragDropContext onDragEnd={onDragEnd}>
            <div className={`${styles.OverlayTools} ${overlayToolsVisible ? styles.isOpen : styles.isClosed} ${trayVisible ? "" : styles.isCentered}`}>

                <AnimatePresence>
                    { overlayToolsVisible && enabledOverlayGroups.length > 0 &&
                        <ResponsiveContext.Consumer>

                            {size => {
                                let transition = {initial: {y: "100%"}, animate: {y: 0}, exit: {y: "100%"}, duration: 0.25}

                                if( !/(desktop|wide|medium)/.test(size) ) { // animate to the right of the screen unless desktop or wide displays
                                    transition = {initial: {x: "100%"}, animate: {x: 0}, exit: {x: "100%"}, duration: 0.25}
                                }

                                return <motion.div key="tools" initial={transition.initial} animate={transition.animate} exit={transition.exit} transition={{ease: "easeInOut", duration: transition.duration}}>
                                    <div className={styles.toolsWrapper}>
                                        <h2>Overlay tools: </h2>
                                        <button className={styles.hideTools} onClick={() => setOverlayToolsVisible(!overlayToolsVisible)}>Hide tools</button>

                                        <Droppable droppableId="overlayGroups">
                                            {(provided, snapshot) => {
                                                return <div ref={provided.innerRef} {...provided.droppableProps} className={snapshot.isDraggingOver ? styles.activeDropzone : styles.inactiveDropzone}>
                                                    <OverlayToolList overlayGroups={enabledOverlayGroups} />
                                                    {provided.placeholder}
                                                </div>
                                            }}
                                        </Droppable>
                                    </div>
                                </motion.div>
                            }}
                        </ResponsiveContext.Consumer>
                    }
                </AnimatePresence>

                {enabledOverlayGroups.length > 0 &&
                    <button className={styles.toggleOverlayTools} onClick={() => setOverlayToolsVisible(!overlayToolsVisible)}>Show Overlay Tools</button>
                }

            </div>
        </DragDropContext>
    </>
}

export default observer(OverlayTools)