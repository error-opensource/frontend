import React from "react"
import PropTypes from "prop-types"
import { observer } from "mobx-react-lite"
import { RangeInput } from "grommet"
import { Link } from "gatsby"

// Styles
import styles from "./OverlayTools.module.scss"

const OverlayToolRow = ({link, overlay, name}) => {

    return <li>
        <div className={styles.overlayListItem}>
            {/*<span className={styles.overlayColor}></span>*/}
            <span className={styles.overlayTitle}>
                { link &&
                    <Link to={link}>{name}</Link>
                }

                { !!!link &&
                    name
                }
            </span>

            <RangeInput disabled={overlay.disabled} min={0.0} max={1.0} step={0.05} value={overlay.value} onChange={(event) => overlay.setValue( parseFloat( event.target.value ))} />

            <span className={styles.overlayToolButtons}>
                <span className={styles.overlayToolLocate} onClick={overlay.focusInMap}></span>
                <span className={`${styles.overlayToolShowHide} ${overlay.disabled ? 'is-hidden' : ''}`} onClick={overlay.toggleEnabled}></span>
            </span>

        </div>
    </li>
}

OverlayToolRow.propTypes = {
    overlay: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    link: PropTypes.string
}

export default observer(OverlayToolRow)
