import React, {useEffect} from "react"
import {observer} from "mobx-react-lite"
import {useStore} from "../../models/RootStore"

// Styles
import styles from "./OverlayView.module.scss"

// Components
import LocationBar from "../Shared/LocationBar";
import CloseButton from "../Shared/CloseButton";
import TrayHero from "../Tray/TrayHero";
import TrayContent from "../Tray/TrayContent";
import LoadingSpinner from "../Shared/LoadingSpinner";
import TrayTitle from "../Tray/TrayTitle";
import TrayArticle from "../Tray/TrayArticle";
import OverlayKeeper from "./OverlayKeeper";
import Image from "../Shared/Image";

const OverlayView = (props) => {
    const {previousLocations, overlayGroups: {loading, fetchOverlayGroup, overlayGroup}} = useStore()

    useEffect(() => {
        fetchOverlayGroup(props.overlayId)
    }, [props.overlayId])

    if (loading.isLoading) {
        return <LoadingSpinner/>
    }

    if (loading.didFail) {
        return <p>Error</p>
    }

    if (!overlayGroup) {
        return <p></p>
    }
    
    const closePath = () => {
        return previousLocations['overlays']
    }

    return (
        <div className={styles.OverlayView}>

            <TrayContent>

                <LocationBar title='Overlay'>
                    <CloseButton closePath={closePath()}>Close Overlay</CloseButton>
                </LocationBar>

                {overlayGroup.image &&
                <TrayHero>
                    <Image {...overlayGroup.image}
                           parameters={{
                               ar: '3:2'
                           }}
                           options={{
                               minWidth: 400,
                               maxWidth: 1024
                           }}
                           sizes={{
                               "(min-width:1400px)": "840px",
                               "(max-width: 399px)": "400px",
                               "(max-width: 599px)": "600px",
                               "(max-width: 799px)": "800px",
                               "(max-width:1024px)": "1024px",
                               "(max-width:1199px)": "600px",
                               "(max-width:1399px)": "700px"
                           }}

                    />
                </TrayHero>
                }

                <TrayTitle>
                    <h1>{overlayGroup.name}</h1>

                    <OverlayKeeper checked={overlayGroup.enabled} overlayGroup={overlayGroup} />

                </TrayTitle>

                <TrayArticle layoutType='centred'>
                    <div dangerouslySetInnerHTML={{__html: overlayGroup.sanitised_content}}/>
                </TrayArticle>

            </TrayContent>

        </div>
    )

}

export default observer(OverlayView)
