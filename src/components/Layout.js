import React, { useEffect } from "react"
import PropTypes from "prop-types"
import { deepMerge } from 'grommet/utils';

import layoutStyles from "./Layout.module.scss"
import { Grommet, Box, ResponsiveContext } from "grommet"
import humapGrommetTheme from "./Global/GrommetTheme.js"
import SEO from './Global/SEO'
import { useStore } from "../models/RootStore"

const themeObject = deepMerge(humapGrommetTheme, {
    global: {
        spacing: "12px", // Just for range slider, but why?
        colors: {
            focus: {
                dark: "#444444",
                light: "#CCCCCC"
            },
        },
        font: {
            family: false, // Removes this declaration entirely
            size: false, // Removes this declaration entirely
            height: false // Removes this declaration entirely
        },
        breakpoints: {
            small: false,
            medium: false,
            large: false,
            smallMobile: {value: 480},
            mobile: {value: 767},
            tablet: {value: 1024},
            desktop: {value: 1680},
            wide: {value: 10000}
        },
        input: {
            weight: 400
        },
    },
    rangeInput: {

        track: {
            //color: "#ff0000",
            height: "2px"
        },
        thumb: {

            //color: "#ff0000"
        }
    },
});


const Layout = ({children}) => {
    const { setLayoutRef, layoutRef } = useStore()

    useEffect(() => {
        if( !layoutRef ) return
    }, [])
    
    /* const data = useStaticQuery(graphql`
       query SiteTitleQuery {
         site {
           siteMetadata {
             title
           }
         }
       }
     `)*/

    return (
        <React.Fragment>
            <SEO />
            {/*<Grommet full theme={themeObject} themeMode="dark" className="darkMode">*/}
            <Grommet full theme={themeObject}>
                <ResponsiveContext.Consumer>
                    { size => (
                        <Box direction="row" flex fill overflow="auto" className={layoutStyles.Layout} ref={setLayoutRef} data-responsivecontext={size}>
                            {children}
                        </Box>
                    )}
                </ResponsiveContext.Consumer>
            </Grommet>
        </React.Fragment>
    )
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
}

export default Layout