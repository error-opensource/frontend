import React from "react"
import { graphql } from "gatsby"
import humapGrommetTheme from "../Global/GrommetTheme.js"
import { deepMerge } from 'grommet/utils'
import Page from "../Pages/Page"
import HeroContent from "../Pages/HeroContent";
import Logo from "../Pages/Logo";
import Navigation from "../Pages/Navigation";
import MainFooter from "../Pages/MainFooter";
import SiteHeader from "../Pages/SiteHeader";
import SiteWrapper from "../Pages/SiteWrapper";
import SecondaryFooter from "../Pages/SecondaryFooter";
import ContentAreaWithSidebar from "../Pages/ContentAreaWithSidebar";
import TertiaryFooter from "../Pages/TertiaryFooter";
import SEO from '../Global/SEO'
import QuickStarts from "../Pages/QuickStarts";

const ContentPageLayout = ({data}) => {
    const page = data.humap.pages_by_pk
    const quick_starts = data.humap.page_quick_starts

    return (
        <Page>
            <SEO/>
            <SiteWrapper>
                <SiteHeader>
                    <Logo/>
                    <Navigation/>
                </SiteHeader>
                <HeroContent image={page.image} name={page.name} description={page.description}/>
                <ContentAreaWithSidebar content={page.sanitised_content} cta_blocks={page.cta_blocks}/>
                {/*<MainFooter />*/}
                {quick_starts && quick_starts.length > 0 &&
                    <QuickStarts content={page.highlighted_content_intro} quick_starts={quick_starts}/>
                }
                <SecondaryFooter/>
                <TertiaryFooter/>
            </SiteWrapper>
        </Page>
    )
}

export default ContentPageLayout

// const themeObject = deepMerge(humapGrommetTheme, {
// 	global: {
// 		spacing: "12px", // Just for range slider, but why?
// 		colors: {
// 			focus: {
// 				dark: "#444444",
// 				light: "#CCCCCC"
// 			},
// 		},
// 		font: {
// 			family: false, // Removes this declaration entirely
// 			size: false, // Removes this declaration entirely
// 			height: false // Removes this declaration entirely
// 		},
// 		breakpoints: {
// 			small: false,
// 			medium: false,
// 			large: false,
// 			smallMobile: { value: 480 },
// 			mobile: { value: 767 },
// 			tablet: { value: 1024 },
// 			desktop: { value: 1680 },
// 			wide: { value: 10000 }
// 		},
// 		input: {
// 			weight: 400
// 		},
// 	},
// 	rangeInput: {

// 		track: {
// 			//color: "#ff0000",
// 			height: "2px"
// 		},
// 		thumb: {

// 			//color: "#ff0000"
// 		}
// 	},
// });

export const query = graphql`
  query ContentPageQuery($pageId: Humap_bigint!) {
    humap {
      pages_by_pk(id: $pageId) {
				description
				name
				sanitised_content
				highlighted_content_intro
				image {
					url
					name
					description
					credit
				}
				cta_blocks {
					url
					title
					sanitised_content
					button_text
				}
			},
			page_quick_starts(where: {page_id: {_eq: $pageId}}) {
				quick_start {
					title
					content
					url
					image {
						url
						name
					}
				}
			}
    }
  }
`