import React from "react"
import humapGrommetTheme from "../Global/GrommetTheme.js"
import {deepMerge} from 'grommet/utils';
import {graphql} from 'gatsby'

// Components
import Page from "../Pages/Page"
import FeatureDemo from "../Pages/FeatureDemo";
import HeroHome from "../Pages/HeroHome";
import HomeIntroduction from "../Pages/HomeIntroduction";
import HighlightedContent from "../Pages/HighlightedContent";
import Logo from "../Pages/Logo";
import Navigation from "../Pages/Navigation";
import MainFooter from "../Pages/MainFooter";
import SiteHeader from "../Pages/SiteHeader";
import SiteWrapper from "../Pages/SiteWrapper";
import SecondaryFooter from "../Pages/SecondaryFooter";
import TertiaryFooter from "../Pages/TertiaryFooter";
import SEO from '../Global/SEO'
import QuickStarts from "../Pages/QuickStarts";

const HomePageLayout = ({data}) => {
  const page = data.humap.pages_by_pk
  const quick_starts = data.humap.page_quick_starts
  const title = data.humap.site_meta[0].title

  return (
    <Page>
      <SEO/>
      <SiteWrapper>
        <SiteHeader>
          <Logo/>
          <Navigation/>
        </SiteHeader>
        <HeroHome title={title} image={page.image} description={page.description} images={page.images}/>
        <HomeIntroduction content={page.sanitised_content}/>
        <FeatureDemo/>
        <QuickStarts content={page.highlighted_content_intro} quick_starts={quick_starts}/>
        <SecondaryFooter/>
        <TertiaryFooter/>
      </SiteWrapper>
    </Page>
  )
}

export default HomePageLayout

const themeObject = deepMerge(humapGrommetTheme, {
  global: {
    spacing: "12px", // Just for range slider, but why?
    colors: {
      focus: {
        dark: "#444444",
        light: "#CCCCCC"
      },
    },
    font: {
      family: false, // Removes this declaration entirely
      size: false, // Removes this declaration entirely
      height: false // Removes this declaration entirely
    },
    breakpoints: {
      small: false,
      medium: false,
      large: false,
      smallMobile: {value: 480},
      mobile: {value: 767},
      tablet: {value: 1024},
      desktop: {value: 1680},
      wide: {value: 10000}
    },
    input: {
      weight: 400
    },
  },
  rangeInput: {

    track: {
      //color: "#ff0000",
      height: "2px"
    },
    thumb: {

      //color: "#ff0000"
    }
  },
});

export const query = graphql`
  query HomePageQuery($pageId: Humap_bigint!) {
    humap {
      pages_by_pk(id: $pageId) {
				description
				name
				sanitised_content
				highlighted_content_intro
				search_image {
					url
					name
				}
				images {
      		image {
       			url
        		name
      		}
    		}
			},
			page_quick_starts(where: {page_id: {_eq: $pageId}}) {
				quick_start {
					title
					content
					url
					image {
						url
						name
					}
				}
            }
            site_meta: site_metas {
                title: site_title
            }
    }
  }
`

