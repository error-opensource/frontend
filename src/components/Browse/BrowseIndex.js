import React, { useEffect } from "react"
import { observer } from "mobx-react-lite";
import { motion, AnimatePresence } from "framer-motion"
import { useStore } from "../../models/RootStore"
import { useMatch } from "@reach/router"

// Styles
import styles from "./BrowseIndex.module.scss"

// Components
import RecordCard from "../Records/RecordCard"
import LoadButton from "../Shared/LoadButton"
import CollectionCard from "../Collections/CollectionCard"
import RecordsList from "../Records/RecordsList"
import TrayContent from "../Tray/TrayContent"
import LoadingSpinner from "../Shared/LoadingSpinner"
import TrayTitle from "../Tray/TrayTitle"

import { contentTypeValues } from "../../models/ContentTypes"

const BrowseIndex = ({contentTypes, title, ...props}) => {
    const {adjustLayoutScroll, fetchNextPage, loading, cardItems, reloadCardItems, searchResults} = useStore()

    useEffect(() => {
        adjustLayoutScroll('bottom')
    })

    useEffect(() => {
        const contentTypeFilterValue = contentTypes ? [].concat(contentTypes) : contentTypeValues
        cardItems.filter.setSearchParameter('type', contentTypeFilterValue)
        
        searchResults.filter.reset()
        reloadCardItems()
    }, [])

    if (loading.didFail) {
        return <p>Error</p>
    }

    const results = cardItems.results.map((result, idx) => {
        switch (result.type) {
            case 'record':
                return <RecordCard key={`card-${idx}-${result.slug}`} path={`/map/records/${result.slug}`}
                                    record={result}/>
            case 'collection':
                return <CollectionCard key={`card-${idx}-${result.slug}`} path={`/map/collections/${result.slug}`}
                                    collection={result}/>
            case 'trail':
                return <RecordCard key={`card-${idx}-${result.slug}`} path={`/map/trails/${result.slug}`}
                                    record={result} resultType={`Trail`} />
            default:
                return <></>
        }
    })
    
    return <div className={styles.BrowseIndex}>

        <AnimatePresence>

            <motion.div key="modal" initial={{opacity: 0}} animate={{opacity: 1}}
                        exit={{opacity: 0}} transition={{duration: 0.5}}>

                <TrayContent>

                    <TrayTitle>
                        <h1>{title}</h1>
                    </TrayTitle>

                    <RecordsList>{/*layoutType='list'*/}
                        {!loading.isLoading && 
                            results
                        }
                        
                        {results.length === 0 && !loading.isLoading && 
                            <p>There are no items in this area.</p>
                        }
                    </RecordsList>

                    {cardItems.hasMore && !loading.isLoading && 
                        <LoadButton callback={fetchNextPage}>Load more</LoadButton>
                    }

                    {loading.isLoading &&
                        <LoadingSpinner/>
                    }

                </TrayContent>

            </motion.div>

        </AnimatePresence>

    </div>
}

// gatsby's static build query
// export const gatsbyQuery = graphql`
// query BrowseQuery {
//     humap {
//         records {
//             id
//             name
//             published_at
//             lonlat
//             sanitised_content
//             date_to
//             date_from
//         }
//     }
// }
// `

export default observer(BrowseIndex)
