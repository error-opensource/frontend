import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { motion, AnimatePresence } from "framer-motion"
import Image from "../Shared/Image";

// Styles
import styles from "../Shared/ItemCard.module.scss"

const RecordCard = (props) => {
    return (
        <AnimatePresence>
            <motion.div className={`${styles.ItemCard} ${props.layoutType}`} key="modal" initial={{opacity: 0}} animate={{opacity: 1}} exit={{opacity: 0}} transition={{duration: 0.25}}>
                <Link to={props.path} onClick={props.onClick}>
                    {props.record.image && props.record.image.url &&
                        <Image {...props.record.image}
                               parameters={{
                                   ar: '3:2'
                               }}
                               options={{
                                   minWidth: 200,
                                   maxWidth: 512
                               }}
                        />
                    }

                    {props.children}

                    {props.trailOrdinalPosition &&
                    <div className={styles.trailOrdinalPosition}>
                        <span>{props.trailOrdinalPosition}</span>
                    </div>
                    }

                    {(!props.record.image || (props.record.image && !props.record.image.url)) &&
                        <div className={styles.imageSubstitute}>
                            <span>excerpt here: {props.record.excerpt}</span>
                        </div>
                    }

                    <section>
                        <h3>{props.record.name}</h3>
                        <div className={styles.typeIndicator}>{props.resultType || RecordCard.resultType}</div>
                        <div className={styles.description}>
                            <p>{props.record.excerpt}</p>
                        </div>
                    </section>
                </Link>
            </motion.div>
        </AnimatePresence>
    )
}

RecordCard.defaultProps = {
    parent: `/map`,
    resultType: 'Record',
    onClick: () => {}
}

RecordCard.propTypes = {
    parent: PropTypes.string,
    path: PropTypes.string.isRequired,
    record: PropTypes.object.isRequired,
    resultType: PropTypes.string,
    onClick: PropTypes.func
}

export default RecordCard