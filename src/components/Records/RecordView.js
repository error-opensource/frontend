import React, { useEffect } from "react"
import PropTypes from "prop-types"
import { useMatch, navigate } from "@reach/router"
import { observer } from "mobx-react-lite"
import { useStore } from "../../models/RootStore"

// Styles
import styles from "./RecordView.module.scss"

// Components
import RecordCard from "./RecordCard"
import Metadata from "../Shared/Metadata"
import TermList from "../Shared/TermList"
import MediaList from "../Shared/MediaList"
import CloseButton from "../Shared/CloseButton"
import MediaGallery from "../Shared/MediaGallery"
import RecordTextContent from "./RecordTextContent"
import ShareButtons from "../Shared/ShareButtons"
import RecordsList from "./RecordsList"
import LocationBar from "../Shared/LocationBar"
import TrayContent from "../Tray/TrayContent";
import LoadingSpinner from "../Shared/LoadingSpinner";
import TrayTitle from "../Tray/TrayTitle";
import TrayArticle from "../Tray/TrayArticle";
import RecordTrayHero from "./RecordTrayHero";
import RecordTrailsSelector from "../Trails/RecordTrailsSelector"

export const RecordViewDetail = observer(({record, ...props}) => {
    const {siteMeta} = useStore()
    
    return <div className={styles.RecordView}>
        <TrayContent>
            {props.children}

            <RecordTrayHero record={record} />

            <TrayTitle>
                <h1>{record.name}</h1>
            </TrayTitle>

            <TrayArticle layoutType='centred'>

                {siteMeta &&
                <ShareButtons excerpt={record.excerpt} URL={typeof(window) === 'object' && window.location.href} site_title={siteMeta.site_title}/>
                }

                {record.user &&
                <div className={styles.createdBy}>
                    Created by {record.user.name}
                </div>
                }

                <RecordTextContent>
                    {record.sanitised_content &&
                    <div dangerouslySetInnerHTML={{__html: record.sanitised_content}}></div>
                    }
                </RecordTextContent>

            </TrayArticle>

            {record.terms.length > 0 &&
            <>
                <h2>This record is tagged with:</h2>
                <TermList terms={record.terms}/>
            </>
            }

            {record.attached_images.results.length > 0 &&
            <>
                {record.attached_images.results.length > 1 &&
                //The first image is always the record image; don't show it again at the bottom
                <>
                    <h2>Gallery</h2>
                    <MediaList id={record.slug} attached_images={record.attached_images}/>
                </>
                }
                <MediaGallery id={record.slug} record={record} attached_images={record.attached_images.results}/>
            </>
            }

            {record.external_cta &&
            <div className="smallCta">
                <h2>
                    {record.external_cta.name}
                </h2>
                <p dangerouslySetInnerHTML={record.external_cta.sanitised_content}></p>
            </div>
            }

            {record.related && record.related.length &&
            <>
                <h2>Related records</h2>

                <RecordsList>
                    {record.related.map((relatedRecord) => <RecordCard key={relatedRecord.id}
                                                                       path={`/map/records/${relatedRecord.slug}`}
                                                                       record={relatedRecord}/>)}
                </RecordsList>
            </>
            }
        </TrayContent>
    </div>
})

const RecordView = (props) => {
    const trailRecordRouteMatch = useMatch('/map/trails/:trailId/stops/:stopId')

    const {previousLocations, setRecordSlug, record, loading} = useStore()

    useEffect(() => {
        setRecordSlug(props.recordId)
    })

    if (!record && loading.isLoading) {
        return <LoadingSpinner />
    }

    if (loading.didFail) {
        return <p>Error</p>
    }

    if (!record) {
        return <></>
    }

    const closePath = () => {
        return previousLocations['records']
    }

    return <RecordViewDetail record={record}>
        <LocationBar title='Record'>
            {!trailRecordRouteMatch && process.env.GATSBY_TRAILS_ENABLED && 
                <RecordTrailsSelector record={record} />
            }
            <CloseButton closePath={closePath()}>Close record</CloseButton>
        </LocationBar>
    </RecordViewDetail>
}

RecordView.defaultProps = {
    afterResourceLoadedCallback: () => {}
}

RecordView.propTypes = {
    record: PropTypes.object,
    afterRenderCallback: PropTypes.func
}

export const RecordTrayOverview = observer((props) => {
    const {record} = useStore()

    if (!record) {
        return <></>
    }

    return <>
        <h1>{record.name}</h1>
        {record.terms.length > 0 &&
        <TermList terms={record.terms}/>
        }
        <section>
            <span className={styles.title}>Record</span>
            {record.owner &&
            <Metadata name={record.owner}/>
            }
        </section>
    </>
})

export default observer(RecordView)

