import React from "react"
import { observer } from "mobx-react-lite";
import { paginatedRecordsQuery, PER_PAGE } from "../../queries/RecordQueries"

// Components
import ContentOptions from "../Shared/ContentOptions"
import LoadButton from "../Shared/LoadButton"
import RecordCard from "./RecordCard"
import RecordsList from "./RecordsList";
import LoadingSpinner from "../Shared/LoadingSpinner";

const RecordsContainer = ({children}) => (
    <>
        {children}
    </>
)

const RecordsIndex = (props) => {
    const {loading, error, data, fetchMore} = useQuery(paginatedRecordsQuery, {
        variables: {
            limit: PER_PAGE,
            offset: 0
        },
        fetchPolicy: 'cache-and-network'
    })

    const fetchNextPage = () => (
        fetchMore({
            variables: {
                offset: data.records.length
            },
            updateQuery: (prev, {fetchMoreResult}) => {
                if (!fetchMoreResult) return prev

                return Object.assign({}, prev, {
                    records: [...prev.records, ...fetchMoreResult.records]
                })
            }
        })
    )

    if (loading) {
        return <LoadingSpinner />
    }

    if (error) {
        return <p>Error...</p>
    }

    const records = data.records.map((record, i) => <RecordCard key={`card-${i}-${record.id}`} path={`${record.id}`} record={record}/>)

    return <>

        <h1>In this area </h1>

        <RecordsList>
            <RecordsContainer>
                {records}
            </RecordsContainer>
        </RecordsList>

        {
            (data.records.length / PER_PAGE < data.records_aggregate.aggregate.count / PER_PAGE) &&
            <LoadButton callback={fetchNextPage}>Load more</LoadButton>
        }

    </>
}

// gatsby's static build query
// export const gatsbyQuery = graphql`
// query RecordsQuery {
//     humap {
//         records {
//             id
//             name
//             published_at
//             lonlat
//             sanitised_content
//             date_to
//             date_from
//         }
//     }
// }
// `

export default observer(RecordsIndex)
