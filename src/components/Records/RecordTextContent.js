import React from "react"

// Styles
import styles from "./RecordTextContent.module.scss"

const RecordTextContent = ({children}) => (

    <div className={styles.RecordTextContent}>
        {children}
    </div>
)

export default RecordTextContent