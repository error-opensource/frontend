import React from "react"
import TrayHero from "../Tray/TrayHero";
import { observer } from 'mobx-react-lite';

// Styles
import styles from "./RecordTrayHero.module.scss"
import Image from "../Shared/Image";
import { useStore } from "../../models/RootStore";

const RecordTrayHero = ({record}) => {

    const {uiSettings: {setMediaGalleryVisible}} = useStore()

    if (!record.heroAttachment) {
        return <></>
    }

    const renderHeroVideoEmbed = () => {
        return (
            <div dangerouslySetInnerHTML={{__html: record.heroAttachment.html}}/>
        )
    }

    const renderHeroImage = () => {
        return (
            <>
                <a onClick={(e) => {
                    setMediaGalleryVisible(true);
                    e.preventDefault();
                }}>

                    <Image {...record.heroAttachment}
                           parameters={{
                               ar: '3:2'
                           }}
                           options={{
                               minWidth: 400,
                               maxWidth: 1024
                           }}
                           sizes={{
                               "(min-width:1400px)": "840px",
                               "(max-width: 399px)": "400px",
                               "(max-width: 599px)": "600px",
                               "(max-width: 799px)": "800px",
                               "(max-width:1024px)": "1024px",
                               "(max-width:1199px)": "600px",
                               "(max-width:1399px)": "700px"
                           }}
                    />

                    <span className={styles.galleryIndicator}>
            {
                record.attached_images.results.length === 1 &&
                'Enlarge image'
            }
                        {
                            record.attached_images.results.length > 1 &&
                            'View gallery'
                        }
          </span>

                </a>
            </>
        )
    }

    const renderHeroAttachment = () => {
        switch (record.hero_attachment_type) {
            case "image":
                return renderHeroImage();
            case "video_embed":
                return renderHeroVideoEmbed();
            default:
                return null;
        }
    }

    return (
        <TrayHero>
            {renderHeroAttachment()}
        </TrayHero>
    )


}

export default observer(RecordTrayHero)