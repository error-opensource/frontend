import React from "react"

// Styles
import styles from "./RecordsList.module.scss"

const RecordsList = (props) => {

    return <div className={`${styles.RecordsList} RecordsList--${props.layoutType}`}> {/* Grid, List */}
        {props.children}
    </div>

}

export default RecordsList