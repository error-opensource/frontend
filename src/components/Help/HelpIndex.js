import React, { useEffect } from "react"
import { graphql } from "gatsby"
import { motion, AnimatePresence } from "framer-motion"
import { useStore } from "../../models/RootStore"

// Styles
import styles from "./HelpIndex.module.scss"

// Components
import TrayContent from "../Tray/TrayContent";
import TrayTitle from "../Tray/TrayTitle";
import TrayArticle from "../Tray/TrayArticle";

// default Gatsby build-time query
export const BUILD_QUERY = graphql`
query HelpHeadingQuery {
  site {
    siteMetadata {
      title
    }
  }
}`

const HelpIndex = () => {
    const {adjustLayoutScroll, helpTrayView} = useStore()


    useEffect(() => {
        adjustLayoutScroll('top')
    })

    if (!helpTrayView) {
        return <></>
    }

    return <div className={styles.HelpIndex}>
        <AnimatePresence>

            <motion.div key="modal" initial={{opacity: 0}} animate={{opacity: 1}}
                        exit={{opacity: 0}} transition={{duration: 0.5}}>

                <TrayContent>

                    <TrayTitle>
                        <h1>{helpTrayView.title}</h1>
                    </TrayTitle>

                    <TrayArticle>
                        <div dangerouslySetInnerHTML={{__html: helpTrayView.sanitised_content}}></div>
                    </TrayArticle>

                </TrayContent>

            </motion.div>

        </AnimatePresence>
    </div>
}

export default HelpIndex
