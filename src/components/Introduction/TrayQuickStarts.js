import React from "react"

const TrayQuickStart = ({title, content, url, image}) => {
    return <div>
        <a href={url}>
            {title}
            {content}

            <img src={image.cardImage} alt={title} />
        </a>
    </div>
}

export default TrayQuickStart
