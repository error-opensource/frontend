import React, { useEffect } from "react";
import Image from "../Shared/Image";

// Styles
import styles from "./Quickstart.module.scss";

const Quickstart = ({title, content, image, url}) => {

    return <div className={styles.Quickstart}>
        <a href={url}>
            <div className={styles.image}><Image {...image} parameters={{w: 1680}}/></div>
            <div className={styles.textContent}>
                <h3>{title}</h3>
                <p>{content}</p>
            </div>
        </a>
    </div>
}

export default Quickstart








