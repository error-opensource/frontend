import React, { useEffect } from "react"
import { motion, AnimatePresence } from "framer-motion"
import { graphql } from "gatsby"
import { observer } from "mobx-react-lite";
import { useStore } from "../../models/RootStore"

// Styles
import styles from "./IntroductionIndex.module.scss"

// Components
import RecordCard from "../Records/RecordCard"
import CategoryList from "../Shared/CategoryList"
import RecordsList from "../Records/RecordsList";
import Quickstart from "./Quickstart";
import TrayContent from "../Tray/TrayContent";
import TrayTitle from "../Tray/TrayTitle";
import TrayArticle from "../Tray/TrayArticle";

// default Gatsby build-time query
export const BUILD_QUERY = graphql`
query IntroductionHeadingQuery {
  site {
    siteMetadata {
      title
    }
  }
}`

const IntroductionIndex = () => {
    const {adjustLayoutScroll, introductionTrayView} = useStore()

    useEffect(() => {
        adjustLayoutScroll('top')
    })

    if (!introductionTrayView) {
        return <></>
    }

    const recordsList = (records) => {
        return records.map((record) => <RecordCard key={record.slug} path={`/map/records/${record.slug}`}
                                                   record={record}/>)
    }

    const quickStarts = (quick_starts) => {
        return quick_starts.map((quick_start) => <Quickstart key={`quick-start-${quick_start.id}`} {...quick_start} />)
    }

    return <div className={styles.IntroductionIndex}>

        <AnimatePresence>

            <motion.div key="modal" initial={{opacity: 0}} animate={{opacity: 1}}
                        exit={{opacity: 0}} transition={{duration: 0.5}}>

                <TrayContent>

                    <TrayTitle>
                        <h1>{introductionTrayView.title}</h1>
                    </TrayTitle>

                    <TrayArticle>
                        <div className={styles.introductionText}
                             dangerouslySetInnerHTML={{__html: introductionTrayView.sanitised_content}}></div>
                    </TrayArticle>

                    {introductionTrayView.hasTaxonomies &&
                    <TrayArticle>
                        <h2>{introductionTrayView.taxonomies_title}</h2>
                        <p dangerouslySetInnerHTML={{__html: introductionTrayView.taxonomies_description}}></p>
                        <CategoryList taxonomies={introductionTrayView.taxonomies}/>
                    </TrayArticle>
                    }

                    {introductionTrayView.hasRecords &&
                    <TrayArticle>
                        <h2>{introductionTrayView.records_title}</h2>
                        <div className={styles.records}>
                            <RecordsList>
                                {recordsList(introductionTrayView.records)}
                            </RecordsList>
                        </div>
                    </TrayArticle>
                    }

                    {introductionTrayView.hasQuickStarts &&
                    <TrayArticle>
                        <h2>{introductionTrayView.quick_starts_title}</h2>
                        <p dangerouslySetInnerHTML={{__html: introductionTrayView.quick_starts_description}}></p>
                        {quickStarts(introductionTrayView.quick_starts)}
                    </TrayArticle>
                    }

                </TrayContent>

            </motion.div>

        </AnimatePresence>

    </div>
}

export default observer(IntroductionIndex)
