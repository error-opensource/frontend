import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { Link } from "@reach/router"
import { useStore } from "../../models/RootStore"

// Styles
import styles from "./TrailView.module.scss"

// Components
import CloseButton from "../Shared/CloseButton"
import Metadata from "../Shared/Metadata"
import TrailRecordsView from "./TrailRecordsView"
import LocationBar from "../Shared/LocationBar"
import TrayHero from "../Tray/TrayHero"
import TrayContent from "../Tray/TrayContent"
import Image from "../Shared/Image"
import TrayTitle from "../Tray/TrayTitle"
import TrayArticle from "../Tray/TrayArticle"
import RecordsList from "../Records/RecordsList"
import TrailMeta from "./TrailMeta";

export const TrailView = ({location, trailId}) => {
    const {previousLocations, setTrailSlug, unlockPoints, trail, mapViewport} = useStore()

    const hasPoints = trail ? trail.hasPoints : false
    
    useEffect(() => {
        if( hasPoints ) {
            mapViewport.setCenter(trail.boundingViewport)
        }
    }, [hasPoints])

    if (!trail) {
        return <></>
    }

    const closePath = () => {
        return previousLocations['trails']
    }

    const clearStore = () => {
        setTrailSlug(null)
        unlockPoints()
    }

    return <div className={styles.TrailView}>

        <TrayContent>
            <LocationBar title='Trail'>
                <CloseButton closePath={closePath()} callback={clearStore}>Close Trail</CloseButton>
            </LocationBar>

            <TrailMeta>

                <ul>
                    <li>{trail.stops.results.length} stops</li>

                    {trail.distance &&
                    <li>{trail.distance}</li>
                    }

                    {trail.duration &&
                    <li>{trail.duration}</li>
                    }
                </ul>

                <nav>
                    <Link className='start' to={`/map/trails/${trailId}/stops/1`} onClick={() => trail.goToFirstStop()}>Start <span>trail</span></Link>
                </nav>

            </TrailMeta>

            <TrayHero>
                {trail.image &&
                <Image {...trail.image}
                       parameters={{
                           ar: '3:2'
                       }}
                       options={{
                           minWidth: 400,
                           maxWidth: 1024
                       }}
                       sizes={{
                           "(min-width:1400px)": "840px",
                           "(max-width: 399px)": "400px",
                           "(max-width: 599px)": "600px",
                           "(max-width: 799px)": "800px",
                           "(max-width:1024px)": "1024px",
                           "(max-width:1199px)": "600px",
                           "(max-width:1399px)": "700px"
                       }}
                />
                }
            </TrayHero>

            <TrayTitle>
                <h1>{trail.name}</h1>
            </TrayTitle>

            <TrayArticle layoutType="centred">
                <div dangerouslySetInnerHTML={{__html: trail.sanitised_content}}></div>
            </TrayArticle>

            {trail.stops &&
            <>
                <h2>On this trail</h2>
                <RecordsList>
                    <TrailRecordsView
                        loading={trail.stops.loading}
                        path={location.pathname}
                    />
                </RecordsList>
            </>
            }
        </TrayContent>
    </div>
}

export default observer(TrailView)
