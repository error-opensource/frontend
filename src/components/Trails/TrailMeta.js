import React from "react"

import styles from "./TrailMeta.module.scss"

const TrailMeta = ({children}) => (

    <div className={styles.TrailMeta}>
        {children}
    </div>

)

export default TrailMeta