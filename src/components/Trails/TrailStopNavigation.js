import React from "react"
import { observer } from "mobx-react-lite"
import { Link } from "@reach/router"
import { useStore } from "../../models/RootStore"

export const TrailStopNavigation = ({trailId, children}) => {
    const {trail} = useStore()

    const goBack = (event) => {
        if (!trail.hasPreviousStop) {
            event.preventDefault()
            return false
        }

        trail.goToPreviousStop()
    }

    const goForward = (event) => {
        if (!trail.hasNextStop) {
            event.preventDefault()
            return false
        }

        trail.goToNextStop()
    }

    if (!trail) {
        return <></>
    }

    return <nav>
        <Link className={`previous${trail.hasPreviousStop ? '' : ' is-disabled'}`} to={`/map/trails/${trailId}/stops/${trail.hasPreviousStop ? trail.previousStop.ordinalPosition : ''}`} onClick={goBack}>&lt; Prev</Link>
        <span className='currentStop'>
            {trail.currentStop.ordinalPosition}
        </span>
        <Link className={`next${trail.hasNextStop ? '' : ' is-disabled'}`} to={`/map/trails/${trailId}/stops/${trail.hasNextStop ? trail.nextStop.ordinalPosition : ''}`} onClick={goForward}>Next &gt;</Link>
    </nav>

}

export default observer(TrailStopNavigation)
