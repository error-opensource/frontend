import React, { useState, useEffect } from "react"
import { observer } from "mobx-react-lite"
import { fetchRecordTrailsQuery } from "../../queries/TrailQueries"
import {Link} from "gatsby"
import styles from "./RecordTrailsSelector.module.scss"

const RecordTrailsSelector = (props) => {
    const [trails, setTrails] = useState([])
    const [trailsVisible, setTrailsVisible] = useState(false)

    useEffect(() => {
        let mounted = true
        
        async function fetchData(params) {
            const response = await fetchRecordTrailsQuery(params) 
            const results = response.results.map((result) => ({...result.trail}))

            if( mounted ) {
                setTrails(results)
            }
        }
        
        fetchData({slug: props.record.slug})

        return () => mounted = false;
    }, [])
    
    const recordInTrail = trails.length === 1
    const recordInMultipleTrails = trails.length > 1 
    
    const recordTrails = trails.map((trail) => <li key={`record-trail-${trail.slug}`}><Link to={`/map/trails/${trail.slug}`}>{trail.name}</Link></li>)
    const recordTrailsContainer = <>
        In&nbsp;
        <div className={styles.showDropdown} onClick={() => setTrailsVisible(!trailsVisible)}>
            {trails.length} trails
        </div>
        
        {trailsVisible && 
            <ul className={styles.dropdown}>
                {recordTrails}
            </ul>
        }
    </>

    const recordTrailLink = recordInTrail ? <>
        In <Link className={styles.singleTrailLink} to={`/map/trails/${trails[0].slug}`}>1 trail</Link>
    </> : <></>

    return <div className={styles.RecordTrailsSelector}>
        {recordInTrail && recordTrailLink}
        {recordInMultipleTrails && recordTrailsContainer}
    </div>
}

export default observer(RecordTrailsSelector)
