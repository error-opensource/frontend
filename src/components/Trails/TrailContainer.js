import React, {useEffect} from "react"
import {observer} from "mobx-react-lite"
import {useStore} from "../../models/RootStore"

import LoadingSpinner from "../Shared/LoadingSpinner"

const TrailContainer = ({trailId, children}) => {
    const {setTrailSlug, loading} = useStore()

    useEffect(() => {
        setTrailSlug( trailId )

        return(() => {
            setTrailSlug(null)
        })
    }, [])

    if ( loading.isLoading ) {
        return <LoadingSpinner/>
    }

    if (loading.didFail) {
        return <p>Error</p>
    }

    return <>
        {children}
    </>
}

export const TrailTrayOverview = observer((props) => {
    const {trail} = useStore()

    if (!trail) {
        return <></>
    }

    return <>
    </>
})

export default observer(TrailContainer)