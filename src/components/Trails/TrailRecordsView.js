import React from "react"
import RecordCard from "../Records/RecordCard"
import { observer } from "mobx-react-lite"

import { useStore } from "../../models/RootStore"

const TrailRecordsView = ({ path, loading }) => {
    const { trail } = useStore()

    if (loading.didFail) {
        return <p>Error</p>
    }

    if (loading.isLoading) {
        return <></>
    }

    const handleOnClick = (trailStopId) => {
        trail.setCurrentStop(`${trailStopId}`)
    }

    const records = trail.stops.results.map((result) => {
        return (
            <RecordCard key={result.slug} path={`${path}/stops/${result.ordinalPosition}`} trailOrdinalPosition={result.ordinalPosition} record={result} onClick={(event) => handleOnClick(result.ordinalPosition)} />
        )
    })

    return <>{records}</>
}

export default observer(TrailRecordsView)
