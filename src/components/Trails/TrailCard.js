import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { motion, AnimatePresence } from "framer-motion"
import Image from "../Shared/Image";

// Styles
import styles from "../Shared/ItemCard.module.scss"

const TrailCard = (props) => {
    console.log(props.trail.image)
    return (
        <AnimatePresence>
            <motion.div className={`${styles.ItemCard} ${props.layoutType}`} key="modal" initial={{opacity: 0}} animate={{opacity: 1}} exit={{opacity: 0}} transition={{duration: 0.25}}>
                <Link to={props.path} onClick={props.onClick}>
                    {props.trail.image && props.trail.image.url &&
                        <Image {...props.trail.image}
                               parameters={{
                                   ar: '3:2'
                               }}
                               options={{
                                   minWidth: 200,
                                   maxWidth: 512
                               }}
                        />
                    }

                    {props.children}

                    {(!props.trail.image || (props.trail.image && !props.trail.image.url)) &&
                        <div className={styles.imageSubstitute}>
                            <span>{props.trail.excerpt}</span>
                        </div>
                    }
                    
                    <section>
                        <h3>{props.trail.name}</h3>
                        <div className={styles.typeIndicator}>{props.resultType || TrailCard.resultType}</div>
                        <div className={styles.description}>
                            <p>{props.trail.excerpt}</p>
                        </div>
                    </section>
                </Link>
            </motion.div>
        </AnimatePresence>
    )
}

TrailCard.defaultProps = {
    parent: `/map`,
    resultType: 'Trail',
    onClick: () => {}
}

TrailCard.propTypes = {
    parent: PropTypes.string,
    path: PropTypes.string.isRequired,
    trail: PropTypes.object.isRequired,
    resultType: PropTypes.string,
    onClick: PropTypes.func
}

export default TrailCard