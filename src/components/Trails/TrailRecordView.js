import React from "react"
import { observer } from "mobx-react-lite"
import { RecordViewDetail } from "../Records/RecordView"
import { useStore } from "../../models/RootStore"

// Components
import CloseButton from "../Shared/CloseButton"
import LocationBar from "../Shared/LocationBar"
import TrailMeta from "./TrailMeta";
import { Link } from "@reach/router";
import TrayContent from "../Tray/TrayContent"
import TrayHero from "../Tray/TrayHero"
import TrailStopNavigation from "./TrailStopNavigation";
import Image from "../Shared/Image"

export const TrailRecordView = ({trailId, stopNumber, ...props}) => {
    const {trail, mapViewport: {setPitchAndBearing}} = useStore()

    if (!trail) {
        return <></>
    }

    if (!trail.currentStop && trail.stops.loading.isLoading) {
        return <></>
    } else if (!trail.currentStop && !trail.stops.loading.isLoading) {
        trail.setCurrentStop(stopNumber)
        return <></>
    } else if (!trail.currentStop) {
        return <></>
    }

    return <RecordViewDetail record={trail.currentStop}>
        <LocationBar title='Trail Stop'>
            <CloseButton closePath={`/map/trails/${trailId}`}>Quit trail</CloseButton>
        </LocationBar>

        <TrailMeta>
            <h3>{trail.name}</h3>
            <TrailStopNavigation trailId={trailId} />
        </TrailMeta>

        <TrayHero>
                {trail.currentStop && trail.currentStop.image &&
                <Image {...trail.currentStop.image}
                       parameters={{
                           ar: '3:2'
                       }}
                       options={{
                           minWidth: 400,
                           maxWidth: 1024
                       }}
                       sizes={{
                           "(min-width:1400px)": "840px",
                           "(max-width: 399px)": "400px",
                           "(max-width: 599px)": "600px",
                           "(max-width: 799px)": "800px",
                           "(max-width:1024px)": "1024px",
                           "(max-width:1199px)": "600px",
                           "(max-width:1399px)": "700px"
                       }}
                />
                }
            </TrayHero>

        {props.children}

    </RecordViewDetail>
}

export default observer(TrailRecordView)