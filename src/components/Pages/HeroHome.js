import React from "react"
import Carousel from './Carousel'
// Styles
import styles from "./HeroHome.module.scss"
import 'react-slideshow-image/dist/styles.css'

const HeroHome = (props) => {
  return (
    <div className={styles.HeroHome}>

      {props.images.length > 0 &&
      <Carousel images={props.images}/>
      }

      <div className={styles.textContent}>
        <h1>{props.title}</h1>
        <p>{props.description}</p>
      </div>
    </div>
    )
}

export default HeroHome