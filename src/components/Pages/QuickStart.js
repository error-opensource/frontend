import React, { useEffect } from "react"
import { motion, useAnimation } from "framer-motion";
import { useInView } from "react-intersection-observer";

// Styles
import styles from "./QuickStart.module.scss"

// Components
import Image from "../Shared/Image";

const QuickstartAnimationVariants = {
    visible: {opacity: 1, y: 0, transition: {duration: 1}},
    hidden: {opacity: 0, y: 150},
};

const QuickStart = (props) => {

    const controls = useAnimation();

    const [ref, inView] = useInView();
    useEffect(() => {
        if (inView) {
            controls.start("visible");
        }
    }, [controls, inView]);

    const sizes = {
        "(min-width:1520px)": "220px",
        "(min-width:768px)": "25vw",
        "(min-width:0px)": "100vw"
    }

    return <motion.div className={styles.QuickStart}
                ref={ref}
                animate={controls}
                initial="hidden"
                variants={QuickstartAnimationVariants}>

        <a href={props.quick_start.url}>
            <div className={styles.image}>
                <Image {...props.quick_start.image} sizes={sizes} />
            </div>
            <div className={styles.textContent}>
                <h3>{props.quick_start.title}</h3>
                <p>{props.quick_start.content}</p>
            </div>
        </a>
    </motion.div>

}

export default QuickStart