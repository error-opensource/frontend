import React from "react"
import { Fade } from 'react-slideshow-image';

// Styles
import styles from "./FeatureDemo.module.scss"

// Images
import ImageRecord from "../../assets/images/features-record.jpg";
import ImageCollection from "../../assets/images/features-collection.jpg";
import ImageOverlay from "../../assets/images/features-overlay.jpg";

// Icons
import IconCollection from "../../assets/images/marketingpage-icon-collection.svg";
import IconOverlay from "../../assets/images/marketingpage-icon-overlay.svg";
import IconPin from "../../assets/images/marketingpage-icon-pin.svg";


const slideshowProperties = {
    indicators: i => (<div className="indicator"><span>{i + 1}</span></div>)
}

const FeatureDemo = ({children}) => (

    <div className={styles.FeatureDemo}>
        <Fade {...slideshowProperties} easing='ease' arrows={false} duration='8000' pauseOnHover={false}>
            <div className={styles.feature}>
                <div className={styles.image}>
                    <img src={ImageRecord}/>
                </div>
                <div className={styles.textContent}>
                    <div className={styles.icon}><img src={IconPin} /></div>
                    <h2>Browse a living history through a multitude of documented <strong>records</strong></h2>
                    <div className={styles.description}>
                        <p>Records are the heartbeat of the site, and are associated with a geographical place on a map. A record can be an event, a building, a person, an object, a feature in a landscape. It can include photos, audio and video. They are the story of the famous, infamous, landmark, extraordinary, or ordinary.</p>
                    </div>
                    <a href="/map" className={styles.openMapButton}>Open map</a>
                </div>
            </div>
            <div className={styles.feature}>
                <div className={styles.image}>
                    <img src={ImageCollection}/>
                </div>
                <div className={styles.textContent}>
                    <div className={styles.icon}><img src={IconCollection} /></div>
                    <h2><strong>Collect</strong> records into meaningful stories and themes</h2>
                    <div className={styles.description}>
                        <p>Collections are a set of records collected together by subject or theme such as landmarks or buildings, museum objects, or notable persons. A rich contextual story based on many records can be expressed across many locations within the map.</p>
                    </div>
                    <a href="/map" className={styles.openMapButton}>Open map</a>
                </div>
            </div>
            <div className={styles.feature}>
                <div className={styles.image}>
                    <img src={ImageOverlay}/>
                </div>
                <div className={styles.textContent}>
                    <div className={styles.icon}><img src={IconOverlay} /></div>
                    <h2>Observe context with <strong>overlays</strong> of historic maps and plans</h2>
                    <div className={styles.description}>
                        <p>An overlay can be a historic map, a network or a plan. It can be a collection of maps, historical or geographical. You can switch map overlays on and off, make them transparent or opaque, and reorder them. Map overlays can add historical and quantitive value when shown in conjunction with records and collections on the map.</p>
                    </div>
                    <a href="/map" className={styles.openMapButton}>Open map</a>
                </div>
            </div>
        </Fade>
    </div>

)

export default FeatureDemo