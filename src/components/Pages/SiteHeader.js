import React from "react"
import styles from "./SiteHeader.module.scss"

const SiteHeader = ({ children }) => (

	<div className={styles.SiteHeader}>
		{children}
	</div>

)

export default SiteHeader