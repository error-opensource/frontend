import React from "react"
import {Link} from "gatsby"

const NavigationItem = (props) => (
  <li>
    <Link to={`/${props.page.slug}`} onClick={props.onClick} >
      {props.page.name}
    </Link>
  </li>
)

export default NavigationItem