import React from "react"
import styles from "./Article.module.scss"

const Article = (props) => (
	<div className={styles.Article}>
		<div dangerouslySetInnerHTML={{ __html: props.content }}></div>
	</div>
)

export default Article