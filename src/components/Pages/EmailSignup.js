import React from "react"
import { Button, TextInput } from "grommet"
import styles from "./EmailSignup.module.scss"

const EmailSignup = ({ children }) => (

	<div className={styles.EmailSignup}>
		<h2>Email newsletter</h2>
		<form>
			<TextInput placeholder="Email address" value="" />
			<Button type="submit" primary label="Submit" />
		</form>
		<p>Quisque congue nisi velit, vel accumsan massa molestie et. Nunc scelerisque, nisl eu venenatis blandit, ante eros molestie justo, at.</p>
		{children}
	</div>

)

export default EmailSignup