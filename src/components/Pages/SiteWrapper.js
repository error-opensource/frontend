import React from "react"
import styles from "./SiteWrapper.module.scss"

const SiteWrapper = ({ children }) => (

	<div className={styles.SiteWrapper}>
		{children}
	</div>

)

export default SiteWrapper