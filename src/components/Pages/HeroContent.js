import React from "react"
import styles from "./HeroContent.module.scss"
import Image from "../Shared/Image";

const HeroContent = (props) => (


	<div className={styles.HeroContent}>
		{ props.image &&
		<Image {...props.image} parameters={{w: 1680}} />
		}
		<div className={styles.textContent}>
			<h1>{props.name}</h1>
			<p>{props.description}</p>
		</div>
	</div>
)

export default HeroContent