import React from "react"
import humapGrommetTheme from "../Global/GrommetTheme.js"
import { Grommet } from "grommet"
import { deepMerge } from 'grommet/utils';
import styles from "./Page.module.scss"

const themeObject = deepMerge(humapGrommetTheme, {
    global: {
        spacing: "12px", // Just for range slider, but why?
        colors: {
            focus: {
                dark: "#444444",
                light: "#CCCCCC"
            },
        },
        font: {
            family: false, // Removes this declaration entirely
            size: false, // Removes this declaration entirely
            height: false // Removes this declaration entirely
        },
        breakpoints: {
            small: false,
            medium: false,
            large: false,
            smallMobile: {value: 480},
            mobile: {value: 767},
            tablet: {value: 1024},
            desktop: {value: 1680},
            wide: {value: 10000}
        },
        input: {
            weight: 400
        },
    },
    rangeInput: {

        track: {
            //color: "#ff0000",
            height: "2px"
        },
        thumb: {

            //color: "#ff0000"
        }
    },
});

const Page = ({children}) => (
    <Grommet full theme={themeObject}>
        <div className={styles.Page}>
            {children}
        </div>
    </Grommet>

)

export default Page