import React from "react"

// Styles
import styles from "./HighlightedContent.module.scss"

// Components
import Record from "./Record";

const HighlightedContent = (props) => {
	return <>

		<div className={styles.HighlightedContent}>
			<div className={styles.textContent}>
				<h2>Highlighted Content</h2>
				<div dangerouslySetInnerHTML={{ __html: props.content }}></div>
			</div>
			<div className={styles.contentItems}>
				<Record />
				<Record />
				<Record />
				<Record />
				<Record />
				<Record />
			</div>{
			}
		</div>
	</>

}

export default HighlightedContent