import React from "react"
import { observer } from "mobx-react-lite"
import { useStore } from "../../models/RootStore";

// Styles

import styles from "./Hamburger.module.scss"

const Hamburger = ({ children }) => {
	const { uiSettings: { mobileMenuVisible, setMobileMenuVisible } } = useStore()

	return <div className={styles.Hamburger}>
		<button className={`${styles.hamburger} ${ mobileMenuVisible ? styles.isActive : '' }`} type="button" onClick={(e) => { setMobileMenuVisible(!mobileMenuVisible); e.preventDefault(); }}>
			<span className={styles.hamburgerBox}>
				<span className={styles.hamburgerInner}></span>
			</span>
		</button>
	</div>

}

export default observer(Hamburger)