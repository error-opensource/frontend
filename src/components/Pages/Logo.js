import React from "react"
import PropTypes from 'prop-types'
import styles from "./Logo.module.scss"
import { graphql, useStaticQuery, Link } from "gatsby";
import Image from "../Shared/Image";

const Logo = ({width, height, ...props}) => {
    const data = useStaticQuery(
        graphql`
        query TenantSettings {
            humap {
                tenant_settings {
                    has_site_logo
                    site_logo_url
                }
            }
        }
    `
    )

    const settings = data.humap.tenant_settings[0]

    if (!settings.has_site_logo) {
        return null;
    }

    const logo_url = settings.site_logo_url
    
    return (
        <div className={styles.Logo}>
            <Link to='/'>
                <Image
                    parameters={{w: width, h: height, fit: 'clip'}}
                    url={logo_url}
                    sizes={{}}
                />
            </Link>
        </div>
    )
}

Logo.propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    logoName: PropTypes.string
}

Logo.defaultProps = {
    logoName: 'site'
}

export default Logo