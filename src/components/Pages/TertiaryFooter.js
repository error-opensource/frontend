import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import humapLogoAsset from "../../assets/images/logo-humap-white.svg";

// Styles
import styles from "./TertiaryFooter.module.scss"

const TertiaryFooter = () => {

	const data = useStaticQuery(graphql`
    query TertiaryFooterQuery {
      humap {
        pages(where: {page_location: {_eq: "footer"}}, order_by: {position: asc}) {
					slug
					name
				}
      }
    }
	`)

	const navigationPages = data.humap.pages

	return <div className={styles.TertiaryFooter}>
		<div className={styles.humapSignature}>
			<a href="https://humap.me">
				Made with <b>Humap</b> <img src={humapLogoAsset} alt="Logo" />
			</a>
		</div>
		<ul className={styles.navigation}>
			{navigationPages.map(page => (
				<li key={JSON.stringify(page)}>
					<a href={`/${page.slug}`}>{page.name}</a>
				</li>
			))}
		</ul>
	</div>

}

export default TertiaryFooter