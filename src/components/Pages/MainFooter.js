import React from "react"
import { useStaticQuery, graphql } from 'gatsby'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faFacebook, faTwitter, faInstagram } from "@fortawesome/free-brands-svg-icons"

import styles from "./MainFooter.module.scss"

const MainFooter = ({ children }) => {
	const data = useStaticQuery(graphql`
    query MainFooterQuery {
      humap {
        site_metas {
					contact_email
					facebook
					instagram
					twitter
					website
				}
      }
    }
	`)
	
	const siteMeta = data.humap.site_metas[0] || {}

	return <div className={styles.MainFooter}>
		<div className={styles.contactDetails}>
			<h2>Get in touch</h2>

				{ siteMeta.website &&
					<a href="#">{siteMeta.website}</a>
				}
			
				{ siteMeta.contact_phone && 
					<a href="#">{siteMeta.contact_phone}</a>
				}

			
			<ul className={styles.socialLinks}>
				{ siteMeta.facebook && 
					<li><a href={siteMeta.facebook}><span>Facebook</span><FontAwesomeIcon icon={faFacebook} /></a></li>
				}

				{ siteMeta.twitter && 
					<li><a href={siteMeta.twitter}><span>Twitter</span><FontAwesomeIcon icon={faTwitter} /></a></li>
				}

				{siteMeta.instagram && 
					<li><a href={siteMeta.instagram}><span>Instagram</span><FontAwesomeIcon icon={faInstagram} /></a></li>
				}
			</ul>
		</div>

	</div>

	}

export default MainFooter