import * as React from "react";
import {useState, useEffect} from "react";
import {motion, AnimatePresence, useAnimation} from "framer-motion";
import {wrap} from "popmotion";

// Styles
import styles from "./Carousel.module.scss"

// Components
import Image from "../Shared/Image";

const Carousel = (props) => {
  const [page, setPage] = useState(0);

  useEffect(() => {
    const timer = setTimeout(() => {
      paginate()
    }, 5000)
    return () => {
      clearTimeout(timer)
    }
  })

  const images = props.images.map((i) => i.image);

  // the wrap method from popmotion allows us to roll over and over the same images
  const imageIndex = wrap(0, images.length, page);

  const paginate = () => {
    setPage(page + 1);
  };

  const sizes = {
    "(min-width:1520px)": "1440px",
    "(min-width:0px)": "100vw"
  }

  return (
    <div className={styles.Carousel}>
      <AnimatePresence initial={false}>
        <motion.div className={styles.slide}
          key={page}
          initial={{opacity: 0}}
          animate={{
            opacity: 1,
          }}
          exit={{opacity: 0}}
          transition={{
            duration: 0.5
          }}
        >
          <Image {...images[imageIndex]} sizes={sizes} options={{maxWidth:1440}}/>

        </motion.div>
      </AnimatePresence>
    </div>

  )
};

export default Carousel