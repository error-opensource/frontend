import React from "react"
import {Link} from "gatsby"
import {ResponsiveContext} from "grommet"
import {observer} from "mobx-react-lite"
import styles from "./Navigation.module.scss"
import Hamburger from "./Hamburger"
import {useStore} from "../../models/RootStore"
import {useStaticQuery, graphql} from "gatsby"
import NavigationItem from "./NavigationItem"

const Navigation = () => {
    const data = useStaticQuery(graphql`
		query NavigationQuery {
			humap {
				pages(where: {page_location: {_eq: "main"}}, order_by: {position: asc}) {
					slug
					name
				}
			}
		}
	`)
    const {uiSettings: {mobileMenuVisible, setMobileMenuVisible}} = useStore()
    const navigationPages = data.humap.pages

    const closeMenu = (e) => {
        setMobileMenuVisible(!mobileMenuVisible)
    }

    return <>

        <ResponsiveContext.Consumer>
            {responsive =>
                responsive === "smallMobile" || responsive === "mobile" || responsive === "tablet" ? (

                    <>
                        <Hamburger/>

                        <div className={`${styles.NavigationMobile} ${mobileMenuVisible ? styles.isActive : ''}`}>
                            <ul>
                                <li><Link to="/" onClick={closeMenu}>Home</Link></li>
                                {navigationPages.map(page => (
                                    <NavigationItem onClick={closeMenu} key={JSON.stringify(page)} page={page}/>
                                ))}
                            </ul>
                            <a className={styles.highlightedLink} href="/map">Explore the map</a>
                        </div>
                    </>

                ) : (

                    <div className={styles.Navigation}>
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            {navigationPages.map(page => (
                                <NavigationItem page={page}/>
                            ))}
                        </ul>
                        <a className={styles.highlightedLink} href="/map">Explore the map</a>
                    </div>

                )
            }
        </ResponsiveContext.Consumer>

    </>

}

export default observer(Navigation)