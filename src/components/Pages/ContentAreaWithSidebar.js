import React from "react"
import styles from "./ContentAreaWithSidebar.module.scss"
import Article from "./Article";
import QuickStart from "./QuickStart"
import SidebarCTA from "./SidebarCTA";

const ContentAreaWithSidebar = (props) => (

	<div className={styles.ContentAreaWithSidebar}>
		<div className={styles.contentArea}>
			<Article content={props.content} />
		</div>
		<div className={styles.sidebar}>
			<>
				{props.cta_blocks.map(cta_block => (
					<SidebarCTA
						title={cta_block.title}
						content={cta_block.sanitised_content}
						button_text={cta_block.button_text}
						url={cta_block.url}
					/>
				))}
			</>
			

		</div>
	</div>

)

export default ContentAreaWithSidebar