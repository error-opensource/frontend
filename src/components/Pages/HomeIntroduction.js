import React from "react"
import styles from "./HomeIntroduction.module.scss"
import Search from "./Search";

const HomeIntroduction = (props) => (

	<div className={styles.HomeIntroduction}>
		<div className={styles.textContent}>
			<div dangerouslySetInnerHTML={{ __html: props.content }}></div>
		</div>
		<Search />
	</div>

)

export default HomeIntroduction