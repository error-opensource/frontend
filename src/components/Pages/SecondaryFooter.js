import React from "react"
import { useStaticQuery, graphql } from 'gatsby'
import Image from "../Shared/Image";

import styles from "./SecondaryFooter.module.scss"

const SecondaryFooter = () => {
    const data = useStaticQuery(graphql`
    query SecondaryFooterQuery {
      humap {
        site_metas {
					site_meta_logos {
						logo {
							link_to
							logo_size
							image {
								url
								name
								metadata
							}
						}
					}
				}
      }
    }
	`)
    const siteMeta = data.humap.site_metas[0] || {}

    return <div className={styles.SecondaryFooter}>
        {siteMeta.site_meta_logos &&
        <ul className={styles.sponsorLogos}>
            {siteMeta.site_meta_logos.map(logo => (
                <li key={JSON.stringify(logo.logo.image)} className={logo.logo.logo_size}>
                    <a href={logo.logo.link_to}>
                        <Image {...logo.logo.image} sizes={{}} parameters={{w: 300, fm: 'png'}}/>
                    </a>
                </li>
            ))}
        </ul>
        }



    </div>

}

export default SecondaryFooter