import React from "react"

// Styles
import styles from "./QuickStarts.module.scss"

// Components
import QuickStart from "./QuickStart"


const QuickStarts = (props) => {

    return <>

        <div className={styles.QuickStarts}>
            <div className={styles.textContent}>
                <h2>Explore by theme</h2>
                {props.content &&
                <div dangerouslySetInnerHTML={{__html: props.content}}></div>
                }
            </div>
            <div className={styles.quickStarts}>
                <>
                    {props.quick_starts.map(quick_start => <QuickStart quick_start={quick_start.quick_start} key={quick_start.quick_start.url || Math.random()}/>)}
                </>
            </div>
        </div>
    </>

}

export default QuickStarts