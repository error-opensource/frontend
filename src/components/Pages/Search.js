import React from "react"
import { Button, TextInput } from "grommet"
import styles from "./Search.module.scss"

const Search = () => {
	const [value, setValue] = React.useState('');
	return (
		<div className={styles.Search}>
			<h2>Begin your search</h2>
			<form action="/map/search/results" method="GET">
				<TextInput placeholder="A place, person, or thing" name="q" value={value} onChange={event => setValue(event.target.value)} />
				<Button type="submit" primary label="Search" />
			</form>
		</div>
	)

}

export default Search