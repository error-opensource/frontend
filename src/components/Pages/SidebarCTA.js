import React from "react"

import styles from "./SidebarCTA.module.scss"

const SidebarCTA = ({title, content, button_text, url}) => (

    <div className={styles.SidebarCTA}>
        <h2>{title}</h2>
        <div className={styles.description} dangerouslySetInnerHTML={{__html: content}} />
        <a href={url}>{button_text}</a>
    </div>

)

export default SidebarCTA