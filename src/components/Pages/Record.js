import React from "react"

// Styles
import styles from "./Record.module.scss"

// Components
import PlaceholderImage from "../Shared/PlaceholderImage";

const Record = ({children}) => (

    <div className={styles.Record}>
        <PlaceholderImage width="500" height="200"/>
        <div className={styles.textContent}>
            <span className={styles.type}>Record</span>
            <h3>Duis Bibendum eu Magna id Egestas Proin</h3>
            <p>Donec libero nisi, sollicitudin faucibus bibendum sit amet, scelerisque eget urna. Curabitur vulputate
                urna auctor, fermentum nunc eu, vulputate ex. Etiam mauris mauris, blandit et mi id, vestibulum.</p>
        </div>
    </div>

)

export default Record