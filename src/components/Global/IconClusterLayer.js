import { CompositeLayer } from '@deck.gl/core';
import { IconLayer } from '@deck.gl/layers';
import Supercluster from 'supercluster';

export function getIconName(size, pin, zoom, maxZoom, options = {}) {
    // things we need to determine the name of the sprite:
    // size (zero-padded)
    // type (type of content, record, collection etc)
    // zoom and maxzoom
    
    const atMaxZoom = (zoom === maxZoom);
    const markerType = pin.properties.customSpriteType || pin.properties.type
    const spriteNameParts = []

    let alwaysNumbered = options.alwaysNumbered
    let highlighted = options.isHighlighted || false

    if( pin.properties.customSpriteType && pin.properties.customSpriteType === 'trailpin' ) {
        alwaysNumbered = true
        size = pin.properties.ordinalPosition
    }

    // TYPE OF PIN
    if (size === 1 || alwaysNumbered ) {
        spriteNameParts.push(markerType); // the individual marker type
    } else {
        if (atMaxZoom) {
            spriteNameParts.push('pin'); // the numbered pin when at max zoom
        } else {
            spriteNameParts.push('point'); // the cluster point
        }
    }
    
    // NUMBER
    let spriteNumber = ""
    if( size > 1 && size < 10 || alwaysNumbered ) {
        spriteNumber = String(size).padStart(2,'0');
    }else if( size >= 10 && size < 100 ) {
        spriteNumber = `${Math.floor(size/10)}0`;
    }else if (size >= 100) {
        spriteNumber = '100'
    }

    if (spriteNumber.length) {
        spriteNameParts.push(spriteNumber);
    }

    if( highlighted ) {
        spriteNameParts.push('highlighted')
    }

    return spriteNameParts.join('-')
}

function getIconSize(size) {
    //return Math.min(100, size) / 100 + 1;
    return 1;
}

export default class IconClusterLayer extends CompositeLayer {
    shouldUpdateState({ changeFlags }) {
        return changeFlags.somethingChanged;
    }

    updateState({ props, oldProps, changeFlags }) {
        const rebuildIndex = changeFlags.dataChanged || props.sizeScale !== oldProps.sizeScale;
        const clusterRadius = props.clusterRadius ? (props.sizeScale / props.clusterRadius) : 0

        if (rebuildIndex) {
            const index = new Supercluster({ maxZoom: this.context.deck.viewState.maxZoom+1, radius: clusterRadius });
            
            index.load(
                props.data.map(d => ({
                    geometry: { coordinates: props.getPosition(d) },
                    properties: d
                }))
            );
            this.setState({ index });
        }

        const z = Math.floor(this.context.viewport.zoom);
        if (rebuildIndex || z !== this.state.z) {
            this.setState({
                data: this.state.index.getClusters([-180, -85, 180, 85], z),
                z
            });
        }
    }

    getPickingInfo({ info, mode }) {
        const pickedObject = info.object && info.object.properties;
        if (pickedObject) {
            if (pickedObject.cluster && mode !== 'hover') {
                info.objects = this.state.index
                    .getLeaves(pickedObject.cluster_id, 25)
                    .map(f => f.properties);
            }
            info.object = pickedObject;
        }
        return info;

    }

    renderLayers() {
        const { data } = this.state;
        const { getIcon, iconAtlas, iconMapping, sizeScale, ...options } = this.props;
        
        const _getIcon = d => getIconName(d.properties.cluster ? d.properties.point_count : 1, d, this.context.deck.viewState.zoom, this.context.deck.viewState.maxZoom, options)

        return new IconLayer(
            this.getSubLayerProps({
                id: 'icon',
                data,
                iconAtlas,
                iconMapping,
                sizeScale,
                getIcon: getIcon || _getIcon,
                getPosition: d => d.geometry.coordinates,
                getSize: d => getIconSize(d.properties.cluster ? d.properties.point_count : 1),
            })
        );
    }
}