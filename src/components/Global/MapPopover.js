import React from "react"
import { Popup } from 'react-map-gl';
import { observer } from "mobx-react-lite"
import { Link } from "@reach/router"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCaretDown } from "@fortawesome/free-solid-svg-icons"
import { useStore } from "../../models/RootStore"

// Styles
import styles from "./MapPopover.module.scss"

// Components
import RecordCard from "../Records/RecordCard"
import CollectionCard from "../Collections/CollectionCard"
import TrailCard from "../Trails/TrailCard"

const getPathFor = (type, slug) => {
    let subPath = ''

    switch(type) {
        case 'record': 
            subPath = 'records'
            break
        case 'collection':
            subPath = 'collections'
            break
        case 'trail':
            subPath = 'trails'
            break
        default:
            subPath = type
    }

    return `/map/${subPath}/${slug}`
}

const MapPopoverClusterRow = ( {type, slug, name} ) => {
    return <li>
        <Link to={getPathFor(type, slug)}>{name}</Link>
    </li>
}

const MapPopover = ( {pin} ) => {
    const {setActivePin} = useStore()
    const cluster = pin.object.cluster
    const coordinates = cluster ? pin.objects[0].coordinates : pin.object.coordinates
    let content 

    if( cluster ) {
        const rows = pin.objects.map((result) => <MapPopoverClusterRow key={result.id} type={result.type} slug={result.slug} name={result.name} />)
        content =   <div className={`${pin.objects.length > 9 ? styles.hasMany : ''}`}>
                        <ul>

                            {rows}

                        </ul>
                        <div className={styles.scrollIndicator}><FontAwesomeIcon icon={faCaretDown}/></div>
                    </div>
    }else {
        let component

        switch(pin.object.type) {
            case 'record': 
                component = <RecordCard path={getPathFor(pin.object.type, pin.object.slug)} record={pin.object} />
                break
            case 'collection':
                component = <CollectionCard path={getPathFor(pin.object.type, pin.object.slug)} collection={pin.object} />
                break
            case 'trail': 
                component = <TrailCard path={getPathFor(pin.object.type, pin.object.slug)} trail={pin.object} />
                break
            default:
                component = <></>
        }

        content =   <div>
                        {component}
                    </div>
    }
    
    return <Popup className={cluster ? styles.MapPopoverList : styles.MapPopover} longitude={coordinates[0]} latitude={coordinates[1]} closeButton={true} tipSize={22} closeOnClick={true} anchor="bottom" captureScroll={true} captureClick={true} onClose={()=>setActivePin(null)}>
        {content}
    </Popup>
}

export default observer(MapPopover)