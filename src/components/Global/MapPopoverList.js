import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCaretDown } from "@fortawesome/free-solid-svg-icons"

import styles from "./MapPopoverList.module.scss"

const MapPopoverList = ({}) => (

    <div className={styles.MapPopoverList}>
        <ul>
            <li><a href="#">Cathedral Architecture</a></li>
            <li><a href="#">Post-war cathedral</a></li>
            <li><a href="#">WWII Cathedral</a></li>
            <li><a href="#">Medieval Cathedral</a></li>
            <li><a href="#">St Paul’s Chapel</a></li>
            <li><a href="#">Cathedral East Wing Masonry</a></li>
            <li><a href="#">Cathedral Architecture</a></li>
            <li><a href="#">Post-war cathedral</a></li>
            <li><a href="#">WWII Cathedral</a></li>
            <li><a href="#">Medieval Cathedral</a></li>
            <li><a href="#">St Paul’s Chapel</a></li>
            <li><a href="#">Cathedral East Wing Masonry</a></li>
        </ul>
        <div className={styles.scrollIndicator}><FontAwesomeIcon icon={faCaretDown}/></div>
    </div>

)

export default MapPopoverList