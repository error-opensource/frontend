import React, { useEffect } from "react"
import { Box, CheckBox } from "grommet"
import humapLogoAssetBlue from "../../assets/images/logo-humap-map-blue.svg"

// Styles
import styles from "./Map.module.scss"

import { _MapContext as MapContext, StaticMap, NavigationControl, LinearInterpolator } from "react-map-gl"

import IconClusterLayer, { getIconName } from "./IconClusterLayer"

import { observer } from "mobx-react-lite"
import { useStore } from "../../models/RootStore"
import DeckGL, { GeoJsonLayer } from "deck.gl"
import MapPopover from "./MapPopover"

import OverlayGroupLayer from "../Overlays/map/OverlayGroupLayer"

import mapMarkerSprites from "../../assets/images/sprite@1.png"
import mapMarkerSpritesJSON from "../../assets/images/sprite@1.json"

const MAPTILER_KEY = process.env.GATSBY_MAPTILER_KEY

const MapOverlays = React.memo(({ overlayGroups }) => {
    return overlayGroups.map((overlayGroup) => {
        return <OverlayGroupLayer key={`overlayGroup-${overlayGroup.slug}`} overlayGroup={overlayGroup} />
    })
})

const Map = () => {
    const {
        mapViewport,
        activePin,
        setActivePin,
        setIsHovering,
        getCursor,
        trail,
        siteMeta,
        mapPointsStack: { visibleStackMembers, allVisibleLayerPoints},
        mapViewport: { setMapViewport, setMapRef, zoom, maxZoom },
        uiSettings: { trayVisible, markersVisible, setMarkersVisible },
        overlayGroups: { enabledOverlayGroups },
    } = useStore()

    useEffect(() => {
        if( siteMeta ) {
            mapViewport.setCenter(siteMeta.centroid)
        }

    }, [siteMeta])

    const mapViewportProps = { ...mapViewport, transitionDuration: 500, transitionInterpolator: new LinearInterpolator()}

    const layers = []

    if (trail && trail.path.routesAreSet && trail.path.routesJson.routes.length > 0) {
        // fixme: we only render the first route on the map rather than the others available (ie a shorter but more difficult walk might also be in the results)
        const data = {
            type: "Feature",
            geometry: trail.path.routesJson.routes[0].geometry,
            properties: {
                name: "Primary route",
            },
        }

        const geojsonLayer = new GeoJsonLayer({
            id: "geojson-trail-layer",
            data,
            pickable: true,
            stroked: true,
            filled: true,
            extruded: true,
            lineJointRounded: true,
            lineWidthScale: 1,
            lineWidthMinPixels: 3,
            getFillColor: [0, 0, 0],
            getLineColor: (d) => [0, 175, 54, 200],
            getRadius: 10,
            getLineWidth: 1,
            getElevation: 30,
        })

        layers.push(geojsonLayer)
    }

    const defaultLayerProps = {
        pickable: true,
        getPosition: (point) => point.coordinates,
        iconAtlas: mapMarkerSprites,
        iconMapping: mapMarkerSpritesJSON,
        opacity: 1,
        onClick: (pin) => setActivePin(pin),
        onHover: (info) => setIsHovering(info.picked),
        autoHighlight: true,
        highlightColor: [255, 255, 255, 100],
        sizeScale: 95,
    }

    const getCustomIconName = (point, index) => {
        const options = {
            alwaysNumbered: point.properties.customSpriteType === 'trailpin',
            customSpriteType: point.properties.customSpriteType || point.properties.type,
            isHighlighted: visibleStackMembers[index].highlightAllPoints
        }
        const pointCount = point.properties.cluster ? point.properties.point_count : 1
        const size = options.alwaysNumbered ? point.properties.ordinalPosition : pointCount
        
        return getIconName(size, point, zoom, maxZoom, options)
    }
    
    allVisibleLayerPoints.map((points, index) => {
        const clusterRadius = visibleStackMembers[index].clusterable ? 3 : 0

        layers.push(
            new IconClusterLayer({
                ...defaultLayerProps,
                getIcon: (point) => getCustomIconName(point, index),
                opacity: 1,
                data: points.map((point) => point.toJSON()),
                id: `icon-cluster-${index}-${Math.floor(Math.random()*1000)}`,
                clusterRadius: clusterRadius
            })
        )
    })

    const visiblePoints = (enabledOverlayGroups.length > 0 && !markersVisible) ? [] : layers
    
    return (
        <Box className={`${styles.Map} ${trayVisible ? styles.isHalfWidth : styles.isFullWidth}`}>
            <DeckGL
                layers={ visiblePoints }
                initialViewState={mapViewportProps}
                onViewStateChange={setMapViewport}
                controller={{ dragPan: true }}
                ContextProvider={MapContext.Provider}
                getCursor={() => getCursor}
            >
                <StaticMap
                    reuseMaps
                    mapStyle={`https://api.maptiler.com/maps/68a8c6bf-0173-4e27-9570-08cdb190867a/style.json?key=${MAPTILER_KEY}`}
                    ref={setMapRef}
                    preventStyleDiffing={true}
                >
                    <MapOverlays overlayGroups={enabledOverlayGroups} />
                </StaticMap>

                {activePin && (
                    <div>
                        <MapPopover pin={activePin} />
                    </div>
                )}
                <div className={styles.logo}>
                    <a href="https://humap.me">
                        <img src={humapLogoAssetBlue} alt="Logo" />
                    </a>
                </div>

                {
                    enabledOverlayGroups.length > 0 &&
                    <div className={styles.hideUnrelatedPins}>
                        <CheckBox label="Hide unrelated pins" checked={!markersVisible} onClick={() => {setMarkersVisible(!markersVisible)}} />
                    </div>

                }


                <div className={styles.zoomControl}>
                    <NavigationControl onViewportChange={setMapViewport} showCompass={false} />
                </div>
            </DeckGL>
        </Box>
    )
}

export default observer(Map)
