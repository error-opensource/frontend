import React from "react"
import {Link} from "gatsby"
import {ResponsiveContext} from "grommet"
import {observer} from "mobx-react-lite"
import {useStore} from "../../models/RootStore"
import logoAsset from "../../assets/images/logo-coventry-atlas-icon.png"

// Styles
import styles from "./Toolbar.module.scss"
import Logo from "./Logo";

const Sidebar = () => {
    const {unlockPoints, uiSettings: {trayVisible, setTrayVisible}} = useStore()

    return <>
        <div className={`${styles.Toolbar} ${trayVisible ? styles.isOpen : styles.isClosed}`}>

            <Logo logoName={'map'} width={64} />

            <ul>
                <li><Link className={styles.introButton} to="/map" onClick={unlockPoints}><span><span>Intro</span></span></Link></li>
                <li><Link className={styles.browseButton} to="/map/browse" onClick={unlockPoints}><span><span>Browse</span></span></Link></li>
                <li><Link className={styles.searchButton} to="/map/search"><span><span>Search</span></span></Link></li>
                
                {
                    process.env.GATSBY_OVERLAYS_ENABLED && 
                    <li><Link className={styles.overlaysButton} to="/map/overlays" onClick={unlockPoints}><span><span>Overlays</span></span></Link></li>
                }
                {
                    process.env.GATSBY_TRAILS_ENABLED && 
                    <li><Link className={styles.trailsButton} to="/map/trails" onClick={unlockPoints}><span><span>Trails</span></span></Link></li>
                }

                <li><Link className={styles.helpButton} to="/map/help" onClick={unlockPoints}><span><span>Help</span></span></Link></li>
            </ul>

            <ResponsiveContext.Consumer>
                {responsive =>
                    responsive === "smallMobile" || responsive === "mobile" || responsive === "tablet" ? (
                        <></>
                    ) : (
                        <>
                            <button className={styles.closeTrayButton} onClick={() => setTrayVisible(!trayVisible)}>
                                <span>Close tray</span></button>
                        </>
                    )
                }
            </ResponsiveContext.Consumer>

        </div>
    </>

}

export default observer(Sidebar)




