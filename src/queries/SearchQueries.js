import client from "./client"

const contentTypeAggregateSubQuery = (type) => {
    return `
    ${type}_total: card_item_results_aggregate(args: {query: $query, with_term_ids: $with_term_ids}, where: {
        type: {
            _in: "${type}"
        }
    }) {
        aggregate {
            count
        }
    }
    `
}

/*********
 * Queries to get the card item results to put into the tray
 */

// fetch a collection of CardItemResult rows...
const searchCardItemResultsQuery = (types) => `
query CardItemResults($limit: Int!, $offset: Int!, $type: [String!], $query: String, $with_term_ids: _int4 = "{}") {
    ${types.map((type) => contentTypeAggregateSubQuery(type))}
    total: card_item_results_aggregate(args: {query: $query, with_term_ids: $with_term_ids}, where: {
        type: {
            _in: $type
        }
    }) {
        aggregate {
            count
        }
    }
    results: card_item_results(args: {query: $query, with_term_ids: $with_term_ids}, limit: $limit, offset: $offset, where: {
        type: {
            _in: $type
        }
    }) {
        name, excerpt, slug, location: lonlat, date_from, date_to, type, 
        collection_image: related_collection {
            image {
                name
                url
            }
            records(limit: 1) {
                record {
                    image {
                        url
                        name
                    }
                }
            }
        }
        record_image: related_record {
            image {
                name
                url
            }
        }
    }
}
`

const fetchSearchCardItemResultsQuery = (params) => {
    return client.request(searchCardItemResultsQuery(params.type), params)
}

// fetch a collection of CardItemResult rows...
const searchCardItemResultsQueryWithBounds = (types) => `
query CardItemResults($limit: Int!, $offset: Int!, $type: [String!], $query: String, $centroid: String!, $distance: numeric!, $with_term_ids: _int4 = "{}") {
    ${types.map((type) => contentTypeAggregateSubQuery( type ) )}
    total: card_item_results_aggregate(args: {centroid: $centroid, orderby: "distance", query: $query, with_term_ids: $with_term_ids}, where: {
        distance: { 
            _lte: $distance 
        },
        type: {
            _in: $type
        }
    }) {
        aggregate {
            count
        }
    }
    results: card_item_results(args: {centroid: $centroid, orderby: "distance", query: $query, with_term_ids: $with_term_ids}, limit: $limit, offset: $offset, where: {
        distance: { 
            _lte: $distance 
        },
        type: {
            _in: $type
        }
    }) {
        name, excerpt, slug, location: lonlat, date_from, date_to, type, 
        collection_image: related_collection {
            image {
                name
                url
            }
            records(limit: 1) {
                record {
                    image {
                        url
                        name
                    }
                }
            }
        }
        record_image: related_record {
            image {
                name
                url
            }
        } 
    }
}
`

const fetchSearchCardItemResultsQueryWithBounds = (params) => {
    return client.request(searchCardItemResultsQueryWithBounds(params.type), params)
}


/*********
 * Queries to get the card item results to plot on the map. No pagination.
 */

// fetch a collection of CardItemResult rows for the points
const searchCardItemPointsQuery = (types) => `
query SearchPointData($limit: Int!, $offset: Int!, $type: [String!], $query: String, $with_term_ids: _int4 = "{}") {
    results: card_item_results(args: {query: $query, with_term_ids: $with_term_ids}, where: {
        type: {
            _in: $type
        }
    }) {
        id
        name, 
        point_json,
        type,
        slug,
        location: lonlat,
        collection_image: related_collection {
            image {
                name
                url
            }
            records(limit: 1) {
                record {
                    image {
                        url
                        name
                    }
                }
            }
        }
        record_image: related_record {
            image {
                name
                url
            }
        }
    }
}
`

const fetchSearchCardItemPointsQuery = (params) => {
    return client.request(searchCardItemPointsQuery(params.type), params)
}

// fetch a collection of CardItemResult rows...
const searchCardItemPointsQueryWithBounds = (types) => `
query SearchPointsWithBounds($offset: Int!, $type: [String!], $query: String, $centroid: String!, $distance: numeric!, $with_term_ids: _int4 = "{}") {
    results: card_item_results(args: {centroid: $centroid, orderby: "distance", query: $query, with_term_ids: $with_term_ids}, where: {
        distance: { 
            _lte: $distance 
        },
        type: {
            _in: $type
        }
    }) {
        id
        name, 
        point_json,
        type,
        slug,
        location: lonlat,
        collection_image: related_collection {
            image {
                name
                url
            }
            records(limit: 1) {
                record {
                    image {
                        url
                        name
                    }
                }
            }
        }
        record_image: related_record {
            image {
                name
                url
            }
        } 
    }
}
`

const fetchSearchCardItemPointsQueryWithBounds = (params) => {
    return client.request(searchCardItemPointsQueryWithBounds(params.type), params)
}

export { fetchSearchCardItemResultsQuery, fetchSearchCardItemResultsQueryWithBounds, fetchSearchCardItemPointsQuery, fetchSearchCardItemPointsQueryWithBounds }
