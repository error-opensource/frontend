import { GraphQLClient } from 'graphql-request'
const client = new GraphQLClient(`${process.env['GATSBY_CLIENTSIDE_GRAPHQL-SERVICE_URL']}/v1/graphql`, {
        headers: {"X-Hasura-Tenant-Id": process.env['GATSBY_TENANT_ID']}
    }
)

export const PER_PAGE = 12
export const TRAIL_STOPS_LIMIT = 25 

export default client
