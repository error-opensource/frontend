import client from "./client"

// fetch a collection of CardItemResult rows...
const pointDataResultsQuery = (query) => `
query PointDataResults($type: [String!], ${query ? `query: String!,` : ``} $centroid: String!, $orderby: String, $distance: numeric!) {
    results: card_item_results(args: {${query ? `query: $query,` : ``}centroid: $centroid, orderby: $orderby}, 
        where: { 
            type: { _in: $type },
            distance: { _lte: $distance }
        } 
    ) {
        id
        name, 
        point_json,
        type,
        slug
        collection_image: related_collection {
            image {
                name
                url
            }
            records(limit: 1) {
                record {
                    image {
                        url
                        name
                    }
                }
            }
        }
        record_image: related_record {
            image {
                name
                url
            }
        } 
    }
}
`

const fetchPointDataResultsQuery = (params) => {
    Object.keys(params).map((k) => {
        if(params[k] === null) delete params[k]
        return k
    })
    
    return client.request(pointDataResultsQuery(params.query), params)
}

const collectionPointDataResultsQuery = `
query CollectionPointDataResultsQuery($slug: String!) {
    total: collection_records_aggregate(where: {collection: {slug: {_eq: $slug}}}) {
        aggregate {
            count
        }
    }
    results: collection_records(where: {collection: {slug: {_eq: $slug}}}) {
        record {
            id
            name
            slug
            location: lonlat
            record_image: image {
                url
                name
            }
        }
    }
} 
`

const fetchCollectionPointDataResultsQuery = (params) => {
    return client.request(collectionPointDataResultsQuery, params)
}

export { fetchPointDataResultsQuery, fetchCollectionPointDataResultsQuery }
