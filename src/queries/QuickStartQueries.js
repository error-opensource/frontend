import client from "./client"

const trayQuickStartsQuery = `
query QuickStartsQuery($limit: Int) {
    total: tray_view_quick_starts_aggregate {
        aggregate {
            count
        }
    }
    results: tray_view_quick_starts(limit: $limit, order_by: {updated_at: desc}) {
        quick_start {
            title
            content
            url
            image {
                name
                url
            }
        }
    }
}
`

const fetchTrayQuickStartsQuery = (params) => {
    return client.request(trayQuickStartsQuery, params)
}

export { fetchTrayQuickStartsQuery }
