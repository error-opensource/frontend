import client from "./client"

const trailQuery = `
query TrailQuery($slug: String!) {
    trail: trails(limit: 1, where: {
        slug: {
            _eq: $slug
        }
    }) {
        tenant_id, name, slug, sanitised_content, excerpt, duration, distance,
        image {
            name
            url
            credit
            description
        }
    }
}
`

const fetchTrailQuery = (id) => {
    return client.request(trailQuery, {slug: id})
}

const trailStopsQuery = `
query TrailRecordsQuery($slug: String!, $limit: Int!, $offset: Int!, $_eq: String = "") {
    total: trail_records_aggregate(where: {trail: {slug: {_eq: $slug}}}) {
        aggregate {
            count
        }
    }
    results: trail_records(where: {trail: {slug: {_eq: $slug}}}, limit: $limit, offset: $offset, order_by: {position: asc}) {
        ordinalPosition: position
        record {
            id, name, slug, sanitised_content, excerpt, location: lonlat, date_from, date_to
            image {
                url
                name
            }
            terms: record_terms {
                term: taxonomy_term {
                    id
                    name
                }
            }
        }
    }
}   
`

const fetchTrailRouteStopsQuery = (params) => {
    return client.request(trailStopsQuery, params)
}

const trailPointDataResultsQuery = `
query TrailPointDataResultsQuery($slug: String!) {
    total: trail_records_aggregate(where: {trail: {slug: {_eq: $slug}}}) {
        aggregate {
            count
        }
    }
    results: trail_records(where: {trail: {slug: {_eq: $slug}}}, order_by: {position: asc}) {
        ordinalPosition: position
        record {
            id
            name
            slug
            location: lonlat
            record_image: image {
                url
                name
            }
        }
    }
} 
`

const fetchTrailPointDataResultsQuery = (params) => {
    return client.request(trailPointDataResultsQuery, params)
}

const recordTrailsQuery = `
query TrailRecords($slug:String!) {
    results: trail_records(where:{record:{slug:{_eq: $slug}}}) {
        trail {
            name,
            slug
        }
    }
}
`

const fetchRecordTrailsQuery = (params) => {
    return client.request(recordTrailsQuery, params)
}

export { fetchTrailQuery, fetchTrailRouteStopsQuery, fetchTrailPointDataResultsQuery, fetchRecordTrailsQuery }
