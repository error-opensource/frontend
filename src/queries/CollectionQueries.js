import client from "./client"

const collectionQuery = `
query CollectionQuery($slug: String!) {
    collection: collections(limit: 1, where: {
        slug: {
            _eq: $slug
        }
    }) {
        tenant_id, name, slug, sanitised_content, excerpt,
        image {
            name
            url
            credit
            description
        }
    }
}
`

const fetchCollectionQuery = (id) => {
    return client.request(collectionQuery, {slug: id})
}

const collectionRecordsQuery = `
query CollectionRecordsQuery($slug: String!, $limit: Int!, $offset: Int!, $_eq: String = "") {
    total: collection_records_aggregate(where: {collection: {slug: {_eq: $slug}}}) {
        aggregate {
            count
        }
    }
    results: collection_records(where: {collection: {slug: {_eq: $slug}}}, limit: $limit, offset: $offset) {
        record {
            id
            name
            slug
            location: lonlat
            image {
                url
                name
            }
            terms: record_terms {
                term: taxonomy_term {
                    id
                    name
                }
            }
        }
    }
}   
`

const fetchCollectionRecordsQuery = (params) => {
    return client.request(collectionRecordsQuery, params)
}

export { fetchCollectionQuery, fetchCollectionRecordsQuery }
