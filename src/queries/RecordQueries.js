import client from "./client"

const recordQuery = `
query RecordQuery($slug: String!) {
    record: records(limit: 1, where: {
        slug: {
            _eq: $slug
        }
    }) {
        id, name, slug, sanitised_content, excerpt, location: lonlat, date_from, date_to, hero_attachment_type, hero_attachment_id
        image {
            name
            url
        }
        terms: record_terms {
            term: taxonomy_term {
                id
                name
            }
        }
    }
}
`

const fetchRecordQuery = (id) => {
    return client.request(recordQuery, {slug: id})
}

const recordImagesQuery = `
query RecordImagesQuery($slug: String!) {
    total: record_images_aggregate(where: {record: {slug: {_eq: $slug}}}) {
        aggregate {
            count
        }
    }
    results: record_images(where: {record: {slug: {_eq: $slug}}}) {
        image {
            id
            name
            credit
            description
            url
        }
    }
}
`

const fetchRecordImagesQuery = (params) => {
    return client.request(recordImagesQuery, params)
}


const recordVideoEmbedsQuery = `
query RecordVideoEmbedsQuery($slug: String!) {
  total: record_video_embeds_aggregate(where: {record: {slug: {_eq: $slug}}}) {
    aggregate {
      count
    }
  }
  results: record_video_embeds(where: {record: {slug: {_eq: $slug}}}) {
    video_embed {
      id
      image_url: metadata(path:"thumbnail_url")
      html: metadata(path: "html")
      credit: metadata(path: "author_name")
      url: metadata(path: "url")
      description: metadata(path: "description")
      name: metadata(path: "title")
    }
  }
}
`

const fetchRecordVideoEmbedsQuery = (params) => {
    return client.request(recordVideoEmbedsQuery, params)
}

export { fetchRecordQuery, fetchRecordImagesQuery, fetchRecordVideoEmbedsQuery }
