import client from "./client"

const taxonomiesQuery = `
query TaxonomiesQuery {
    total: taxonomies_aggregate(where: {display_as: {_is_null: false}}) {
        aggregate {
            count
        }
    }
    results: taxonomies(where: {display_as: {_is_null: false}}) {
        id
        name
        slug
        display_as
        terms: taxonomy_terms(where: {queryable_parent_id: {_is_null: true}}) {
            id
            name
            sub_terms: children {
                id
                name
            }
        }
    }
}  
`

const fetchTaxonomiesQuery = (params) => {
    return client.request(taxonomiesQuery, params)
}

export { fetchTaxonomiesQuery }
