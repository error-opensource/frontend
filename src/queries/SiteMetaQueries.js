import client from "./client"

const siteMetaQuery = `
  query SiteMetaQuery {
    results: site_metas {
      contact_email
      facebook
      instagram
      twitter
      website
      site_title
      centroid
    }
  }
`

const fetchSiteMetaQuery = () => {
  return client.request(siteMetaQuery)
}

export { fetchSiteMetaQuery }
