import client from "./client"

const contentTypeAggregateSubQuery = (type) => {
    return `
    ${type}_total: card_item_results_aggregate(args: {query: $query}, where: {
        type: {
            _in: "${type}"
        }
    }) {
        aggregate {
            count
        }
    }
    `
}

// fetch a collection of CardItemResult rows...
const cardItemResultsQuery = (types) => `
query CardItemResults($limit: Int!, $offset: Int!, $type: [String!], $query: String) {
    ${types.map((type) => contentTypeAggregateSubQuery(type))}
    total: card_item_results_aggregate(args: {query: $query}, where: {
        type: {
            _in: $type
        }
    }) {
        aggregate {
            count
        }
    }
    results: card_item_results(args: {query: $query}, limit: $limit, offset: $offset, where: {
        type: {
            _in: $type
        }
    }) {
        name, excerpt, slug, location: lonlat, date_from, date_to, type, excerpt,
        collection_image: related_collection {
            image {
                name
                url
            }
            records(limit: 1) {
                record {
                    image {
                        url
                        name
                    }
                }
            }
        }
        record_image: related_record {
            image {
                name
                url
            }
        } 
    }
}
`

const fetchCardItemResultsQuery = (params) => {
    return client.request(cardItemResultsQuery(params.type), params)
}

// fetch a collection of CardItemResult rows...
const cardItemResultsQueryWithBounds = (types) => `
query CardItemResults($limit: Int!, $offset: Int!, $query: String, $type: [String!], $centroid: String!, $distance: numeric!) {
    ${types.map((type) => contentTypeAggregateSubQuery( type ) )}
    total: card_item_results_aggregate(args: {centroid: $centroid, orderby: "distance", query: $query}, where: {
        distance: { 
            _lte: $distance 
        },
        type: {
            _in: $type
        }
    }) {
        aggregate {
            count
        }
    }
    results: card_item_results(args: {centroid: $centroid, orderby: "distance", query: $query}, limit: $limit, offset: $offset, where: {
        distance: { 
            _lte: $distance 
        },
        type: {
            _in: $type
        }
    }) {
        name, excerpt, slug, location: lonlat, date_from, date_to, type, excerpt,
        collection_image: related_collection {
            image {
                name
                url
            }
            records(limit: 1) {
                record {
                    image {
                        url
                        name
                    }
                }
            }
        }
        record_image: related_record {
            image {
                name
                url
            }
        } 
        trail_image: related_trail { 
            image {
                name
                url
            }
        }
    }
}
`

const fetchCardItemResultsQueryWithBounds = (params) => {
    return client.request(cardItemResultsQueryWithBounds(params.type || []), params)
}

export { fetchCardItemResultsQuery, fetchCardItemResultsQueryWithBounds }
