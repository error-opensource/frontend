import Client from 'imgix-core-js';

const domain = process.env['GATSBY_ASSETS_DOMAIN'];
const ImgixClient = new Client({
  domain: domain,
  includeLibraryParam: false,
  useHTTPS: true
})

export default ImgixClient;