import client from "./client"

const overlayGroupsQuery = `
query OverlayGroups($limit: Int!, $offset: Int!, $slugs: [String!]) {
    total: overlay_groups_aggregate(where: {slug: {_in: $slugs}}) {
        aggregate {
            count
        }
    }
    
    results: overlay_groups(limit: $limit, offset: $offset, where: {slug: {_in: $slugs}}, order_by: {name: asc}) {
        id
        name
        slug
        sanitised_content
        image {
            name
            url
        }
        overlays {
          overlay {
            id
            name
            metadata
            url
            description
            overlay_type {
              description
              name
            }
          }
        }
    }
}
`

const fetchOverlayGroupsQuery = (params) => {
    return client.request(overlayGroupsQuery, params)
}

const overlayGroupQuery = `
query OverlayGroup($slug: String!) {
    result: overlay_groups(limit: 1, where: {slug: {_eq: $slug}}) {
        id
        name
        slug
        sanitised_content
        image {
            name
            url
        }
        overlays {
          overlay {
            id
            name
            metadata
            url
            description
            overlay_type {
              description
              name
            }
          }
        }
    }
}
`

const fetchOverlayGroupQuery = (params) => {
    return client.request(overlayGroupQuery, params)
}

export { fetchOverlayGroupsQuery, fetchOverlayGroupQuery }
