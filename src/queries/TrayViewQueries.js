import client from "./client"

// fetch a collection of CardItemResult rows...
const trayViewQuery = `
query TrayViewQuery($type: String!) {
    results: tray_views(where: {view_type: {_eq: $type}}) {
        id
        title 
        sanitised_content
        taxonomies_description: highlighted_taxonomies_description
        taxonomies_title: highlighted_taxonomies_title
        records_title: highlighted_records_title
        records_description: highlighted_records_description
        quick_starts_title: highlighted_quick_starts_title
        quick_starts_description: highlighted_quick_starts_description
        records {
            record {
                id
                name
                slug
                location: lonlat
                image {
                    name
                    url
                }
            }
        }

        taxonomies {
            taxonomy {
                id
                name
            }
        }

        quick_starts {
            quick_start {
                id
                title
                content
                url
                image {
                    name
                    url
                }
            }
        }
        
    }
}  
`

const fetchTrayViewQuery = (params) => {
    return client.request(trayViewQuery, params)
}

export { fetchTrayViewQuery }
