import React from "react"

import Layout from "../components/Layout"
import SEO from "../components/Global/SEO"
import SiteWrapper from "../components/Pages/SiteWrapper";
import SiteHeader from "../components/Pages/SiteHeader";
import Logo from "../components/Pages/Logo";
import Navigation from "../components/Pages/Navigation";
import ContentAreaWithSidebar from "../components/Pages/ContentAreaWithSidebar";
import TertiaryFooter from "../components/Pages/TertiaryFooter";
import Page from "../components/Pages/Page";
import styles from "../components/Pages/HeroContent.module.scss";

const NotFoundPage = () => (
  <Layout>
    <Page>
      <SEO/>
      <SiteWrapper>
        <SiteHeader>
          <Logo />
          <Navigation />
        </SiteHeader>
        <div className={styles.HeroContent}>
          <img src="/404.jpg" alt="Image courtesy Annie Spratt on Unsplash" />
          <div className={styles.textContent}>
            <h1>Content not found</h1>
            <p>The page you were looking for can't be found.</p>
          </div>
        </div>
        <ContentAreaWithSidebar content={`We couldn't find the content you were looking for. It's possible that you mis-typed an address, or followed a link from somewhere. You might want to <a href="/">go back to the homepage</a>`} quick_starts={[]} cta_blocks={[]} />
        <TertiaryFooter />
      </SiteWrapper>
    </Page>
  </Layout>
)

export default NotFoundPage
