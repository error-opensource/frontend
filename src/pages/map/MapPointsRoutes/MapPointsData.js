import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import PropTypes from "prop-types"
import { useMatch } from "@reach/router"
import { useStore } from "../../../models/RootStore"

export const MapPointsData = observer((props) => {
    const {searchResults, mapRefIsSet, mapViewport, mapPointsStack, mapPointsStack: { addPoints, setVisibleStackDepth, clearStackByKey }} = useStore()
    const currentRouteMatch = useMatch(props.path)
    const renderingTrailStopRoute = useMatch("/map/trails/:trailId/stops/:stopId")
    
    useEffect(() => {
        if( props.setVisibleStackDepth && props.location.pathname === props.uri ) {
            setVisibleStackDepth(props.visibleStackDepth)
        }
    })

    useEffect(() => {
        if( !props.queryObject ) return
        
        // if we're rendering a trail stop route, dont re-fetch the base layer pin data
        // todo: abstract this out into MapPointData props
        if( renderingTrailStopRoute && props.path === '*' ) {
            return
        }
        
        const pointDataPromise = props.queryObject.getPointData(props.path, props.queryParams, {stackPosition: props.stackPosition})

        if(props.queryObjectFulfilsPromise) {
            addPoints(props.path, pointDataPromise, {stackPosition: props.stackPosition, spriteSheet: props.spriteSheet, disableClustering: props.disableClustering, hideAtStackSize: props.hideAtStackSize, highlightAllPoints: props.highlightAllPoints})
        }else {
            pointDataPromise.then((response) => {
                if( !response ) return
                
                if( props.responseHandler ) {
                    const results = props.responseHandler(response)
                    addPoints(props.path, results, {stackPosition: props.stackPosition, spriteSheet: props.spriteSheet, disableClustering: props.disableClustering, hideAtStackSize: props.hideAtStackSize, highlightAllPoints: props.highlightAllPoints})
                }else {
                    addPoints(props.path, response.results, {stackPosition: props.stackPosition, spriteSheet: props.spriteSheet, disableClustering: props.disableClustering, hideAtStackSize: props.hideAtStackSize, highlightAllPoints: props.highlightAllPoints})
                }
            })
        }
    }, [searchResults.filter.query, props.queryObject, mapRefIsSet, mapViewport.longitude, mapViewport.latitude])
    
    useEffect(() => {
        return (() => {
            if( !currentRouteMatch ) {
                clearStackByKey(props.path, props.clearCustomSpriteTypesOnTeardown)
            }
        })
    }, [mapPointsStack])

    return <>{props.children}</>
})

MapPointsData.defaultProps = {
    queryObject: {getPointData: () => {}},
    queryObjectFulfilsPromise: false,
    queryParams: {},
    setVisibleStackDepth: true,
    visibleStackDepth: 1,
    disableClustering: false,
    spriteSheet: 'base',
    hideAtStackSize: null,
    highlightAllPoints: false,
    clearCustomSpriteTypesOnTeardown: false,
}

MapPointsData.propTypes = {
    setVisibleStackDepth: PropTypes.bool,
    visibleStackDepth: PropTypes.number,
    queryObject: PropTypes.any,
    queryObjectFulfilsPromise: PropTypes.bool,
    queryParams: PropTypes.object,
    spriteSheet: PropTypes.string,
    disableClustering: PropTypes.bool,
    hideAtStackSize: PropTypes.number,
    highlightAllPoints: PropTypes.bool,
    clearCustomSpriteTypesOnTeardown: PropTypes.bool
}

export default MapPointsData
