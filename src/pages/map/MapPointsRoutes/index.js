import React from "react"
import { Router } from "@reach/router"
import { observer } from "mobx-react-lite"

import { useStore } from "../../../models/RootStore"
import { getIconName } from "../../../components/Global/IconClusterLayer"

import MapPointsData from "./MapPointsData"

export const MapPointsRoutes = observer(() => {
    const { collection, record, trail, mapViewport, searchResults } = useStore()

    const nestedRecordsResponseCallback = (response) => {
        const points = response.results.filter((result) => result.record).map((result) => {
            let point = {}
            point.id = result.record.id
            point.name = result.record.name
            point.slug = result.record.slug
            point.type = 'record'
            point.point_json = { lonlat: result.record.location.coordinates }
            point.record_image = { image: result.record.record_image }
            point.ordinalPosition = result.ordinalPosition
            
            return point
        })

        return points
    }

    const trailsNestedRecordsResponseCallback = (response) => {
        return nestedRecordsResponseCallback(response).map((point) => ({...point, customSpriteType: 'trailpin'}))
    }

    const getTrailIconName = (point) => {
        const data = {
            geometry: {coordinates: point.coordinates},
            properties: {...point, type: point.customSpriteType || point.type }
        }
        
        const icon = getIconName( point.ordinalPosition, data, 10, 10, { alwaysNumbered: true } )
        
        return icon
    }

    return <>
        <Router basepath="/map" primary={false}>
            <MapPointsData path='*'
                           default
                           queryObject={mapViewport}
                           setVisibleStackDepth={false}
                           stackPosition={0}>
                <MapPointsData path="/records/:recordId"
                               queryObject={record}
                               queryObjectFulfilsPromise={true}
                               visibleStackDepth={2}
                               stackPosition={1}
                               highlightAllPoints={true}
                />

                <MapPointsData path="/collections/:collectionId"
                               queryObject={collection}
                               responseHandler={nestedRecordsResponseCallback}
                               visibleStackDepth={1}
                               stackPosition={1}>
                    <MapPointsData path="/records/:recordId"
                                   queryObject={record}
                                   queryObjectFulfilsPromise={true}
                                   visibleStackDepth={2}
                                   stackPosition={2}
                                   highlightAllPoints={true}
                    />
                </MapPointsData>
                
                <MapPointsData path="/search/results"
                               queryObject={searchResults}
                               visibleStackDepth={1}
                               stackPosition={1}
                />

                <MapPointsData
                    path="/trails"
                    queryObject={searchResults}
                    queryParams={{with_term_ids: null, offset: 0, query: null, type: ['trail']}}
                    visibleStackDepth={1}
                    stackPosition={1}
                    hideAtStackSize={3}>
                        <MapPointsData 
                            path="/:trailId" 
                            queryObject={trail} 
                            responseHandler={trailsNestedRecordsResponseCallback} 
                            visibleStackDepth={1} 
                            stackPosition={2}
                            disableClustering={true}
                            getIcon={getTrailIconName}
                            clearCustomSpriteTypesOnTeardown={true}
                        >
                                <MapPointsData 
                                    path="/stops/:stopId" 
                                    queryObject={trail ? trail.currentStop : false} 
                                    queryObjectFulfilsPromise={true} 
                                    visibleStackDepth={2} 
                                    stackPosition={3} 
                                    highlightAllPoints={true}
                                />
                        </MapPointsData>
                </MapPointsData>
            </MapPointsData>
        </Router>
    </>
})

export default MapPointsRoutes