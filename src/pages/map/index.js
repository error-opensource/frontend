import React, { useEffect } from "react"
import { Router, useLocation, globalHistory } from "@reach/router"

import Tray from "../../components/Tray/Tray"
import Map from "../../components/Global/Map"
import Layout from "../../components/Layout"

import IntroductionIndex from "../../components/Introduction/IntroductionIndex"

import BrowseIndex from "../../components/Browse/BrowseIndex"

import SearchForm, { SearchResultsTrayOverview } from "../../components/Search/SearchForm"
import SearchResults from "../../components/Search/SearchResults"

import OverlayIndex from "../../components/Overlays/OverlayIndex"
import OverlayView from "../../components/Overlays/OverlayView"

import HelpIndex from "../../components/Help/HelpIndex"

import RecordView, { RecordTrayOverview } from "../../components/Records/RecordView"

import CollectionContainer from "../../components/Collections/CollectionContainer"
import CollectionView, { CollectionTrayOverview } from "../../components/Collections/CollectionView"
import CollectionRecordView from "../../components/Collections/CollectionRecordView"

import TrailContainer, { TrailTrayOverview } from "../../components/Trails/TrailContainer"
import TrailView from "../../components/Trails/TrailView"
import TrailRecordView from "../../components/Trails/TrailRecordView"

import OverlayTools from "../../components/Overlays/OverlayTools"
import TrayOverview from "../../components/Tray/TrayOverview";
import MapPointsRoutes from "./MapPointsRoutes"

import { useStore } from "../../models/RootStore"

const App = () => {
    const { previousLocation, trackPreviousLocation, setPreviousLocation } = useStore()
    const location = useLocation()
    
    globalHistory.listen(({action}) => {
        if( action === 'PUSH' ) {
            setPreviousLocation(location.pathname)
        }
    })

    useEffect(() => {
        if (typeof (window) !== 'undefined') {
            function handleFirstTab(e) {
                if (e.keyCode === 9) {
                    document.body.classList.add('user-is-tabbing')
                    window.removeEventListener('keydown', handleFirstTab)
                }
            }

            return () => {
                window.removeEventListener('keydown', handleFirstTab)
            }
        }
    })

    useEffect(() => {
        const pathMatchParts = /map\/(?<resource>[^/]+)(\/(?<resourceId>[^/]+))?(\/(?<subResource>[^/]+))?/.exec(location.pathname)
        if( !pathMatchParts ) {
            return 
        }

        const {groups: {resource, subResource}} = pathMatchParts
        const key = [resource, subResource].filter((routePart) => routePart).join("/")
        
        trackPreviousLocation(key, previousLocation)
    })
    
    return <Layout>
        <MapPointsRoutes />
        
        <Tray>
            <Router basepath="/map">
                <IntroductionIndex path="/" default />

                <BrowseIndex path="/browse" title="Browse in this area"/>

                <SearchResults path="/search/results"/>
                <SearchForm path="/search" />

                {
                    process.env.GATSBY_OVERLAYS_ENABLED && 
                    <>
                        <OverlayIndex path="/overlays"/>
                        <OverlayView path="/overlays/:overlayId"/>
                    </>
                }

                <HelpIndex path="/help"/>

                <RecordView path="/records/:recordId"/>

                <CollectionContainer path="/collections/:collectionId">
                    {/* CollectionContainer triggers our collection lookup, CollectionView renders the collection components */}
                    <CollectionView path="/" />

                    <CollectionRecordView path="/records/:recordId" />
                </CollectionContainer>

                {
                    process.env.GATSBY_TRAILS_ENABLED && 
                    <>
                        <BrowseIndex path="/trails" contentTypes={['trail']} title={"Trails"}/>

                        <TrailContainer path="/trails/:trailId">
                            <TrailView path="/" />
                            
                            <TrailRecordView path="/stops/:stopNumber">

                            </TrailRecordView>
                        </TrailContainer>
                    </>
                }
            </Router>
        </Tray>

        <Router basepath="/map" primary={false}>
            <TrayOverview path="/records/:recordId">
                <RecordTrayOverview path="/"/>
            </TrayOverview>

            <TrayOverview path="/collections/:collectionId">
                <CollectionTrayOverview path="/"/>
                <RecordTrayOverview path="/records/:recordId" />
            </TrayOverview>

            <TrayOverview path="/search">
                <SearchResultsTrayOverview path="/"/>
            </TrayOverview>

            <TrayOverview path="/search">
                <SearchResultsTrayOverview path="/"/>
            </TrayOverview>

            <TrayOverview path="/trails">
                <TrailTrayOverview path="/" />
            </TrayOverview>

            <TrayOverview path="/" default/>
        </Router>

        <OverlayTools/>

        <Map/>
    </Layout>
}

export default App 