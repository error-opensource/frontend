import React from 'react'
import { Query } from 'react-apollo';
import queryString from 'query-string';

const withPreview = (args = { preview: false } ) => Component => {
    const preview = (props) => {
        const search = queryString.parse(props.location.search);
        
        const {
            id, revision_id, token
        } = search

        if( !token || !id || !revision_id ) {
            return <Component preview={false} {...props} />
        }

        return (
            <Query query={args.preview} variables={{id, revision_id, token}}>
                {({ data }) => {
                    return <Component preview={data} {...props} />
                }}
            </Query>
        )
    }

    return preview
}

export default withPreview
