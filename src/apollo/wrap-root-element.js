import React from 'react';
import { StoreProvider, rootStore } from "../models/RootStore"

export const wrapRootElement = ({ element }) => (
  <StoreProvider value={rootStore}>
    {element}
  </StoreProvider>
);
