import { useEffect } from 'react'
import { useStore } from "../models/RootStore"
import queryString from 'query-string'

export const useQueryStringParams = (location) => {
    const {searchResults} = useStore()
    const defaults = searchResults.filter.defaultSearchProperties

    const search = queryString.parse(location.search, {arrayFormat: "comma"});
    console.log({...defaults, ...search})
    
    // useEffect(() => {
    //     const {q: query="", taxonomy_term_ids=[]} = search
    
    //     const ids = [].concat(taxonomy_term_ids).map(i => parseInt(i))
    
    //     const hasResults = searchResults.count > 0 
    
    //     if (!hasResults && ( searchResults.filter.taxonomiesAreLoaded && ((query && query !== searchResults.filter.query) || (ids !== searchResults.filter.taxonomies.checkedIds))) )  {
    //         searchResults.filter.setSearchParameter('query', query)
    //         searchResults.filter.taxonomies.setCheckedIds(ids)

    //         searchResults.fetchResults();
    //     }
    // }, [location])

    return {...defaults, ...search}
}
