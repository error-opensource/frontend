import { types, getParentOfType } from "mobx-state-tree"
import TaxonomyModel from "./TaxonomyModel"

const TaxonomyTermModel = types.model({
    id: types.number,
    name: types.string,
    sub_terms: types.maybeNull( types.array( types.late( () => TaxonomyTermModel ), [] ) )
}).volatile(self => ({
    checked: false
})).views(self => ({
    get partiallyChecked() {
        const values = self.sub_terms.map((term) => term.isChecked)

        const uniqued = values.filter((el, idx, self) => self.indexOf(el) === idx)
        return uniqued.find((el) => el === true) && uniqued.length > 1
    },
    get allChecked() {
        const values = self.sub_terms.map((term) => term.isChecked).filter((el, idx, self) => self.indexOf(el) === idx)
        return (values.length === 1 && values[0] === true)
    },
    get isChecked() {
        return self.checked || (self.sub_terms && self.allChecked);
    },
    get toQuery() {
        const taxonomy = getParentOfType(self, TaxonomyModel)
        const query = {}
        let taxonomy_term_ids = []

        if( taxonomy.hasSubTerms ) {
            taxonomy_term_ids = self.sub_terms.filter((term) => term.isChecked).map((term) => term.id)
            
            if( taxonomy_term_ids.length === 0 ) {
                return null
            }
            
            if( taxonomy_term_ids.length > 0 ) {
                query.taxonomy_term_ids = self.sub_terms.filter((term) => term.isChecked).map((term) => term.id)
            }
        }
        
        query.taxonomy_term_ids = false 
        
        if( self.isChecked ) {
            taxonomy_term_ids.push(self.id)
        }
        query.taxonomy_term_ids = taxonomy_term_ids

        return query
    }
})).actions(self => ({
    setChecked(value) {
        if( value === self.isChecked ) return
        self.checked = value
    },
    toggle() {
        self.checked = !self.checked
    },
    toggleAllSubTerms() {
        const anyChecked = self.sub_terms.filter((term) => term.isChecked).length > 0;
        self.sub_terms.map((term) => term.setChecked(!anyChecked));
    }
}))

export default TaxonomyTermModel 
