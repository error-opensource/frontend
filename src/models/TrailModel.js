import { types, flow, getRoot, getParentOfType } from "mobx-state-tree"
import turf, { bbox } from "turf"
import geoViewport from "@mapbox/geo-viewport"
import RecordModel from "./RecordModel"
import ImageModel from "./ImageModel"
import { fetchTrailRouteStopsQuery } from "../queries/TrailQueries"
import { fetchTrailPointDataResultsQuery } from "../queries/TrailQueries"
import PageableQueryModel from "./PageableQueryModel"
import { TRAIL_STOPS_LIMIT } from "../queries/client"
import TaxonomyTermModel from "./TaxonomyTermModel"

const TrailStopsFilter = types
    .model('TrailStopsFilter', {
        slug: types.string,
        limit: types.number,
        offset: types.number,
    })
    .actions((self) => ({
        setSearchParameter(name, value) {
            self[name] = value
        },
    }))
    .views((self) => ({
        get nextOffset() {
            return self.limit + self.offset
        },
    }))

// TrailRecordModel is a composition of Record model and 
// its associated trail_record#position attribute
const TrailRecordModel = RecordModel.named('TrailRecordModel').props({
    id: types.number,
    identifier: types.identifier,
    ordinalPosition: types.maybeNull(types.integer)
}).actions(self => ({
    getPointData: (_, __ = {}) => {
        const point = {
            id: self.id, 
            name: self.name, 
            slug: self.slug, 
            type: 'record',
            point_json: {lonlat: self.location.coordinates.toJSON()}, 
            record_image: {},
            customSpriteType: 'trailpin',
            ordinalPosition: 1
        }

        return [point]
    },
}))
.preProcessSnapshot((snap) => {
    return {...snap.record, ordinalPosition: snap.ordinalPosition, identifier: `trail-stop-${snap.ordinalPosition}`}
})

// TrailStops - the array of associated records representing each stop on the trail
const TrailStops = PageableQueryModel.named('TrailStops').props({
    filter: types.maybeNull(TrailStopsFilter),
    results: types.array(TrailRecordModel, []),
})
.preProcessSnapshot((snap) => {
    return snap
})

// TrailPath - the route between stops on the trail - Integration specific stuff can go here
const TrailPath = types.model('TrailPath', {
    points: types.array(types.array(types.number, []), []),
    routesAreSet: types.boolean,
    transportProfile: types.maybeNull( types.enumeration('transportProfile', ['walking', 'driving', 'cycling']) )
}).volatile(self => ({
    routesJson: {}
}))
.actions((self) => ({
    afterCreate: () => {
        self.transportProfile = 'walking'
    },
    generateRoute: flow(function* generateRouteGeoJSON() {
        const lonlatPairs = self.points.map((point) => point.join(',')).join(';')
        const mapboxToken = process.env.GATSBY_MAPBOX_TOKEN
        const transportProfile = self.transportProfile || 'walking'
        
        const response = yield fetch(`https://api.mapbox.com/directions/v5/mapbox/${transportProfile}/${lonlatPairs}?annotations=duration&overview=simplified&geometries=geojson&access_token=${mapboxToken}`)
        const json = yield response.json()

        self.routesJson = json
        self.routesAreSet = true
    }),
    setPoints: (points) => {
        self.points = points
    },
    setTransportProfile: (value) => {
        self.transportProfile = value
        self.generateRoute()
    }
})).views((self) => ({
    get centroid() {
        const points = self.points.map((point) => turf.point( point.toJSON()) )
        const featureCollection = turf.featureCollection( points )
        
        const { geometry: { coordinates } } = turf.center( featureCollection )
        return coordinates
    },
    get distance() {
        const distances = self.routesJson.routes[0].legs.map((leg) => leg.distance)
        return distances.reduce((a, b) => a + b, 0)
    },
    get distanceInKM() {
        const km = Math.floor( self.distance / 1000 )
        return `${km}km`
    },
    get duration() {
        const durations = self.routesJson.routes[0].legs.map((leg) => leg.duration)
        const durationInSeconds = durations.reduce((a,b) => a + b, 0)

        const hours = Math.floor( durationInSeconds / 3600 )
        const minutes = Math.floor( (durationInSeconds % 3600) / 60 )
        const seconds = Math.floor( durationInSeconds % 3600 % 60 )

        return {hours, minutes, seconds}
    },
    get durationHMS() {
        const duration = self.duration
        const durationString = `${String(duration.hours).padStart(2, 0)}:${String(duration.minutes).padStart(2, 0)}:${String(duration.seconds).padStart(2, 0)}`
        
        return durationString
    }
}))

const TrailModel = types
    .model('TrailModel', {
        tenant_id: types.number,
        name: types.string,
        slug: types.string,
        sanitised_content: types.maybeNull(types.string, ""),
        excerpt: types.maybeNull(types.string, ""),
        image: types.maybeNull(ImageModel),
        terms: types.array(TaxonomyTermModel, []),
        distance: types.maybeNull(types.string),
        duration: types.maybeNull(types.string),
        currentStop: types.maybeNull(types.reference(TrailRecordModel, {
            get(identifier, parent) {
                return parent.stops.results.find((result) => result.identifier === identifier)
            },
            set(value) {
                return value.ordinalPosition
            }
        }))
    })
    .volatile((self) => ({
        stops: TrailStops.create({
            results: [],
            filter: { slug: self.slug, limit: TRAIL_STOPS_LIMIT, offset: 0 },
        }),
        path: TrailPath.create({
            points: [],
            routesAreSet: false, 
            routes: {}
        }),
    }))
    .actions((self) => ({
        afterCreate: () => {
            self.fetchStops()
        },
        fetchStops: flow(function* fetchStops() {
            self.stops.clear()
            yield self.stops.fetch(fetchTrailRouteStopsQuery, self.stops.filter.toJSON())
        }),
        fetchNextPage: flow(function* fetchNextPage() {
            yield self.records.fetchNextPage()
        }),
        getPointData: flow(function* getPointData(_, __, ___) {
            const response = yield fetchTrailPointDataResultsQuery({
                slug: self.slug,
            })  
            
            const points = response.results.filter((result) => result.record).map((result) => {
                let point = {}
                point.id = result.record.id
                point.name = result.record.name
                point.slug = result.record.slug
                point.type = 'record'
                point.point_json = { lonlat: result.record.location.coordinates }
                point.record_image = { image: result.record.record_image }
                point.ordinalPosition = result.ordinalPosition
                
                return point
            })

            self.results = points
            
            self.path.setPoints(self.stopPoints)
            yield self.getRoute()

            return response
        }),
        getRoute: flow(function* getRoute() {
            self.path.generateRoute()
        }),
        navigateTo: (stop) => {
            const root = getRoot( self )
            
            const pitch = 0
            const zoom = Math.max(19, root.mapViewport.zoom)
            root.mapViewport.setCenterPitchBearingAndZoom(stop.location, self.nextStop ? self.nextStop.location : false, pitch, zoom)
        },
        setCurrentStop: (stopId) => {
            self.currentStop = `trail-stop-${stopId}`
            self.navigateTo(self.currentStop)
        },
        goToPreviousStop: () => {
            const currentIndex = self.stops.results.indexOf(self.currentStop)
            let ordinalPosition = self.stops.results[currentIndex-1].ordinalPosition

            if( !ordinalPosition ) {
                ordinalPosition = self.stops.results[0].ordinalPosition
            }

            if( ordinalPosition ) {
                self.setCurrentStop(ordinalPosition)
            }
        },
        goToFirstStop: () => {
            self.setCurrentStop(self.stops.results[0].ordinalPosition)
        },
        goToNextStop: () => {
            const currentIndex = self.stops.results.indexOf(self.currentStop)
            let ordinalPosition = self.stops.results[currentIndex+1].ordinalPosition

            if( !ordinalPosition ) {
                ordinalPosition = self.stops.results[0].ordinalPosition
            }

            self.setCurrentStop(ordinalPosition)
        }
    }))
    .views((self) => ({
        get stopPoints() {
            return self.results.map((stop) => stop.point_json.lonlat)
        },
        get hasPoints() {
            return self.path.points.length > 0 
        },
        get boundingViewport() {
            const points = self.path.points.map((point) => point.toJSON())
            
            const root = getRoot( self )
            const line = turf.lineString(points)
            const box  = turf.bbox(line)

            const viewport = geoViewport.viewport(box, root.mapViewport.mapDimensions )

            const viewportObject = {
                coordinates: viewport.center,
                zoom: viewport.zoom-1.25
            }

            return viewportObject
        },
        get hasNextStop() {
            return self.stops.results.indexOf(self.currentStop) < self.stops.results.length-1
        },
        get hasPreviousStop() {
            return self.stops.results.indexOf(self.currentStop) > 0
        },
        get nextStop() {
            return self.stops.results[self.stops.results.indexOf(self.currentStop)+1]
        },
        get previousStop() {
            return self.stops.results[self.stops.results.indexOf(self.currentStop)-1]
        }
    }))

export default TrailModel
