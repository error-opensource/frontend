import { types, flow } from "mobx-state-tree"

import QuickStartModel from "./QuickStartModel"
import PageableQueryModel from "./PageableQueryModel"
import { fetchQuickStartsQuery } from "../queries/QuickStartQueries"

export const QuickStartsFilter = types.model({
    limit: types.number
}).actions(self => ({
    setSearchParameter(name, value) {
        self[name] = value
    }
})).views(self => ({
    get nextOffset() {
        return self.limit + self.offset
    }
}))

// build a PageableQueryModel for QuickStart results...
const QuickStartsCollection = PageableQueryModel.named("QuickStartsCollection").props({
    filter: QuickStartsFilter,
    results: types.array(QuickStartModel, [])
}).actions(self => ( {
    fetchQuickStarts: flow(function* fetchQuickStarts() {
        self.loading.loading()

        try {
            yield self.fetch(fetchQuickStartsQuery, {limit: 100} )
            self.loading.done()
        }catch( error ) {
            console.log(error)
        }
    })
})).preProcessSnapshot((snapshot) => {
    let snap = snapshot 

    return snapshot
})

export default QuickStartsCollection