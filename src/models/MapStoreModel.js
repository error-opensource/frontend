import { types, applySnapshot, getRoot, getParent, flow } from "mobx-state-tree"
import { debounce, defer } from "lodash"
import turf from "turf"
import { fetchPointDataResultsQuery } from "../queries/PointDataQueries"
import { fetchSearchCardItemPointsQueryWithBounds } from "../queries/SearchQueries"
import { contentTypeValues } from "./ContentTypes"

const MapStoreModel = types.model({
    width:     '100%',
    height:    '100%',
    zoom:      12.25, 
    maxZoom:   20,
    longitude: 0,
    latitude:  0,
    transitionDuration: 350,
    bearing: 0,
    pitch: 0
}).volatile(self => ({
    mapRef: {}
}))
.actions(self => ({
    getPointData: flow(function* getPointData(_, queryParms, options = {}) {
        const root = getRoot(self)
        const searchFilter = root.searchResults.filter
        
        if( !root.mapRefIsSet ) return

        const { type } = {type: contentTypeValues, ...queryParms}

        // let params = {type: type, centroid: self.centroid, distance: self.boundingBoxDistance}
        // const response = yield fetchPointDataResultsQuery( { ...params, orderby: "distance" } )
        
        // use search filter to fetch points
        let { limit, ...params } = searchFilter.toQuery
        console.warn('MapStoreModel searching with: ', {...params, ...queryParms, centroid: self.centroid, distance: self.boundingBoxDistance})
        const response = yield fetchSearchCardItemPointsQueryWithBounds( {...params, ...queryParms, centroid: self.centroid, distance: self.boundingBoxDistance} )

        return response
    }),
    setMapRef: (ref) => {
        self.mapRef = ref
        const root = getRoot(self);
        root.setMapRef( true )
    },
    setZoom: (level) => {
        self.zoom = level
    },
    setPitchAndBearing: (angle, degrees) => {
        self.pitch = angle
        self.bearing = degrees
    },
    setCenter: (location) => {
        self.longitude = location.coordinates[0]
        self.latitude  = location.coordinates[1]
        
        if( location.zoom ) {
            self.zoom = location.zoom
        }
    },
    setCenterPitchBearingAndZoom: (location, nextLocation, pitch, zoom) => {
        self.pitch = pitch
        self.zoom = zoom
        self.longitude = location.coordinates[0]
        self.latitude  = location.coordinates[1]

        // if( nextLocation ) {
        //     self.bearing = turf.bearing(location.coordinates.toJSON(), nextLocation.coordinates.toJSON())
        // }
    },
    fitBounds: ( bounds, zoom ) => {
        const viewport  = { ...self, zoom }
        
        // set viewport to center of bounding box
        viewport.latitude  = ( bounds[0][1] + bounds[1][1] ) / 2
        viewport.longitude = ( bounds[0][0] + bounds[1][0] ) / 2
        viewport.transitionDuration = 500

        applySnapshot( self, viewport )
    },
    setMapViewport: debounce ( ({ viewState }) => {
        applySnapshot( self, viewState )

        defer(() => {
            getRoot( self ).reloadCardItems()
        })
    }, 250, {leading: false, trailing: true})
})).views(self => ({
    get centroid() {
        let center = self.mapRef._map.getCenter()
        return `${center.lng} ${center.lat}`
    }, 
    get boundingBoxDistance() {
        let center = self.mapRef._map.getCenter()
        let boundingBox = self.mapRef._map.getBounds()
        return turf.distance([center.lng, center.lat], [boundingBox._ne.lng, boundingBox._ne.lat], 'metres')
    },
    get mapDimensions() {
        return [self.mapRef._map.getContainer().offsetWidth, self.mapRef._map.getContainer().offsetHeight]
    }
}))
.preProcessSnapshot((snapshot) => {
    const newSnapshot = {...snapshot}
    newSnapshot.height = '100%'
    newSnapshot.width = '100%'
    delete newSnapshot.points

    return newSnapshot
}).postProcessSnapshot((snapshot) => {
    const newSnapshot = {...snapshot}
    newSnapshot.width = '100%'
    newSnapshot.height = '100%'

    return newSnapshot
})

export default MapStoreModel
