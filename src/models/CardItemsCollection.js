import PageableQueryModel from "./PageableQueryModel"
import CardItemsFilter from "./CardItemsFilter"
import CardItemResult from "./CardItemResult"
import { types } from "mobx-state-tree"

// build a PageableQueryModel for card item results...
const CardItemsCollection = PageableQueryModel.named("CardItemResultsModel").props({
    filter: CardItemsFilter,
    results: types.array(CardItemResult, [])
})

export default CardItemsCollection