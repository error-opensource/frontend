import { types } from "mobx-state-tree"

const ImageModel = types.model("ImageModel",{
    id: types.maybeNull(types.integer),
    name: types.string,
    description: types.maybeNull(types.string),
    credit: types.maybeNull(types.string),
    url: types.maybeNull(types.string)
})
.preProcessSnapshot((snapshot) => {
    if( !snapshot ) return

    if( snapshot.image ) {
        return snapshot.image
    }else {
        return {...snapshot}
    }
})

export default ImageModel 
