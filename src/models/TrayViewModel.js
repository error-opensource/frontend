import { types } from "mobx-state-tree"

import RecordModel from "./RecordModel"
import TaxonomyTermModel from "./TaxonomyTermModel"
import QuickStartModel from "./QuickStartModel"

const TrayViewModel = types.model({
    title: types.string,
    sanitised_content: types.maybeNull(types.string),
    records_title: types.maybeNull(types.string),
    records_description: types.maybeNull(types.string),
    records: types.array(RecordModel, []),

    taxonomies_description: types.maybeNull(types.string),
    taxonomies_title: types.maybeNull(types.string),
    taxonomies: types.array(TaxonomyTermModel, []), 
    
    quick_starts_title: types.maybeNull(types.string),
    quick_starts_description: types.maybeNull(types.string),
    quick_starts: types.array(QuickStartModel, [])
}).preProcessSnapshot((snapshot) => {
    if( !snapshot) return null 

    // fixup the snapshot json so that the sub-fields are 
    // properly flattened out to match the shape of the tree

    let snap = {...snapshot}
    
    snap.records = snapshot.records.map((record) => {
        record.record.terms = []
        return record.record
    })
    
    snap.taxonomies   = snapshot.taxonomies.map((taxonomy) => taxonomy.taxonomy)
    snap.quick_starts = snapshot.quick_starts.map((quick_start) => quick_start.quick_start)
    
    return snap
}).views(self => ({
    get hasTaxonomies() {
        return self.taxonomies.length > 0
    },
    get hasRecords() {
        return self.records.length > 0
    },
    get hasQuickStarts() {
        return self.quick_starts.length > 0
    }
}))

export default TrayViewModel
