import { types } from "mobx-state-tree"

const LocationModel = types.model('LocationModel', {
    type: types.string,
    coordinates: types.array(types.number, [])
}).views(self => ({
    get longitude() {
        return self.coordinates[0]
    },
    get latitude() {
        return self.coordinates[1]
    },
    get centroid() {
        return `${self.coordinates[0]} ${self.coordinates[1]}`
    }
}))

export default LocationModel