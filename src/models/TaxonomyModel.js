import { types } from "mobx-state-tree"
import TaxonomyTermModel from "./TaxonomyTermModel"

const TaxonomyModel = types.model({
    id: types.number,
    name: types.string,
    slug: types.string,
    type: types.enumeration( "taxonomy_type", ["dropdown", "nested", "radio"] ),
    terms: types.array( types.late( () => TaxonomyTermModel ), [])
})
.volatile(self => ({
    checked: false
}))
.actions(self => ({
    toggleAll() {
        const anyChecked = self.terms.filter((term) => term.isChecked).length > 0 
        
        self.terms.map((term) => term.setChecked(!anyChecked))
    },
    setCheckedIds(ids) {
    //    receive an array of IDs to check on this taxonomy
        self.terms.forEach((t) => {
            t.setChecked(ids.indexOf(t.id) >= 0)
            if (t.sub_terms && t.sub_terms.length > 0) {
                t.sub_terms.forEach((st) => {
                    st.setChecked(ids.indexOf(st.id) >= 0)
                })
            }
        })
    }
}))
.views(self => ({
    get partiallyChecked() {
        const values = self.terms.map((term) => term.isChecked)

        const uniqued = values.filter((el, idx, self) => self.indexOf(el) === idx)
        return uniqued.find((el) => el === true) && uniqued.length > 1
    },
    get allChecked() {
        const values = self.terms.map((term) => term.isChecked).filter((el, idx, self) => self.indexOf(el) === idx)
        
        return (values.length === 1 && values[0] === true)
    },
    get hasSubTerms() {
        return self.type === "nested"
    },
    get toQuery() {
        const queries = self.terms.map((term) => term.toQuery).filter((query) => query)
        const flattendQueries = [].concat.apply([], queries)

        
        return flattendQueries
    },
    get checkedIds() {
        // Return an array of IDs which are checked for this taxonomy
        return [].concat(...self.terms.map((t) => (t.toQuery === null) ? [] : t.toQuery.taxonomy_term_ids).filter(t => t.length > 0))
    }
})).preProcessSnapshot((snap) => {
    let snapshot = {...snap}

    const displayAs = snapshot.display_as 
    delete snapshot.display_as 

    let type = ""
    switch(displayAs) {
        case 1:
            type = "radio"
            break
        case 2:
            type = "nested"
            break
        default:
            type = "dropdown"
            break
    }

    snapshot.type = type
    return snapshot
})

export default TaxonomyModel