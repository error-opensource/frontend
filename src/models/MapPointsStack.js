import { types, getParentOfType, getType, getParent, isArrayType } from "mobx-state-tree"

import { MapPointModel } from "./MapPointModel"
import { uniq, intersection, includes } from "lodash"

const MapPointsStack = types.model('MapPointsStack', {
    points: types.map(types.late(() => MapPointModel)),
    stack: types.array(types.late(() => MapPointCollectionModel)),
    depth: types.number,
}).actions(self => ({
    setVisibleStackDepth(value) {
        self.depth = value
    },
    clearStackExceptKeys(except = []) {
        if (except && Array.isArray(except)) {
            const filtered = self.stack.filter((element) => except.indexOf(element.path) > -1)
            self.stack = filtered
        } else {
            self.stack = []
        }
    },
    clearStackByKey(key, clearCustomSpriteTypesOnTeardown) {
        const stack = self.stack.slice()
        const stackMember = stack.find((mps) => mps.path === key)
        if( !stackMember ) return

        if (clearCustomSpriteTypesOnTeardown) {
            stackMember.points.map((p) => p.customSpriteType = null)
        }

        const index = stack.indexOf(stackMember)
        stack.splice(index, 1)
        self.stack = stack
    },
    addPoints(key, points, options = {}) {
        let currentStack = self.stack.find((mps) => mps.path === key)

        points.map((point) => {
            if( self.points.has(`${point.type}-point-${point.id}`) ) {
                const currentPoint = self.points.get(`${point.type}-point-${point.id}`)
                // todo: abstract this into a method for persisting certain attributes through the stack when updating a point:
                // 
                // some attributes need to persist down through the stack, even if we're replacing the point
                // ie user goes to / to /trails/trail-1234, the getPointData call for `trails-1234` happens first and we set
                // our point in the store with a trailpin sprite type, but then the getPointData for the base layer 
                // overwrites it and we lose our custom sprite
                point.customSpriteType = currentPoint.customSpriteType ? currentPoint.customSpriteType : point.customSpriteType
                point.ordinalPosition  = currentPoint.ordinalPosition  ? currentPoint.ordinalPosition  : point.ordinalPosition
            }
            
            // point.isHighlighted = options.highlightAllPoints ? true : false
            self.points.set(`${point.type}-point-${point.id}`, point)
        })

        const newPointIdentifiers = points.map((point) => `${point.type}-point-${point.id}`)
        
        if( currentStack ) {
            currentStack.setPoints(newPointIdentifiers)
        }else {
            const newMapPointsCollection = {
                path: key, 
                points: newPointIdentifiers, 
                spriteSheet: options.spriteSheet,
                disableClustering: options.disableClustering,
                hideAtStackSize: options.hideAtStackSize,
                highlightAllPoints: options.highlightAllPoints,
            }

            self.stack.splice(options.stackPosition, 0, newMapPointsCollection)
        }
    },
})).views(self => ({
    get visibleStackMembers() {
        return self.stack.slice(-self.depth)
    }, 
    get visibleStackMembersExceptTop() {
        return self.stack.slice(-1)
    },
    get topOfStack() {
        const [topOfStack] = self.stack.slice(-1)
        return topOfStack
    },
    get allVisibleLayerPoints() {
        const stack = self.visibleStackMembers

        return stack.map((mapPointsStackMember, idx) => {
            const next = stack[idx+1]

            if( next ) {
                return mapPointsStackMember.points.filter((point) => !includes( next.points.toJSON(), point.identifier ) )
            }else {
                return mapPointsStackMember.points
            }
        })
    }
}))

export const MapPointCollectionModel = types.model('MapPointCollectionModel', {
    path: types.string,
    points: types.array(
        types.reference(types.late(() => MapPointModel), {
            get(identifier, parent) {
                return getParentOfType( parent, MapPointsStack ).points.get(identifier)
            }, set(value) {
                return value
            }
        })
    ),
    highlightedPointIdentifier: types.maybeNull(types.string),
    spriteSheet: types.maybeNull(types.string),
    disableClustering: types.maybeNull(types.boolean),
    hideAtStackSize: types.maybeNull(types.number),
    highlightAllPoints: types.boolean
})
.actions(self => ({
    clearPoints() {
        self.points = []
    }, 
    setPoints(points) {
        self.points = points
    },
    setDisableClustering(value) {
        self.disableClustering = value
    },
    setHighlightedPoint(identifier) {
        self.highlightedPointIdentifier = identifier
    },
    removeHighlightedPoint() {
        self.highlightedPointIdentifier = null
    },
})).views(self => ({
    get visiblePoints() {
        if( self.hideAtStackSize && getParentOfType( self, MapPointsStack ).stack.length >= self.hideAtStackSize ) {
            return []
        }

        // return self.points with self.hiddenPointIdentifiers excluded
        return self.points.filter((point) => self.hiddenPointIdentifiers.indexOf(point.identifier) < 0)
    }, 
    get clusterable() {
        return !self.disableClustering
    }
}))
.preProcessSnapshot((snap) => {
    return {...snap}
})

export default MapPointsStack
