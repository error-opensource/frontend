import { types } from "mobx-state-tree"
import PageableQueryModel from "./PageableQueryModel"
import TaxonomyModel from "./TaxonomyModel"

const TaxonomiesCollection = PageableQueryModel.named("TaxonomiesCollection").props({
    results: types.array( TaxonomyModel, [] )
}).actions(self => ({
    setCheckedIds(ids) {
        // receive an array of IDs to set true (i.e. check) across all taxonomies.
        self.results.forEach((taxonomy) => taxonomy.setCheckedIds(ids))
    }
})).views(self => ({
    get toQuery() {
        const queries = self.results.map((taxonomy) => taxonomy.toQuery).filter((query) => query)
        const flattendQueries = [].concat.apply([], queries)

        const reducer = (accumulator, query) => {
            accumulator.taxonomy_term_ids = [].concat(...(accumulator.taxonomy_term_ids || []), query.taxonomy_term_ids)
            return accumulator
        }
        
        const query = flattendQueries.reduce(reducer, {})
        return query
    },
    get checkedIds() {
        // return an array of all checked term IDs across all taxonomies.
        return    [].concat.apply(self.results.map((tax) => tax.checkedIds))
    }
}))

export default TaxonomiesCollection 
