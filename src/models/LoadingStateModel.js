import { types } from "mobx-state-tree"

export const LoadingStatesEnum = {
    PENDING: 1, 
    LOADING: 2, 
    ERROR: 500, 
    NOT_FOUND: 404
}

export const LoadingState = types.model({
    code: 1, 
    message: types.maybeNull(types.string, "")
}).actions(self => ({
    setMessage: (value) => {
        self.message = value
    },
    setCode: (value) => {
        self.code = value
    },
    setError: (error, code) => {
        self.message = error.toString()
        self.code = code
    }, 

    start: () => {
        self.setCode( LoadingStatesEnum.LOADING )
    },
    loading: () => {
        self.setCode( LoadingStatesEnum.LOADING )
    },
    done: () => {
        self.setCode( LoadingStatesEnum.PENDING )
    }
})).views(self => ({
    get isLoading() {
        return self.code === LoadingStatesEnum.LOADING 
    },
    get didFail() {
        return self.code === LoadingStatesEnum.ERROR ||self.code === LoadingStatesEnum.NOT_FOUND
    }
}))
export default LoadingState 
