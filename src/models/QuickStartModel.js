import { types } from "mobx-state-tree"
import ImageModel from "./ImageModel"

const QuickStartModel = types.model({
    id: types.number,
    title: types.string,
    content: types.maybeNull(types.string),
    url: types.maybeNull(types.string),
    image: types.maybeNull(ImageModel)
})

export default QuickStartModel 
