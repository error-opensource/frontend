import React from "react";
import { types, flow, applySnapshot, onPatch, getSnapshot, getType, destroy, getRoot, getParent, getParentOfType, getPath, getPathParts } from "mobx-state-tree"
import * as mst from "mobx-state-tree"
import MapStoreModel from "./MapStoreModel"
import RecordModel from "./RecordModel"
import CollectionModel from "./CollectionModel"
import TrailModel from "./TrailModel"
import UISettingsModel from "./UISettingsModel"
import queryString from "query-string"
import { rehydrateStoreFromURL, persistStoreToURL, readSnapshotFromURL, writeSnapshotToURL } from "./hydration/url"
import LoadingStateModel, { LoadingStatesEnum } from "./LoadingStateModel"

import { PER_PAGE } from "../queries/client"
import { fetchRecordQuery } from "../queries/RecordQueries"
import { fetchCollectionQuery } from "../queries/CollectionQueries"
import { fetchTrailQuery } from "../queries/TrailQueries"
import { fetchCardItemResultsQueryWithBounds } from "../queries/CardItemQueries"
import { fetchTrayViewQuery } from "../queries/TrayViewQueries"
import { fetchSiteMetaQuery } from "../queries/SiteMetaQueries"

import SearchResultsCollection from "./SearchResultsCollection"
import TrayViewModel from "./TrayViewModel"
import SiteMetaModel from "./SiteMetaModel"
import CardItemsCollection from "./CardItemsCollection"
import OverlayGroupStore from "./OverlayGroupStore"
import { MapPointModel } from "./MapPointModel"
import { contentTypeValues } from "./ContentTypes"
import MapPointsStack from "./MapPointsStack"

const RootModel = types.model({
    mapViewport: types.late(() => MapStoreModel),
    uiSettings: types.late(() => UISettingsModel),

    record: types.maybeNull(RecordModel),
    collection: types.maybeNull(CollectionModel),
    trail: types.maybeNull(TrailModel),

    recordSlug: types.maybeNull(types.string),
    collectionSlug: types.maybeNull(types.string),
    trailSlug: types.maybeNull(types.string),

    cardItems: CardItemsCollection,
    searchResults: SearchResultsCollection,
    overlayGroups: OverlayGroupStore,

    introductionTrayView: types.maybeNull(TrayViewModel),
    helpTrayView: types.maybeNull(TrayViewModel),

    siteMeta: types.maybeNull(SiteMetaModel),

    mapRefSet: types.boolean,
    mapPointsStack: MapPointsStack
}).volatile(self => ({
    activePinObject: null,
    isHovering: false,
    loading: LoadingStateModel.create({code: 1}),
    layoutRef: null,
    previousLocation: null,
    previousLocations: {}
}))
.actions(self => ({
    afterCreate() {
        self.fetchSiteMeta()
        
        if( !self.introductionTrayView) {
            self.fetchIntroductionTrayView()
        }

        if( !self.helpTrayView ) {
            self.fetchHelpTrayView()
        }

        if( self.recordSlug ) {
            self.fetchRecord( self.recordSlug )
        }

        if( self.collectionSlug ) {
            self.fetchCollection( self.collectionSlug )
        }

        if( self.trailSlug ) {
            self.fetchTrail( self.trailSlug )
        }
    },

    setRecordSlug( value ) {
        if( self.record && value !== self.recordSlug ) {
            destroy( self.record )
        }
        
        self.recordSlug = value
    },

    setCollectionSlug( value ) {
        if( self.collection && value !== self.collectionSlug ) {
            destroy( self.collection )
        }

        self.collectionSlug = value
    },

    setTrailSlug( value ) {
        if( self.trail && value !== self.trailSlug ) {
            destroy( self.trail )
        }

        self.trailSlug = value
    },

    unlockPoints() {
        self.uiSettings.setTrayLock( false )
    },

    fetchRecord: flow(function* fetchRecord(id) {
        self.loading.loading()

        try {
            const result = yield fetchRecordQuery(id)
            if( result.record && result.record.length === 1 ) {
                self.loading.done()

                if( self.record ) {
                    // make sure we're re-creating a record instance by destroying this one
                    destroy( self.record )
                }
                self.adjustLayoutScroll('top')

                self.record = result.record[0]
                self.record.fetchAttachments()
            }else {
                //fixme: raise an exception
                self.loading.setError("Record not found", LoadingStatesEnum.NOT_FOUND)
            }
        }catch( error ) {
            self.loading.setError(error.toString(), LoadingStatesEnum.ERROR)
        }
    }),

    fetchCollection: flow(function* fetchCollection(id) {
        self.loading.loading()

        try {
            const result = yield fetchCollectionQuery(id)
            if( result.collection && result.collection.length === 1 ) {
                self.loading.done()

                const collection = result.collection[0]

                if( self.collection ) {
                    // make sure we're re-creating a collection instance by destroying this one
                    destroy(self.collection)
                }
                self.adjustLayoutScroll('top')

                self.collection = collection
                self.collection.fetchRecords()
            }else {
                //fixme: raise an exception
                self.loading.setError("Collection not found", LoadingStatesEnum.NOT_FOUND)
            }
        }catch( error ) {
            self.loading.setError(error.toString(), LoadingStatesEnum.ERROR)
        }
    }),

    fetchTrail: flow(function* fetchTrail(id) {
        self.loading.start()

        try {
            const result = yield fetchTrailQuery(id)
            if( result.trail && result.trail.length === 1 ) {
                self.loading.done()

                const trail = result.trail[0]

                if( self.trail ) {
                    // make sure we're re-creating a collection instance by destroying this one
                    destroy(self.trail)
                }
                self.adjustLayoutScroll('top')

                self.trail = trail
            }else {
                //fixme: raise an exception
                self.loading.setError("Trail not found", LoadingStatesEnum.NOT_FOUND)
            }
        }catch( error ) {
            self.loading.setError(error.toString(), LoadingStatesEnum.ERROR)
        }
    }),

    fetchIntroductionTrayView: flow(function* fetchIntroductionTrayView() {
        const type = process.env.GATSBY_INTRODUCTION_PAGE_TYPE

        let {results} = yield fetchTrayViewQuery({type})
        self.introductionTrayView = results[0]
    }),

    fetchHelpTrayView: flow(function* fetchHelpTrayView() {
        const type = process.env.GATSBY_HELP_PAGE_TYPE

        let {results} = yield fetchTrayViewQuery({type})
        self.helpTrayView = results[0]
    }),

    fetchSiteMeta: flow(function* fetchSiteMeta() {
        const {results} = yield fetchSiteMetaQuery()
        self.siteMeta = results[0]
    }),

    fetchCardItemResults: flow(function* fetchItems() {
        if( self.loading.isLoading ) {
            return 
        }

        self.loading.loading()

        try {
            let params = {...self.cardItems.filter.toJSON(), centroid: self.mapViewport.centroid, distance: self.mapViewport.boundingBoxDistance}
            yield self.cardItems.fetch(fetchCardItemResultsQueryWithBounds, params)
            
            self.loading.done()
        }catch( error ) {
            self.loading.setError(error.toString(), LoadingStatesEnum.ERROR)
        }
    }),

    fetchNextPage: flow(function* fetchNextPage() {
        yield self.cardItems.fetchNextPage()
    }),

    reloadCardItems() {
        let root = getRoot( self )
        let locked = root.uiSettings.trayLocked

        if( locked ) return

        // reset the cardItems filter params and perform a new search
        self.cardItems.filter.setSearchParameter( 'offset', 0 )
        self.fetchCardItemResults()
    },
    setLayoutRef: ( value ) => {
        self.layoutRef = value
    },
    adjustLayoutScroll: (trayPosition = 'bottom') => {
        //console.log('adjustLayoutScroll: ' + trayPosition)
        // Possible values: bottom, top (Could add resetOnly?)

        // resetOnly should move back to the top IF the user has scrolled down so the title is no longer visible.

        //console.log('adjustLayoutScroll: scrollTop ' + self.layoutRef.scrollTop)
        //self.layoutRef.scrollTo({top: 0, behavior: 'smooth'})
        //return

        if( self.layoutRef ) {
            const inMobileLayout = /mobile|smallMobile|tablet/.test(self.layoutRef.dataset.responsivecontext)
            
            setTimeout(() => {
                if( inMobileLayout ) {
                    // Original (bottom) position
                    const originalPosition = 0

                    // Absolute top scroll position (minus the toolbar (64))
                    // Calculation is based on css: margin-top: calc(75vh)
                    const absoluteTopPosition = (self.layoutRef.offsetHeight * 0.75) - 64

                    // Comfortable top position
                    const comfortableTopPosition = absoluteTopPosition - 20

                    //const adjustBy = Math.max( self.layoutRef.scrollTopMax * multiplier, self.layoutRef.scrollTop )

                    let scrollTo = ''

                    if (trayPosition === 'bottom') {
                        scrollTo = originalPosition
                    } else if (trayPosition === 'top') {
                        scrollTo = comfortableTopPosition
                    }
                    
                    self.layoutRef.scrollTo({top: scrollTo, behavior: 'smooth'})
                }
            }, 100)
        }
    },
    setMapRef: ( value ) => {
        if( !self.mapRefSet && value ) {
            self.fetchCardItemResults()
        }

        self.mapRefSet = value
    },
    setActivePin: ( value ) => {
        self.activePinObject = value
    },
    setIsHovering: ( value ) => {
        self.isHovering = value
    },
    setPreviousLocation( value ) {
        self.previousLocation = value
    },
    trackPreviousLocation( key, value ) {
        self.previousLocations[key] = value
    }
})).views(self => ({
    get mapRefIsSet() {
        return self.mapRefSet
    },
    get activePin() {
        return self.activePinObject === null ? false : self.activePinObject
    },
    get getCursor() {
        return self.isHovering === false ? 'default' : 'pointer';
    },
    get navigatedFromSearch() {
        return /\/map\/search/.test(self.previousLocation)
    }
}))

export const rootStore = RootModel.create({
    cardItems: {results: [], filter: {query: null, type: contentTypeValues, limit: PER_PAGE, offset: 0}},
    searchResults: {results: [], filter: {query: "", type: contentTypeValues, whole_map: true, limit: PER_PAGE, offset: 0, taxonomies: {results: []}}},
    overlayGroups: {results: [], filter: {limit: PER_PAGE, offset: 0}, sort: [], enabled: []},
    introductionTrayView: null,
    mapRefSet: false,
    mapViewport: {points: { }},
    uiSettings: {trayVisible: true, trayLocked: false, overlayToolsVisible: false, mediaGalleryVisible: false, mobileMenuVisible: false, markersVisible: true},
    mapPointsStack: {points: {}, stack: [], depth: 1}
})

// observe changes on the store to trigger actions
onPatch(rootStore, (patch) => {
    if( patch.path === "/recordSlug" && patch.value ) {
        rootStore.fetchRecord( patch.value )
    }

    if( patch.path === "/collectionSlug" && patch.value ) {
        rootStore.uiSettings.setTrayLock( true )
        rootStore.fetchCollection( patch.value )
    }
    
    if( patch.path === "/trailSlug" && patch.value ) {
        rootStore.uiSettings.setTrayLock( true )
        rootStore.fetchTrail( patch.value )
    }
})

if( typeof window === "object" ) {
    // if( process.env.NODE_ENV === "development" ) {
        window.store = rootStore
        window.getSnapshot = getSnapshot
        window.applySnapshot = applySnapshot
        window.onPatch = onPatch
        window.destroy = destroy
        window.queryString = queryString
        window.writeSnapshotToURL = writeSnapshotToURL
        window.readSnapshotFromURL = readSnapshotFromURL
        window.getType = getType
        window.getRoot = getRoot
        window.getParent = getParent
        window.getParentOfType = getParentOfType
        window.getPath = getPath
        window.getPathParts = getPathParts
        window.mst = mst

        window.snapshotting = {rehydrateStoreFromURL, persistStoreToURL, readSnapshotFromURL, writeSnapshotToURL}
        window.MapPointModel = MapPointModel
    // }

    // define some stores that we snapshot and push to the url
    const snapshottableStores = {
        'uiSettings': rootStore.uiSettings,
        'mapViewport': rootStore.mapViewport
    }

    for( let [storeName, store] of Object.entries( snapshottableStores ) ) {
        rehydrateStoreFromURL( storeName, store )
        // persistStoreToURL( storeName, store )
    }

    // store specific rehydration callbacks
    if( rootStore.overlayGroups.enabled.length ) {
        rootStore.overlayGroups.prefetchOverlayGroups( rootStore.overlayGroups.enabled.toJS() )
    }

    // rehydrate/persist nested stores
    // rehydrateStoreFromURL('searchResults.filter', rootStore.searchResults.filter)
    // persistStoreToURL('searchResults.filter', rootStore.searchResults.filter)

    // when the overlayGroups store is updated, persist the new snapshot
    onPatch( rootStore.overlayGroups, ( patch ) => {
        let {enabled} = getSnapshot( rootStore.overlayGroups )

        //const updatedSort = patch.op === "add" && /\/sortOrder\//.test(patch.path)
        const updatedEnabled = /\/enabled\//.test(patch.path)

        if( updatedEnabled ) {
            writeSnapshotToURL( 'overlayGroups', {enabled})
        }
    })
}

const RootStoreContext = React.createContext()
export const StoreProvider = RootStoreContext.Provider

export const useStore = () => {
    const store = React.useContext(RootStoreContext)

    if( store === null ) {
        throw new Error("RootStoreContext cannot be null")
    }

    return store
}

export default RootModel
