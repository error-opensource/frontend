import { types } from "mobx-state-tree"

const UISettingsModel = types.model({
    trayVisible: types.boolean,
    trayLocked: types.boolean,
    overlayToolsVisible: types.boolean,
    mediaGalleryVisible: types.boolean,
    mobileMenuVisible:   types.boolean,
    markersVisible: types.boolean,
}).actions(self => ({
    setTrayVisible( value ) {
        self.trayVisible = value
    },
    setOverlayToolsVisible( value ) {
        self.overlayToolsVisible = value
    },
    setMediaGalleryVisible( value ) {
        self.mediaGalleryVisible = value
    },
    setMobileMenuVisible( value ) {
        self.mobileMenuVisible = value
    },
    setMarkersVisible( value ) {
        self.markersVisible = value
    },
    setTrayLock( value ) {
        self.trayLocked = value 
    }
}))

export default UISettingsModel 
