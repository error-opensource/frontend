import { types, flow, getType, getSnapshot, applySnapshot } from "mobx-state-tree"
import LoadingStateModel, { LoadingStatesEnum } from "./LoadingStateModel"

/**
 * A model to handle calling a graphql query and passing its data back to the parent model. 
 * 
 * 
 * Usage: 
 * 
 * const ItemsQueryModel = PageableQueryModel.named("RecordsQueryModel").props({
 *     results: types.array(RecordModel, [])
 * })
 *   
 * // create a prop with some default params
 * items: ItemsQueryModel.create({limit: 12, offset: 0, count: 0, loading: {code: LoadingStatesEnum.PENDING}})
 * 
 * // flow actions
 * const items = yield self.items.fetch(graphQLQueryFunction, {additional: "params"})
 * 
 */
const PageableQueryModel = types.model({
}).volatile(self => ({
    loading: LoadingStateModel.create({code: LoadingStatesEnum.PENDING}),
    query: null,
    customParams: {},
    count: 0, 
    append: false
})).actions(self => { 
    // a higher order function to perform the request and yield the results back to the store
    const fetch = flow(function* fetch(query, customParams = {}) {
        self.query = query 
        self.customParams = customParams 

        let params = customParams

        if( self.filter ) {
            const filterSnapshot = getSnapshot(self.filter)
            params = {...params, ...filterSnapshot}

            // TODO this is the wrong way to do this. We work around the fact that the postProcessSnapshot method
            //  returns an empty object on SearchResultsCollection by checking if the filter responds to toQuery.
            //  If it does, we assign that.
            if (self.filter.hasOwnProperty('toQuery')) {
                params = Object.assign({}, params, self.filter.toQuery)
            }
        }

        self.loading.loading()

        try {
            const queryResults = yield query(params)
            
            const {total: {aggregate: {count}}, results} = queryResults

            self.count = count
            self.loading.done()

            if( self.results ) {
                let newResults = []
                
                if( self.append ) {
                    newResults = [...self.results.toJSON(), ...results]
                }else {
                    newResults = results
                }
                
                applySnapshot( self.results, newResults )
                
                return queryResults
            }else {
                // if the model PageableQueryModel is implemented upon doesn't have 
                // its own .results property, just return the results directly.
                return queryResults.results
            }
        }catch( error ) {
            console.log("error!!", error)
            self.loading.setError(error, LoadingStatesEnum.ERROR)
        }
    })

    // Increment the offset and re-call our fetch() function to update the records
    const fetchNextPage = flow(function* fetchNextPage() {
        if(! ( self.query || self.filter || self.customParams ) ) {
            throw Error("No query, filter, or parameters")
        }

        self.filter.setSearchParameter('offset', self.filter.nextOffset)
        self.append = true

        const results = yield self.fetch(self.query, self.customParams)

        self.append = false
        
        return results
    })

    // clear out old state to avoid rendering stale content
    const clear = () => {
        self.results = []
        self.filter.offset = 0
    }
    
    return { fetch, fetchNextPage, clear }
}).views(self => ({
    get hasMore() {
        return self.count > (self.filter.offset + self.filter.limit)
    }
}))

export default PageableQueryModel