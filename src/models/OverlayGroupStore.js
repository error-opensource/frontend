import { types, flow, applySnapshot, getSnapshot, getRoot } from "mobx-state-tree"
import PageableQueryModel from "./PageableQueryModel"

import OverlayGroupModel from "./overlays/OverlayGroupModel"

import { PER_PAGE } from "../queries/client"
import { fetchOverlayGroupsQuery, fetchOverlayGroupQuery } from "../queries/OverlayQueries"
import { readSnapshotFromURL } from "./hydration/url"

export const OverlayGroupsFilter = types.model({
    limit: types.number,
    offset: types.number
}).actions(self => ({
    setSearchParameter(name, value) {
        self[name] = value
    }
})).views(self => ({
    get nextOffset() {
        return self.limit + self.offset
    }
}))

const OverlayGroupStore = PageableQueryModel.named("OverlayGroupCollection").props({
    filter: OverlayGroupsFilter,
    results: types.array(OverlayGroupModel, []),
    prefetchedResults: types.array(OverlayGroupModel, []), // overlays that have been fetched through url persistence
    enabled: types.array( types.string, [] )
}).volatile(self => ({
    overlayGroup: null,
}))
.actions(self => ({
    afterCreate: () => {
        // get any snapshotted store values, merge and apply to the store to restore sort order etc
        const snapshot = {...getSnapshot(self), ...readSnapshotFromURL( 'overlayGroups' )}
        applySnapshot( self, snapshot )
    },
    setSortOrder: (value) => {
        self.enabled = value
    }, 
    fetchOverlayGroups: flow(function* fetchOverlayGroups() {
        self.loading.loading()

        try {
            yield self.fetch(fetchOverlayGroupsQuery, self.filter.toJSON() )

            const root = getRoot( self )
            root.adjustLayoutScroll('top')
            
            self.loading.done()
        }catch( error ) {
            console.log(error)
        }
    }),
    prefetchOverlayGroups: flow(function* prefetchOverlayGroups( slugs ) {
        self.loading.loading()

        try {
            const params = { limit: PER_PAGE, offset: 0, slugs }
            const response = yield fetchOverlayGroupsQuery(params)

            self.prefetchedResults = response.results
            
            self.prefetchedResults.map((result) => {
                result.prefetched()
                result.isEnabled = true
                return result
            })

            self.loading.done()
        }catch( error ) {
            console.log(error)
        }
    }),
    fetchOverlayGroup: flow(function* fetchOverlayGroup( slug ) {
        const currentOverlayGroup = self.listOverlayGroups.find((overlay) => overlay.slug === slug)

        // check whether this overlay group is already in the store
        if( currentOverlayGroup ) {
            self.overlayGroup = currentOverlayGroup
            self.loading.done()

            return currentOverlayGroup
        }

        self.loading.loading()

        try {
            const params = { slug }
            const response = yield fetchOverlayGroupQuery(params)
            
            const results = [...self.prefetchedResults.slice(), ...response.result].filter((result, index, self) => self.indexOf(result) === index)
            self.prefetchedResults = results

            var overlayGroup 
            overlayGroup = self.listOverlayGroups.find((overlayGroup) => overlayGroup.slug === slug)
            if(! overlayGroup) {
                overlayGroup = self.prefetchedResults.find((overlayGroup) => overlayGroup.slug === slug)
            }

            if( !overlayGroup ) {
                self.loading.setError( "Not found", 404 )
                return null
            }

            self.overlayGroup = overlayGroup
            self.loading.done()
        }catch( error ) {
            console.log(error)
        }
    }),
    setPrefetchedResults: (results) => {
        self.prefetchedResults = results
        // self.sort = results.map((og) => og.id)
    },
    toggleOverlayGroupEnabled( value ) {
        let enabled = self.enabled.slice()
        let index = enabled.indexOf( value )
        
        if( index > -1 ) {
            enabled.splice(index, 1)
        }else {
            enabled.push( value )
        }

        self.enabled = enabled
    }
})).views(self => ({
    // the frontend iterates over listOverlayGroups. If a result is also in the prefetched 
    // collection, we render the prefetched one so that its state is maintained
    get listOverlayGroups() {
        const results = self.results.map((result) => {
            const prefetched = self.prefetchedResults.find((r) => r.slug === result.slug )

            if( prefetched ) {
                return prefetched
            }else {
                return result
            }
        })

        return results
    },
    get enabledOverlayGroups() { // return the enabled overlay groups sorted by the array of id's in self.sort - we call setSort([...]) to re-order the overlays when the drag event ends
        const enabledResults = self.results.filter((r) => r.enabled)
        const results = [...enabledResults, ...self.prefetchedResults].filter((result) => result.enabled)
        
        const slugSortOrder = self.enabled.length === results.length ? self.enabled : results.map((og) => og.id)
        // https://coding4.gaiama.org/en/es6-sort-an-array-based-on-another-array
        const arrayMap = results.reduce(
            (accumulator, currentValue) => ({
                ...accumulator,
                [currentValue.slug]: currentValue,
            }),
            {}
        )

        const sorted = slugSortOrder.map((slug) => arrayMap[slug]).filter((value) => value)
        return sorted
    },
    get enabledOverlayIds() {
        const allIds = self.enabledOverlayGroups.map((overlayGroup) => overlayGroup.overlays.map((overlay) => overlay.id))
        return [].concat(...allIds)
    }
}))

export default OverlayGroupStore
