import { types } from "mobx-state-tree"
import { trayContentTypes } from "./ContentTypes"

const CardItemsFilter = types.model({
    query: types.maybeNull(types.string, ""), 
    limit: types.number,
    offset: types.number,
    type: types.array(types.string, []),
}).volatile((self) => ({
    contentTypes: trayContentTypes
}))
.actions(self => ({
    setSearchParameter(name, value) {
        self[name] = value
    }
})).views(self => ({
    get nextOffset() {
        return self.limit + self.offset
    }
}))

export default CardItemsFilter
