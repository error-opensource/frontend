import { types } from "mobx-state-tree"

const CardItemImage = types.model({
    name: types.string, 
    url: types.maybeNull(types.string)
})

export default CardItemImage