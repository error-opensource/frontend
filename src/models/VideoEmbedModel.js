import { types } from "mobx-state-tree"

const VideoEmbedModel = types.model("VideoEmbedModel",{
  id: types.maybeNull(types.integer),
  name: types.string,
  description: types.maybeNull(types.string),
  credit: types.maybeNull(types.string),
  url: types.maybeNull(types.string),
  html: types.maybeNull(types.string),
  image_url: types.maybeNull(types.string)
})
  .preProcessSnapshot((snapshot) => {
    if( !snapshot ) return

    if( snapshot.video_embed ) {
      return snapshot.video_embed
    }else {
      return {...snapshot}
    }
  })

export default VideoEmbedModel
