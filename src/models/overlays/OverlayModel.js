import { types, getRoot } from "mobx-state-tree"

import OverlayType from "./OverlayType"
import OverlayMetadata from "./OverlayMetadata"

const OverlayModel = types.model("OverlayModel",{
    id: types.number,
    name: types.string,
    url: types.string,
    overlay_type: OverlayType,
    metadata: OverlayMetadata
}).volatile(self => ({
    value: 0.75,
    disabled: false
})).actions(self => ({
    setValue(value) {
        self.value = parseFloat( value )
    },
    focusInMap() {
        self.disabled = false

        const root = getRoot( self )
        root.mapViewport.fitBounds( self.layerBounds, self.metadata.minzoom )
    },
    toggleEnabled() {
        self.disabled = !self.disabled
    }
})).views(self => ({
    get layerBounds() {
        return [ [self.metadata.bounds[0], self.metadata.bounds[1]], [self.metadata.bounds[2], self.metadata.bounds[3]] ]
    },
    get layerOpacity() {
        return self.disabled ? 0 : self.value
    },
    get previousOverlayId() {
        try {
            const overlayStore = getRoot( self ).overlayGroups
            const ids = overlayStore.enabledOverlayIds
            const indexOfOverlay = ids.indexOf( self.id )

            if( indexOfOverlay > 0 ) {
                return ids[indexOfOverlay-1]
            }else {
                return null
            }
        }catch( error ) {
            console.log(error)
            console.log(self)
        }

        return null
    }
}))

export default OverlayModel
