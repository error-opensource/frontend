import { types } from "mobx-state-tree"

const OverlayType = types.model("OverlayType",{
    name: types.string, 
    description: types.string
}).volatile(self => (
    {
        source: 'raster' // fixme: this needs to be sent back in the graphql responses for new source types
    }
))

export default OverlayType
