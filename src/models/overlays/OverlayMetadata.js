import { types } from "mobx-state-tree"

const OverlayMetadata = types.model("OverlayMetadata",{
    bounds: types.array(types.number, []),
    minzoom: types.number, 
    maxzoom: types.number
}).preProcessSnapshot((snapshot) => {
    let snap = {...snapshot}
    snap.minzoom = parseInt( snap.minzoom, 10 )
    snap.maxzoom = parseInt( snap.maxzoom, 10 )

    return snap
})

export default OverlayMetadata
