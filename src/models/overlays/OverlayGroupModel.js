import { types, getParentOfType } from "mobx-state-tree"
import ImageModel from "../ImageModel"
import OverlayModel from "./OverlayModel"
import OverlayGroupStore from "../OverlayGroupStore"

export const OverlayGroupModel = types.model("OverlayGroupModel",{
    id: types.number,
    name: types.string, 
    slug: types.identifier, 
    sanitised_content: types.maybeNull( types.string ),
    image: types.maybeNull(ImageModel),
    overlays: types.array( OverlayModel, [])
}).volatile(self => ({
    enabled: false,
    isPrefetched: false,
    closed: false
})).actions(self => ({
    toggle() {
        self.enabled = !self.enabled
        
        // toggle this overlayGroup in the OverlayGroupStore model too so that it gets persisted to the url
        const overlayStore = getParentOfType( self, OverlayGroupStore )
        overlayStore.toggleOverlayGroupEnabled(self.slug)

        if( self.enabled && self.overlays.length > 0 ) {
            self.overlays[0].focusInMap()
        }
    },
    prefetched() {
        self.isPrefetched = true
    },
    toggleClosed() {
        self.closed = !self.closed
    }
})).views(self => ({
    get simplified() {
        return self.overlays.length === 1
    },
    get isEnabled() {
        return self.enabled
    },
    set isEnabled(value) {
        self.enabled = value
    }
})).preProcessSnapshot((snapshot) => {
    let snap = { ...snapshot }

    if (snap.overlays) {
        snap.overlays = snap.overlays.map((overlay) => overlay.hasOwnProperty('overlay') ? overlay.overlay : overlay)
    }

    return snap
})

export default OverlayGroupModel
