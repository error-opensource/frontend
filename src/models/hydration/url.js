import { applySnapshot, onPatch, getSnapshot } from "mobx-state-tree"
import queryString from "query-string"
import { throttle } from "lodash"

// restore the stores state by building a snapshot from the url params
export const rehydrateStoreFromURL = ( storeName, store ) => {
    if( typeof window === "object" && queryString.parse(window.location.search)[storeName] ) {
        try {
            const snapshot = readSnapshotFromURL( storeName )
            
            // try to apply the decoded snapshot onto the store
            applySnapshot( store, snapshot )
        }catch(error) {
        }
    }
}

// observe changes on a store and push its snapshot to the url params
export const persistStoreToURL = ( storeName, store ) => {
    onPatch( store, (patch) => {
        const snapshot = getSnapshot( store )
        
        writeSnapshotToURL( storeName, snapshot )
    })
}

export const readSnapshotFromURL = ( storeName ) => {
    if( typeof window === "object" && queryString.parse(window.location.search)[storeName] ) {
        try {
            const snapshotString = atob( queryString.parse(window.location.search)[storeName] )
            const snapshot       = JSON.parse( snapshotString )
            
            return snapshot
        }catch(error) {
        }
    }
}

// observe changes on a store and push its snapshot to the url params
export const writeSnapshotToURL = throttle( ( storeName, snapshot ) => {
    if( !typeof window === "object" ) return;

    // base64 encode the store params
    const encodedSnapshot = btoa( JSON.stringify( snapshot ) ) 
    
    const params = queryString.parse( window.location.search )
    params[storeName] = encodedSnapshot
    
    const url = new URL(window.location)
    url.search = queryString.stringify( params )
    
    window.history.pushState(null, null, url.toString())
}, 500 )