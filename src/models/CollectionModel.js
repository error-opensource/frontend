import { types, flow, getRoot } from 'mobx-state-tree'
import RecordModel from './RecordModel'
import ImageModel from './ImageModel'
import { fetchCollectionRecordsQuery } from '../queries/CollectionQueries'
import { fetchCollectionPointDataResultsQuery } from '../queries/PointDataQueries'
import PageableQueryModel from './PageableQueryModel'
import { PER_PAGE } from '../queries/client'
import TaxonomyTermModel from './TaxonomyTermModel'

const CollectionRecordsFilter = types
  .model({
    slug: types.string,
    limit: types.number,
    offset: types.number,
  })
  .actions((self) => ({
    setSearchParameter(name, value) {
      self[name] = value
    },
  }))
  .views((self) => ({
    get nextOffset() {
      return self.limit + self.offset
    },
  }))

const CollectionRecords = PageableQueryModel.named('CollectionRecords').props({
  filter: types.maybeNull(CollectionRecordsFilter),
  results: types.array(RecordModel, []),
})

const CollectionModel = types
  .model('CollectionModel', {
    tenant_id: types.number,
    name: types.string,
    slug: types.string,
    sanitised_content: types.maybeNull(types.string, ''),
    excerpt: types.maybeNull(types.string, ''),
    image: types.maybeNull(ImageModel),
    terms: types.array(TaxonomyTermModel, []),
  })
  .volatile((self) => ({
    records: CollectionRecords.create({
      results: [],
      filter: { slug: self.slug, limit: PER_PAGE, offset: 0 },
    }),
    owner: '',
  }))
  .actions((self) => ({
    fetchRecords: flow(function* fetchRecords() {
      self.records.clear()

      yield self.records.fetch(
        fetchCollectionRecordsQuery,
        self.records.filter.toJSON(),
      )
    }),
    fetchNextPage: flow(function* fetchNextPage() {
      yield self.records.fetchNextPage()
    }),
    getPointData: flow(function* getPointData(path, _, options = {}) {
        const response = yield fetchCollectionPointDataResultsQuery({
            slug: self.slug,
        })
        
        return response
    }),
  }))
  .views((self) => ({
    get centroid() {
      return self.records.results[0].location.centroid()
    },
    get showPoints() {
      return true
    },
  }))
  .preProcessSnapshot((snapshot) => {
    let snap = { ...snapshot }

    if (snap.terms) {
      snap.terms = snap.terms.map((term) => term.term)
    }

    return snap
  })

export default CollectionModel
