import { flow, types, getRoot, applySnapshot } from 'mobx-state-tree'
import { LoadingStatesEnum } from './LoadingStateModel'
import CardItemsCollection from './CardItemsCollection'
import TaxonomiesCollection from './TaxonomiesCollection'

import {
  fetchSearchCardItemResultsQuery,
  fetchSearchCardItemResultsQueryWithBounds,
  fetchSearchCardItemPointsQuery,
  fetchSearchCardItemPointsQueryWithBounds,
} from '../queries/SearchQueries'
import { fetchTaxonomiesQuery } from '../queries/TaxonomyQueries'
import CardItemResult from './CardItemResult'
import { trayContentTypes, contentTypeValues } from "./ContentTypes"

export const SearchFilter = types
  .model({
    query: types.maybeNull(types.string),
    limit: types.number,
    offset: types.number,
    type: types.array(types.string, []),
    whole_map: types.boolean,
    taxonomies: TaxonomiesCollection,
  })
  .volatile((self) => ({
    contentTypes: trayContentTypes,
    taxonomiesLoaded: false,

    defaultSearchProperties: {
        query: "",
        offset: 0,
        whole_map: true, 
        type: contentTypeValues
    }
  }))
  .actions((self) => ({
    reset() {
      self.taxonomies.setCheckedIds([])

      applySnapshot(self, {...self, ...self.defaultSearchProperties})
    },
    afterCreate() {
      self.fetchTaxonomies()
    },
    setSearchParameter(name, value) {
      self[name] = value
    },
    toggleSearchType(type) {
      let enabledTypes = self.type.slice()

      if (enabledTypes.indexOf(type) > -1) {
        enabledTypes.splice(enabledTypes.indexOf(type), 1)
      } else {
        enabledTypes.push(type)
      }

      self.type = enabledTypes
    },
    typeIsEnabled(type) {
      return self.type.indexOf(type) > -1
    },
    toggleBoundedSearch() {
      self.whole_map = !self.whole_map
    },
    fetchTaxonomies: flow(function* fetchTaxonomies() {
      yield self.taxonomies.fetch(fetchTaxonomiesQuery)
      self.taxonomiesLoaded = true
    }),
  }))
  .views((self) => ({
    get taxonomiesAreLoaded() {
      return self.taxonomiesLoaded
    },
    get nextOffset() {
      return self.limit + self.offset
    },
    get enabledContentTypes() {
      return self.type
    },
    get toQuery() {
      const root = getRoot(self)

      const query = {
        limit: self.limit,
        offset: self.offset,
        type: self.type.toJSON(),
      }

      if (self.query && self.query.length > 0) {
        query.query = self.query
      }

      if (!self.whole_map) {
        query.centroid = root.mapViewport.centroid
        query.distance = root.mapViewport.boundingBoxDistance
      }

      if (
        self.taxonomies.toQuery.taxonomy_term_ids &&
        self.taxonomies.toQuery.taxonomy_term_ids.length > 0
      ) {
        const term_ids = self.taxonomies.toQuery.taxonomy_term_ids.join(',')
        query.with_term_ids = `{${term_ids}}`
      } else {
        query.with_term_ids = null
      }

      return { ...query }
    },
    get boundedSearch() {
      return self.whole_map
    },
  }))
  .postProcessSnapshot((snap) => {
    // return an empty object so that when the PageableQueryModel calls getSnapshot, this store isn't
    // automatically merged with the query params for a search - we're using toQuery instead

    //   TODO this isn't working. In order for the PageableQueryModel to advance the offset, it needs a snapshot.
    return {}
  })

const SearchResultsCollection = CardItemsCollection.named(
  'SearchResultsCollection',
)
  .props({
    filter: SearchFilter,
    taxonomies: types.array(types.number, []),
    recordCount: types.maybeNull(types.number),
    collectionCount: types.maybeNull(types.number),
    trailCount: types.maybeNull(types.number),
    results: types.array(CardItemResult, []),
  })
  .volatile((self) => ({
    points: [],
  }))
  .actions((self) => ({
    reset: () => {
      self.results = []
      self.filter.reset()
    },
    fetchResults: flow(function* fetchResults() {
      self.loading.loading()

      try {
        const query = self.filter.whole_map
          ? fetchSearchCardItemResultsQuery
          : fetchSearchCardItemResultsQueryWithBounds

        const result = yield self.fetch(query, self.filter.toQuery)

        // store additional attributes from the response
        const {
          record_total: {
            aggregate: { count: recordCount },
          },
          collection_total: {
              aggregate: {count: collectionCount}
          },
          trail_total: {
            aggregate: {count: trailCount}
          }
        } = result
        
        self.recordCount = recordCount || 0
        self.collectionCount = collectionCount || 0
        self.trailCount = trailCount || 0

        const root = getRoot(self)
        root.adjustLayoutScroll('top')

        self.loading.done()
      } catch (error) {
        console.log('ERROR', error)
        self.loading.setError(error.toString(), LoadingStatesEnum.ERROR)
      }
    }),
    getPointData: flow(function* getPointData(_, queryParms, options = {}) {
      const query = self.filter.whole_map
        ? fetchSearchCardItemPointsQuery
        : fetchSearchCardItemPointsQueryWithBounds
      const response = yield query({...self.filter.toQuery, ...queryParms})
      
      return response
    })
  }))

export default SearchResultsCollection
