export const trayContentTypes = [
    { value: 'record', label: 'Record' },
    { value: 'collection', label: 'Collection' }
]

if( process.env.GATSBY_OVERLAYS_ENABLED ) {
    trayContentTypes.push({ value: 'overlay', label: 'Overlays' })
}

if( process.env.GATSBY_TRAILS_ENABLED ) {
    trayContentTypes.push({ value: 'trail', label: 'Trails' })
}

export const contentTypeValues = trayContentTypes.map((type) => type.value)