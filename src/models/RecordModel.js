import { types, flow, getRoot } from "mobx-state-tree"
import LocationModel from "./LocationModel"
import ImageModel from "./ImageModel"
import VideoEmbedModel from "./VideoEmbedModel";
import PageableQueryModel from "./PageableQueryModel"
import { fetchRecordImagesQuery, fetchRecordVideoEmbedsQuery } from "../queries/RecordQueries"
import TaxonomyTermModel from "./TaxonomyTermModel"

// extend our FilterQueryModel to store record images 
const RecordImagesFilter = types.model({
    slug: types.string, 
})

const RecordVideoEmbedsFilter = types.model({
    slug: types.string,
})

const RecordImages = PageableQueryModel.named("RecordImages").props({
    filter: types.maybeNull(RecordImagesFilter),
    results: types.array(ImageModel, [])
})

const RecordVideoEmbeds = PageableQueryModel.named("RecordVideoEmbeds").props({
    filter: types.maybeNull(RecordVideoEmbedsFilter),
    results: types.array(VideoEmbedModel, [])
})

const RecordModel = types.model({
    id: types.number,
    name: types.string,
    slug: types.string,
    sanitised_content: types.maybeNull(types.string),
    excerpt: types.maybeNull(types.string),
    location: LocationModel,
    date_from: types.maybeNull(types.string),
    date_to: types.maybeNull(types.string),
    image: types.maybeNull(ImageModel),
    terms: types.array(TaxonomyTermModel, []),
    hero_attachment_type: types.maybeNull(types.string),
    hero_attachment_id: types.maybeNull(types.integer)
}).volatile(self => ({
    owner: "",
    attached_images: RecordImages.create({results: [], filter: {slug: self.slug}} ),
    attached_video_embeds: RecordVideoEmbeds.create({results: [], filter: {slug: self.slug}}),
    heroAttachment: null
}))
.actions(self => ({ 
    fetchAttachments: flow(function* fetchAttachments() {
        self.attached_images.clear()
        self.attached_video_embeds.clear()
        yield self.attached_images.fetch(fetchRecordImagesQuery, self.attached_images.filter.toJSON() )
        yield self.attached_video_embeds.fetch(fetchRecordVideoEmbedsQuery, self.attached_video_embeds.filter.toJSON())
        self.setHeroAttachment();
    }),
    setHeroAttachment: () => {
        if (self.hero_attachment_type && self.hero_attachment_id) {
            switch (self.hero_attachment_type) {
                case "image":
                    self.heroAttachment = self.attached_images.results.find((image) => image.id === self.hero_attachment_id);
                    break;
                case "video_embed":
                    self.heroAttachment = self.attached_video_embeds.results.find((video_embed) => video_embed.id === self.hero_attachment_id)
                    break;
            }
        }
    },
    fetchNextPage: flow(function* fetchNextPage() {
        yield self.fetchAttachments()
    }),
    getPointData: (_, __ = {}) => {
        const point = {
            id: self.id, 
            name: self.name, 
            slug: self.slug, 
            type: 'record', 
            point_json: {lonlat: self.location.coordinates.toJSON()}, 
            record_image: {}
        }
        
        return [point]
    }
}))
.preProcessSnapshot((snapshot) => {
    if( !snapshot) return

    let snap 
    
    // when a Collection builds its array of records, the record is nested 
    // in a .record attribute from the graphql query so we need to move it up 1 level
    if( snapshot.record ) { 
        snap = {...snapshot.record}
    }else {
        snap = {...snapshot}
    }

    if( snap.hasOwnProperty( 'terms' ) ) {
        snap.terms = snap.terms.map((term) => term.hasOwnProperty('term') ? term.term : term)
    }

    return snap
})

export default RecordModel 
