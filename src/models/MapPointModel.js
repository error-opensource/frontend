import { types, getRoot, getParent, getParentOfType } from 'mobx-state-tree'
import CardItemImage from "./CardItemImage"
import MapPointsStack from "./MapPointsStack";

export const MapPointModel = types
  .model({
    id: types.number,
    identifier: types.identifier,
    name: types.string,
    coordinates: types.array(types.number),
    type: types.string,
    slug: types.string,
    image: types.maybeNull(CardItemImage),
    ordinalPosition: types.maybeNull(types.integer),
    customSpriteType: types.maybeNull(types.string),
  })
  .actions(self => ({
    setCustomSpriteType(value) {
        self.customSpriteType = value
    }
  }))
  .preProcessSnapshot((snapshot) => {
    let snap = { ...snapshot, identifier: `${snapshot.type}-point-${snapshot.id}` }

    snap.coordinates = snapshot.point_json.lonlat

    if (snapshot.type === 'record') {
      snap.image = snapshot.record_image.image
    } else if (snapshot.type === 'collection') {
      try {
        snap.image =
          snapshot.collection_image.image ||
          snapshot.collection_image.records[0].record.image
      } catch (error) {}
    }

    return snap
  })
  .views((self) => ({
    get spriteType() {
        return self.customSpriteType ? self.customSpriteType : self.type
    }
  })
)
