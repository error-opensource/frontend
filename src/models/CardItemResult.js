import { types } from "mobx-state-tree"
import LocationModel from "./LocationModel"
import CardItemImage from "./CardItemImage"

const CardItemResult = types.model({
    name: types.string,
    slug: types.string,
    location: LocationModel,
    date_from: types.maybeNull(types.string),
    date_to: types.maybeNull(types.string),
    type: types.string,
    excerpt: types.maybeNull(types.string),
    image: types.maybeNull(CardItemImage)
}).views(self => ({
    
})).preProcessSnapshot((snapshot) => {
    const newSnapshot = {...snapshot}

    if( snapshot.image ) { // already processed
        return newSnapshot
    }

    if( snapshot[`${snapshot.type}_image`] && snapshot[`${snapshot.type}_image`].image ) {
        newSnapshot.image = snapshot[`${snapshot.type}_image`].image
    }

    return newSnapshot
})

export default CardItemResult 
