import { types } from "mobx-state-tree"
import LocationModel from "./LocationModel";

const SiteMetaModel = types.model({
    site_title: types.maybeNull(types.string),
    facebook: types.maybeNull(types.string),
    instagram: types.maybeNull(types.string),
    twitter: types.maybeNull(types.string),
    contact_email: types.maybeNull(types.string),
    website: types.maybeNull(types.string),
    centroid: types.maybeNull(LocationModel)
})
export default SiteMetaModel