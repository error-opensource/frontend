import React from "react"
import renderer from "react-test-renderer"

import Header from "../../../src/components/Shared/Header"

describe("Header", () => {
    it("renders correctly", () => {
        const tree = renderer
            .create(<Header siteTitle="Default Starter" />)
            .toJSON()

        expect(tree).toMatchSnapshot()
    })

    it("has a h1 tag", () => {
        const wrapper = renderer.create(<Header siteTitle="Hello" />)
        const instance = wrapper.root

        expect(instance.findAllByType("h1").length).toBe(1)
    })
})
