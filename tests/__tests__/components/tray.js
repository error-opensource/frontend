import React from "react"
import renderer from "react-test-renderer"

import Header from "../../../src/components/Shared/Header"

const TrayItem = ({name}) => {
    return <div>{name}</div>
}
const Tray = ({items}) => {
    const trayItems = items.map((i, index) => <TrayItem key={index} name={i.name} />)
    
    return <div>
        {trayItems}
    </div>
}

describe("Header", () => {
    it("renders correctly", () => {
        const tree = renderer
            .create(<Header siteTitle="Default Starter" />)
            .toJSON()

        expect(tree).toMatchSnapshot()
    })

    it("has a h1 tag", () => {
        const wrapper = renderer.create(<Header siteTitle="Hello" />)
        const instance = wrapper.root

        expect(instance.findAllByType("h1").length).toBe(1)
    })
})

describe("Tray", () => {
    it("renders tray items", () => {
        const items = [{name: "Spec1"}, {name: "Spec2"}, {name: "Spec3"}, {name: "Spec4"}, {name: "Spec5"}, {name: "Spec6"}]
        const wrapper = renderer.create(<Tray items={items} />)
        const instance = wrapper.root

        expect(instance.findAllByType(TrayItem).length).toBe(items.length)
    })

    it("renders tray items upto a given pagination limit", () => {
        const PER_PAGE = 5
        const items = [{name: "Spec1"}, {name: "Spec2"}, {name: "Spec3"}, {name: "Spec4"}, {name: "Spec5"}, {name: "Spec6"}]
        const wrapper = renderer.create(<Tray items={items} paginate={{perPage: PER_PAGE}} />)
        const instance = wrapper.root

        expect(instance.findAllByType(TrayItem).length).toBe(PER_PAGE)
    })
})